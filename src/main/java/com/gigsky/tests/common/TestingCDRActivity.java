package com.gigsky.tests.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigsky.tests.common.calldatarecords.datagenerator.*;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.gigsky.tests.common.calldatarecords.datagenerator.CDRDataConfig.getCDRDataList;
import static com.gigsky.tests.common.calldatarecords.datagenerator.CDRDataConfig.getCDRListDailyUsageLimit;
import static com.gigsky.tests.common.calldatarecords.datagenerator.SimLocActivityResBuilder.getLocUpdateList;

public class TestingCDRActivity {

    @Test
    public void test() throws ParseException {

        TestAccountCDRConfig accountCDRConfig = new TestAccountCDRConfig(93465l);
        QueryParams queryParams = new QueryParams();
        queryParams.queryParamsUpdate();
        queryParams.cdrQueryParamUpdate();
//
        SimCDRActivityResBuilder simCDRActivityResBuilder= new SimCDRActivityResBuilder();

        List<CDRTestData> cdrTestDataList = getCDRDataList(queryParams.cdrQueryParam1);
        cdrTestDataList.addAll(getCDRDataList(queryParams.cdrQueryParam3));
        cdrTestDataList.addAll(getCDRDataList(queryParams.cdrQueryParam4));
        cdrTestDataList.addAll(getCDRDataList(queryParams.cdrQueryParam2));

        accountCDRConfig.setAccountDataUsage(cdrTestDataList);


        simCDRActivityResBuilder.parse(Arrays.asList(accountCDRConfig));

        simCDRActivityResBuilder.getEnrichedCDRList();

        Long accountId = 93465l;

//        System.out.println("cdrActivityList");
        simCDRActivityResBuilder.getSIMCDRActivityResponse(accountId,queryParams.queryParam3);
        CDRActivityList cdrActivityList=simCDRActivityResBuilder.getSimCDRActivity();


//        System.out.println("cdrEventList");
        SimCdrEventResBuilder simCdrEventResBuilder = new SimCdrEventResBuilder();
        simCdrEventResBuilder.parse(Arrays.asList(accountCDRConfig));
        simCdrEventResBuilder.getSIMCDREventResponse(93465l,"MyAcc",queryParams.queryParam1);
        CDREventList eventList = simCdrEventResBuilder.getSimCDREvent();


//        System.out.println("LocationActivityList");
        SimLocActivityResBuilder simLocActivityResBuilder = new SimLocActivityResBuilder();
        TestAccountCDRConfig accountCDRConfig1 = new TestAccountCDRConfig(93465l);
        List<LocationTestData> locationUpdateList = getLocUpdateList(queryParams.cdrQueryParam1);
        locationUpdateList.addAll(getLocUpdateList(queryParams.cdrQueryParam2));
        locationUpdateList.addAll(getLocUpdateList(queryParams.cdrQueryParam3));
        locationUpdateList.addAll(getLocUpdateList(queryParams.cdrQueryParam4));
        accountCDRConfig1.setLocationInfo(locationUpdateList);
        simLocActivityResBuilder.parse(Arrays.asList(accountCDRConfig1));


        simLocActivityResBuilder.getSIMLocationActivityResponse(accountId,queryParams.queryParam3);
        LocationActivityList locActivityList = simLocActivityResBuilder.getSimLocationActivity();

        SimLocEventResBuilder simLocEventResBuilder = new SimLocEventResBuilder();
        simLocEventResBuilder.parse(Arrays.asList(accountCDRConfig1));
        simLocEventResBuilder.getSIMLocationUpdateResponse(accountId,"MYACC",queryParams.queryParam1);
        LocationEventList locationEventList = simLocEventResBuilder.getSimLocationEvent();





        ObjectMapper Obj = new ObjectMapper();
//
        try {


            // get Oraganisation object as a json string
            String cdrActivity = Obj.writeValueAsString(cdrActivityList);
            String locationActivity = Obj.writeValueAsString(locActivityList);
            String locationEvent = Obj.writeValueAsString(locationEventList);
            String cdrEvent = Obj.writeValueAsString(eventList);
            // Displaying JSON String
            System.out.println("cdrActivity\n"+cdrActivity);
            System.out.println("locationActivity\n"+locationActivity);
            System.out.println("cdrEvent\n"+cdrEvent);
            System.out.println("LocationEvent\n"+locationEvent);

        }

        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
