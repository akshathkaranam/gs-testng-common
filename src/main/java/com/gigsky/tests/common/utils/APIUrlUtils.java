/*
 * Copyright (c) 2016. GigSky, Inc.
 * All rights reserved.
 *
 * THIS SOFTWARE IS THE CONFIDENTIAL AND PROPRIETARY
 * INFORMATION OF GIGSKY, INC.
 *
 * UNAUTHORIZED USE OR DISCLOSURE IS PROHIBITED.
 *
 * https://www.gigsky.com/
 */
package com.gigsky.tests.common.utils;

import com.gigsky.tests.common.apihandler.beans.APIUrl;
import com.gigsky.tests.common.apihandler.beans.APIUrlInfo;
import com.gigsky.tests.common.apihandler.beans.APIUrlInfoList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;

/**
 * Created by haribongale on 25/11/16.
 */
public class APIUrlUtils {
    private static final Logger logger = LoggerFactory.getLogger(APIUrlUtils.class);


    public static String getUrl(String apiServer,
                                String apiName) {
        APIUrlInfo apiUrlInfo = mUrlInfoList.getAPIUrlInfo(apiServer);
        Assert.assertNotNull(apiUrlInfo, "API Server:"+apiServer+" Invalid");
        APIUrl apiUrl = apiUrlInfo.getAPIUrl(apiName);
        Assert.assertNotNull(apiUrl, "API Name:"+apiName+" Invalid");
        Assert.assertNotNull(apiUrl.getUrl(), "URL for api:"+apiName+" Invalid");
        Assert.assertNotNull(apiUrl.getApi(), "API for api:"+apiName+" Invalid");
        return apiUrlInfo.getBaseurl() + apiUrl.getUrl();
    }
    public static String getBaseUrl(String apiServer) {
        APIUrlInfo apiUrlInfo = mUrlInfoList.getAPIUrlInfo(apiServer);
        Assert.assertNotNull(apiUrlInfo, "API Server:"+apiServer+" Invalid");
        return apiUrlInfo.getBaseurl();
    }



    /* Load json file into Bean*/
    private static APIUrlInfoList mUrlInfoList = null;
    static {
        //Object to JSON in file
        try {
            String filePath = ConfigKeyValueUtils.getValue(ConfigKeyValueUtils.KEYS.API_URLS_FILE_PATH.name());
            File file = new File(Thread.currentThread().getContextClassLoader().getResource(filePath).getFile());
            logger.info("====== API URL Info ======");
            TestUtils.printContents(file);
            mUrlInfoList = JsonUtils.readValue(file, APIUrlInfoList.class);
        } catch (IOException e) {
            logger.error("Exception: ", e);
            logger.error("ERROR: dbhosts file could not be parsed");
        }

        /*run through the DB config and build map*/
        Assert.assertNotNull(mUrlInfoList,"API URLInfo List Not found in file");
        Assert.assertNotNull(mUrlInfoList.getUrls(),"API URLs Not found in file");
        Assert.assertTrue(mUrlInfoList.getUrls().size() > 0);
        /*
        for(APIUrlInfo apiUrlInfo : mUrlInfoList.getUrls()) {
            APIServer apiServer = APIServer.valueOf(apiUrlInfo.getServer());
            Assert.assertNotNull(apiServer, "API Server name:"+apiUrlInfo.getServer()+" Invalid");
        }*/
    }
}
