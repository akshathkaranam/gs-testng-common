package com.gigsky.tests.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by haribongale on 15/12/16.
 */
public class ConfigKeyValueUtils {

    private static final Logger logger = LoggerFactory.getLogger(ConfigKeyValueUtils.class);


    public enum KEYS {
        API_URLS_FILE_PATH,
        DB_CONFIG_INFO_FILE_PATH,
        FTP_HOSTS_INFO_FILE_PATH,
        COMPEGENCE_SCRIPT_PATH
    }
    private static Map<String, String> mConfigKeyValue = new HashMap<String, String>();

    /*
    public static void main(String[] args) throws IOException {

        final String expected = "{\"application_type\":\"apache\",\"hosts\":[{\"one\":\"1\"},{\"two\":\"2\"}],\"last\":\"99\"}";
        logger.info("expected+"+expected);
    }*/

    public static String getValue(String key) {
        return mConfigKeyValue.get(key);
    }

    static {

        String config_file = System.getProperty("CONFIG_FILE");
        logger.info("System property CONFIG_FILE:"+config_file);
//        Assert.assertNotNull(config_file, "Configuration file is Not found");
        if(config_file == null) {
            config_file = "configs/local_mac/config_keyvalue.txt";
            logger.info("System property CONFIG_FILE is not defined; locating at default path:"+config_file);
        }

        /*
        for(KEYS key : KEYS.values()) {
            mConfigKeyValue.put(key, null);
        }*/

        //Object to JSON in file
        try {
//            String filePath = Thread.currentThread().getContextClassLoader().getResource(config_file).getPath();
            List<String> fileRows = TestUtils.parseFileLines(config_file);

            for(String keyval : fileRows) {
                String[] split = keyval.split("[ \t]*:[ \t]*");
                Assert.assertTrue(split.length >= 2);
                mConfigKeyValue.put(split[0], split[1]);
            }

        } catch (IOException e) {
            logger.error("Exception: ", e);
            logger.error("ERROR: dbhosts file could not be parsed");

            Assert.assertTrue(false, "Config file reading has failed");
        }

        /*Mandatory Keys check*/
        for(KEYS key : KEYS.values()) {
            Assert.assertTrue((mConfigKeyValue.get(key.name()) != null), "Mandatory Key:"+key+" has not been provided in configuration file");
        }

        logger.info("=== Configuration Key Value file parse completed ===");
    }
}
