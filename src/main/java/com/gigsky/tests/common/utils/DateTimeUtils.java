package com.gigsky.tests.common.utils;

import com.gigsky.tests.common.beans.GigskyDuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by jeyarajs on 20/12/16.
 */
public class DateTimeUtils {

    private static final Logger logger = LoggerFactory.getLogger(DateTimeUtils.class);
    private static final String UTC_TIMEZONE = "UTC";
    private static final String ADD_DURATION = "+";
    private static final String SUBTRACT_DURATION = "-";
    private static final int GIGSKY_DURATION_WITH_SIGN = 16;

    public static String getUTCTime(String gigskyDuration) throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        dateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TIMEZONE));
        Date dateToConvert = getDateFromGigskyDurationString(gigskyDuration);
        return dateFormat.format(dateToConvert);
    }

    private static Date getDateFromGigskyDurationString(String gigskyDurationStringWithSign) {
        try {
            if (StringUtils.isNotEmpty(gigskyDurationStringWithSign)) {
                String sign = ADD_DURATION;
                String gigskyDurationString;
                if (GIGSKY_DURATION_WITH_SIGN == gigskyDurationStringWithSign.length()) {
                    sign = gigskyDurationStringWithSign.substring(0, 1);
                    gigskyDurationString = gigskyDurationStringWithSign.substring(1);
                } else {
                    throw new IllegalArgumentException("Gigsky duration specified is incorrect");
                }
                GigskyDuration gigskyDuration = new GigskyDuration(gigskyDurationString);
                long timeInMilliSeconds = gigskyDuration.convertTimeInMilliSeconds();
                Calendar now = Calendar.getInstance();
                if (SUBTRACT_DURATION.equals(sign)) {
                    timeInMilliSeconds *= -1;
                }
                now.add(Calendar.MILLISECOND, (int) timeInMilliSeconds);
                return now.getTime();
            } else {
                return Calendar.getInstance().getTime();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Gigsky duration specified is incorrect\nGigsky duration should be 15 digit input with + or - function");
        }
    }

    public static Calendar getUTCTimeCal(){
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        timeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Calendar cal = Calendar.getInstance(timeZone);
        return cal;
    }

    private static String formatDate(Calendar calendar, String timeStampFormat){
        SimpleDateFormat format = new SimpleDateFormat(timeStampFormat);
        String formatted = format.format(calendar.getTime());
        return formatted;
    }

    public static String getDateFromToday(int daysToAddDelete){
        String TimeStampFormat = "yyyy-MM-dd HH:mm:ss";
        Calendar cal = getUTCTimeCal();
        cal.add(Calendar.DATE,daysToAddDelete);
        return formatDate(cal,TimeStampFormat);
    }
}
