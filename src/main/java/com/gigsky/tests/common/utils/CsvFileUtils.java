package com.gigsky.tests.common.utils;
import java.io.File;
import java.io.FileWriter;

/**
 * Created by utsavsheth on 03/02/17.
 */
public class CsvFileUtils {

    private static final String TEMP_PATH = "/tmp";

    public static File getFileFromResponseCsvLines(String[] csvLines) throws Exception {
        File responseTempFile = new File(TEMP_PATH + "/response.csv");
        FileWriter fileWriter = new FileWriter(responseTempFile);
        for(int i=0;i<csvLines.length;i++) {
            fileWriter.write(csvLines[i] + "\n");
        }
        fileWriter.flush();
        fileWriter.close();
        return responseTempFile;
    }

}
