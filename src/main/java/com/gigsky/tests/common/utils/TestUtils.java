/*
 * Copyright (c) 2016. GigSky, Inc.
 * All rights reserved.
 *
 * THIS SOFTWARE IS THE CONFIDENTIAL AND PROPRIETARY
 * INFORMATION OF GIGSKY, INC.
 *
 * UNAUTHORIZED USE OR DISCLOSURE IS PROHIBITED.
 *
 * https://www.gigsky.com/
 */
package com.gigsky.tests.common.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.AssertJUnit;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class TestUtils {
    private static final Logger logger = LoggerFactory.getLogger(TestUtils.class);

    public static List<String> parseFileLines(String filePath) throws IOException {

        URL resource = Thread.currentThread().getContextClassLoader().getResource(filePath);
        org.testng.Assert.assertNotNull(resource, "Resource:"+filePath+" NOT FOUND");
        File file = new File(resource.getFile());

        /*
        byte[] fileData = new byte[(int) file.length()];
        DataInputStream dis = new DataInputStream(new FileInputStream(file));
        dis.readFully(fileData);
        dis.close();
        */
        //logger.info(getByteArrayString(fileData));

        /*Read the sql list*/
        List<String> lines = new ArrayList<String>();

        FileInputStream fstream = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        String strLine;
        while ((strLine = br.readLine()) != null) {
//            System.out.println(strLine);
            lines.add(strLine);
        }
        br.close();
        fstream.close();

        return lines;
    }


    public static void printContents(String filePath) {
        File file = new File(Thread.currentThread().getContextClassLoader().getResource(filePath).getFile());
        printContents(file);
    }

    public static void printContents(File file) {
        try {
            /*
            Scanner input = null;
            input = new Scanner(file);
            StringBuilder sb = new StringBuilder();
            while (input.hasNextLine()) {
                sb.append(input.nextLine() + "\n");
            }
            input.close();
            logger.info(sb.toString());
            */
            String data = FileUtils.readFileToString(file, "UTF-8");
            logger.info(data);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String executeJAR(String jarFilePath, String commandLineArgs) throws Exception {
        return executeJAR(jarFilePath, commandLineArgs, true, false);
    }

    public static String executeJAR(String jarFilePath, String commandLineArgs, boolean isResourcePath, boolean assertOnError) throws Exception {
        logger.info("JAR file path is : " + jarFilePath);
        logger.info("Command line arguments are : " + commandLineArgs);

        if(isResourcePath) {
            URL resource = Thread.currentThread().getContextClassLoader().getResource(jarFilePath);
            org.testng.Assert.assertNotNull(resource, "JAR file:" + jarFilePath + " NOT FOUND");
            jarFilePath = resource.getPath();
            logger.info("script path is relative to resources folder; converted to absolute path:"+jarFilePath);
        }

        logger.info("Setting +x permission on file:"+jarFilePath);
        makeExecutable(jarFilePath);
        logger.info("Done");

        Process p = null;
        StringBuilder sb = new StringBuilder();

        try {
            p = Runtime.getRuntime().exec("java -jar " + jarFilePath + " " + commandLineArgs);
            p.waitFor();
            int status = p.exitValue();
            String outputMsg = getCmdOutput(p);
            String errorMsg = getCmdErrorOutput(p);
            sb.append("OutputMessage:"+outputMsg);
            sb.append("ErrorMessage:"+errorMsg);
            logger.info("Output of the shell script execution is  \n"+outputMsg);

            if(assertOnError) {
                Assert.assertTrue("JAR at " + jarFilePath + " failed. " + getFormattedErrorOutput(outputMsg, errorMsg), status == 0);
            }
        } finally {
            try {
                if (p != null) {
                    p.getErrorStream().close();
                    p.getInputStream().close();
                    p.destroy();
                }
            } catch (Exception e) {
                System.out.println("Error during destroying process");
            }
        }
        return sb.toString();
    }

    public static String executeShellScript(String scriptFilePath) throws Exception {
        return executeShellScript(scriptFilePath, true, false);
    }

    public static String executeShellScript(String input, boolean isResourcePath, boolean assertOnError) throws Exception {


        String[] splited = input.split("\\s+");
        String scriptFilePath=splited[0];
        String parameters=null;

        logger.info("Input Value is " +input);
        logger.info("Script file is " +scriptFilePath);

        logger.info("Length is  " +splited.length);

        parameters = StringUtils.join(ArrayUtils.removeElements(splited,splited[0])," ");
     logger.info("Parameters are " +parameters);


        logger.info("executeShellScript: "+scriptFilePath);

        Process p = null;
        StringBuilder sb = new StringBuilder();

        if(isResourcePath) {
            URL resource = Thread.currentThread().getContextClassLoader().getResource(scriptFilePath);
            org.testng.Assert.assertNotNull(resource, "Script file:" + scriptFilePath + " NOT FOUND");
            scriptFilePath = resource.getPath();
            logger.info("script path is relative to resources folder; converted to absolute path:"+scriptFilePath);
        }

        logger.info("Setting +x permission on file:"+scriptFilePath);
        makeExecutable(scriptFilePath);
        logger.info("Done");

        try {
            File file = new File(scriptFilePath);

            String[] shellCmds = new String[]{"bash", "-c", ". ~/.bash_profile; cd " + file.getParent()+ ";" + scriptFilePath +" " + parameters};
            Runtime runtime = Runtime.getRuntime();
            logger.info("Shell cmds is :");

            for (int j=0;j<shellCmds.length;j++)
                logger.info(shellCmds[j]);

            p = runtime.exec(shellCmds);
            p.waitFor();
            int status = p.exitValue();
            String outputMsg = getCmdOutput(p);
            String errorMsg = getCmdErrorOutput(p);
            sb.append("OutputMessage:"+outputMsg);
            sb.append("ErrorMessage:"+errorMsg);
            logger.info("Output of the shell script execution is  \n"+outputMsg);

            if(assertOnError) {
                Assert.assertTrue("Script at " + scriptFilePath + " failed. " + getFormattedErrorOutput(outputMsg, errorMsg), status == 0);
            }
        } finally {
            try {
                if (p != null) {
                    p.getErrorStream().close();
                    p.getInputStream().close();
                    p.destroy();
                }
            } catch (Exception e) {
                System.out.println("Error during destroying process");
            }
        }

        return sb.toString();
    }

    public static String getCmdOutput(Process p) throws IOException {
        StringBuffer output = new StringBuffer();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }
            return output.toString();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    public static String getCmdErrorOutput(Process p) throws IOException {
        StringBuffer output = new StringBuffer();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }
            return output.toString();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    public static String getFormattedErrorOutput(String outputMsg,
                                                 String errorMsg) {
        String msg = " Error output: " + errorMsg + " Command output: " + outputMsg + "\n";
        return msg;
    }



    public static int writeFileToFileSystem(InputStream is, String fsPath) throws Exception {
        BufferedReader reader = null;
        BufferedWriter writer = null;
        try {
            reader = new BufferedReader(new InputStreamReader(is));
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fsPath, false), "UTF-8"));

            char[] buf = new char[1024];
            int numRead = 0;
            while ((numRead = reader.read(buf)) != -1) {
                String readData = String.valueOf(buf, 0, numRead);
                numRead += readData.length();
                writer.write(readData);
            }
            makeExecutable(fsPath);
            return numRead;
        } catch(Exception e) {
            logger.error("Exception: ", e);
            throw new Exception("Unable to copy. To:"+fsPath);
        }
        finally {
            if(reader != null) {
                reader.close();
            }
            if(writer != null) {
                writer.flush();
                writer.close();
            }
        }
    }


    private static void makeExecutable(String fsPath) throws Exception {
        org.testng.Assert.assertNotNull("makeExecutable: Input null", fsPath);
        Process p = null;
        try {
            String[] shellCmds = new String[] { "chmod" , "u+x", fsPath };
            p = Runtime.getRuntime().exec(shellCmds);
            p.waitFor();
            int status = p.exitValue();
            AssertJUnit.assertTrue("Failed to set u+x chmod on path:"+fsPath+" . Error output: " + getCmdOutput(p), status == 0);
        }
        finally {
            try {
                if(p != null) {
                    p.getErrorStream().close();
                    p.getInputStream().close();
                    p.destroy();
                }
            }
            catch (Exception e) {
                System.out.println("Error during destroying process");
            }
        }
    }
}
