package com.gigsky.tests.common.utils;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.CollectionType;

import java.io.File;
import java.io.IOException;

/**
 * Created by jeyarajs on 08/12/16.
 */
public class JsonUtils {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static <T> T readValue(String content,
                                  Class<T> valueType) throws IOException {
        return objectMapper.readValue(content, valueType);
    }
    public static <T> T readValue(String content,
                                  CollectionType valueType) throws IOException {
        return objectMapper.readValue(content, valueType);
    }
    public static <T> T readValue(File file,
                                  Class<T> valueType) throws IOException {
        return objectMapper.readValue(file, valueType);
    }

    public static String writeValueAsString(Object jsonObject) throws IOException {
        return objectMapper.writeValueAsString(jsonObject);
    }
}
