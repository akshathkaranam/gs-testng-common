package com.gigsky.tests.common.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by jeyarajs on 12/12/16.
 */
public class SocketUtils {

    public static Socket connect(String host,
                                 int port) throws Exception {
        Socket socket = new Socket(host, port);
        System.out.println("Connected to host " + host + " and port " + port);
        return socket;
    }

    public static void closeConnection(Socket socket) throws Exception {
        if(socket != null) {
            socket.close();
            System.out.println("Closed the connection");
        }
    }

    public static void writeData(Socket socket,
                                 String message) throws Exception {
        DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
        outToServer.writeBytes(message + '\n');
        outToServer.flush();
        System.out.println("Message is written to the output stream");
    }

    public static String readData(Socket socket) throws Exception {
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        return inFromServer.readLine();
    }
}
