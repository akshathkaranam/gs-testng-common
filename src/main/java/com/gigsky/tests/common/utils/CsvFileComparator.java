package com.gigsky.tests.common.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CsvFileComparator {

    List<String[]> expectedColValues;
    List<String[]> actualColValues;

    //This method compares two csv file and returns false if file does not match, mismatched file value is printed
    public Boolean compareCSV(String inputFilePath, String outputFilePath) {

        try {

            expectedColValues = readFile(inputFilePath);
            actualColValues = readFile(outputFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (expectedColValues.size() != actualColValues.size()) {
            System.out.println("ExpectedOutPutFile count = " + expectedColValues.size() + " does not match with " +
                    "ActualOutputFile count = " + actualColValues.size());
            return false;
        } else {
            for (int i = 0; i < expectedColValues.size(); i++) {

                if (expectedColValues.get(i).length != actualColValues.get(i).length) {
                    System.out.println("At row = " + i + " ExpectedColumn count = " + expectedColValues.get(i).length
                            + " does not match with ActualColumn count = " + actualColValues.get(i).length);
                    return false;
                }

                for (int j = 0; j < expectedColValues.get(i).length; j++) {
                    String expectedRowValues = expectedColValues.get(i)[j];
                    String actualRowValues = actualColValues.get(i)[j];
                    if (!expectedRowValues.equals(actualRowValues)) {
                        System.out.println("Input file Value=" + expectedRowValues + " displayed in row =" + i + " " +
                                "does not match outputFile value=" + actualRowValues + " displayed in row = " + i);
                        return false;
                    }

                }
            }
        }
        return true;
    }


    //Reads csv file
    private static List<String[]> readFile(String fileName) throws IOException {
        List<String[]> values = new ArrayList<String[]>();
        Scanner s = new Scanner(new File(fileName));
        while (s.hasNextLine()) {
            String line = s.nextLine();
            values.add(line.split(","));
        }
        return values;
    }


}
