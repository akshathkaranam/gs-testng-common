package com.gigsky.tests.common.utils;

import com.gigsky.cdrmodule.psm.decoder.PSMGPRSRecord;
import com.gigsky.tests.common.beans.Mccmnc;
import com.gigsky.tests.common.calldatarecords.beans.CDRData;
import com.gigsky.tests.common.calldatarecords.beans.CDRInfo;
import com.gigsky.tests.common.calldatarecords.beans.FtpConfig;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Sureban on 1/23/17.
 */

public class CDRUtils
{
    
    public static CDRData generateCDRData(long imsi,
                                          long msisdn,
                                          int mcc,
                                          int mnc,
                                          int uplinkVolume,
                                          int downlinkVolume,
                                          long recordDurationInSecs,
                                          String trafficType,
                                          long imei,
                                          Calendar recordOpeningTime) {
        Mccmnc mccmnc = new Mccmnc();
        mccmnc.setMcc(mcc);
        mccmnc.setMnc(mnc);
        
        CDRData cdrData = new CDRData();
        cdrData.setImsi(imsi);
        cdrData.setMsisdn(msisdn);
        cdrData.setMccmnc(mccmnc);
        cdrData.setRecordDurationInSeconds(recordDurationInSecs);
        cdrData.setTrafficType(PSMGPRSRecord.ServiceVolume.TrafficType.valueOf(trafficType));
        cdrData.setDownlinkVolume(downlinkVolume);
        cdrData.setUplinkVolume(uplinkVolume);
        cdrData.setImei(imei);
        cdrData.setRecordOpeningTime(recordOpeningTime);
        return cdrData;
    }
    
    public static CDRInfo generateCDRInfo(FtpUtils.FTP_HOSTS ftpHosts,
                                          List<CDRData> cdrDataList) {
        return generateCDRInfo(FtpUtils.getFtpHostInfo(ftpHosts), cdrDataList);
    }
    
    private static CDRInfo generateCDRInfo(FtpConfig ftpConfig,
                                           List<CDRData> cdrDataList) {
        CDRInfo cdrInfo = new CDRInfo();
        cdrInfo.setFtpConfig(ftpConfig);
        cdrInfo.setCdrDataList(cdrDataList);
        return cdrInfo;
    }
}
