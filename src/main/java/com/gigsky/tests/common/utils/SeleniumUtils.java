package com.gigsky.tests.common.utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by vishaljogi on 28/06/18.
 */
public class SeleniumUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(SeleniumUtils.class);
    public static void saveScreenshot(WebDriver driver, String filename) throws Exception {
        if (driver == null) {
            return;
        }
        LOGGER.info("Saving Screenshot as {}",filename);
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        //copy screenshot file to test-output folder.
        if (scrFile != null) {
            FileUtils.copyFile(scrFile, new File(filename));
        }
    }
    public static void saveHtmlSource (WebDriver driver,String filename) throws Exception{
        if (driver == null) {
            return;
        }
        LOGGER.info("Saving Html file as {}",filename);

        String pageSource=driver.getPageSource();
        if (pageSource!=null)
        {
            FileUtils.writeStringToFile(new File(filename),pageSource);
        }



    }
}
