package com.gigsky.tests.common.utils;

import com.gigsky.tests.common.beans.FtpHostsList;
import com.gigsky.tests.common.calldatarecords.beans.FtpConfig;
import com.gigsky.tests.common.calldatarecords.beans.FtpData;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by jeyarajs on 08/12/16.
 */
public class FtpUtils {

    private static final Logger logger = LoggerFactory.getLogger(FtpUtils.class);

    public static void storeFile(FtpData ftpData,
                                 InputStream in) throws Exception {
        FTPClient ftpClient = ftpConnect(ftpData);
        try {
            String filePath = ftpData.getFilePath();
            String fileName = filePath + ".tmp";
            ftpClient.storeFile(fileName, in);
            logger.info("Stored file " + fileName);
            ftpClient.rename(fileName, filePath);
            logger.info("Renamed file " + fileName + " to " + filePath);
        } finally {
            ftpDisconnect(ftpClient);
        }
    }

    private static FTPClient ftpConnect(FtpData ftpData) throws Exception {
        FtpConfig ftpConfig = ftpData.getFtpConfig();
        FTPClient ftpClient = new FTPClient();
        ftpClient.setDefaultTimeout(ftpData.getDefaultTimeout());
        ftpClient.setConnectTimeout(ftpData.getConnectTimeout());
        /*Establish FTP Connection*/
        logger.info("FTP Connecting to:" + ftpConfig.getServer() + ":" + ftpConfig.getPort());
        ftpClient.connect(ftpConfig.getServer(), ftpConfig.getPort());
        ftpClient.login(ftpConfig.getUser(), ftpConfig.getPassword());
        ftpClient.enterLocalPassiveMode();
        ftpClient.setFileType(ftpData.getFileType());
        ftpClient.setDataTimeout(ftpData.getDataTimeout());
        ftpClient.changeWorkingDirectory(ftpConfig.getRemotePath());
        logger.info("FTP Connected");

        return ftpClient;
    }

    public static void deleteAllFiles(FtpData ftpData) throws Exception {
        FTPClient ftpClient = ftpConnect(ftpData);
        try {
            for (FTPFile ftpFile : ftpClient.listFiles()) {
                if (ftpFile.isFile()) {
                    String fileName = ftpFile.getName();
                    ftpClient.deleteFile(fileName);
                    logger.info("Deleted file " + fileName);
                }
            }
        } finally {
            ftpDisconnect(ftpClient);
        }
    }

    private static void ftpDisconnect(FTPClient ftpClient) throws Exception {
        try {
            ftpClient.logout();
            ftpClient.disconnect();
            logger.info("Disconnected ftp");
        } catch(Exception e) {
            logger.info("Disconnecting ftp failed");
        }
    }


    public static FtpConfig getFtpHostInfo(FTP_HOSTS ftpHostName) {
        FtpConfig ftpConfig = mFtpHostsList.getFtpHostInfo(ftpHostName.name());
        Assert.assertNotNull(ftpConfig, "FTP HostName:"+ftpHostName+" Invalid");

        return ftpConfig;
    }


    public enum FTP_HOSTS {
        FTP_IMSI_TDC,
        FTP_IMSI_SPY,
    };

    /* Load json file into Bean*/
    private static FtpHostsList mFtpHostsList = null;
    static {
        //Object to JSON in file
        try {
            String filePath = ConfigKeyValueUtils.getValue(ConfigKeyValueUtils.KEYS.FTP_HOSTS_INFO_FILE_PATH.name());
            File file = new File(Thread.currentThread().getContextClassLoader().getResource(filePath).getFile());
            logger.info("====== FTP Host Info ======");
            TestUtils.printContents(file);
            mFtpHostsList = JsonUtils.readValue(file, FtpHostsList.class);
        } catch (IOException e) {
            logger.error("Exception: ", e);
            logger.error("ERROR: FTP Host file could not be parsed");
        }

        /*run through and build map*/
        Assert.assertNotNull(mFtpHostsList,"FTPHostsInfo List Not found in file");
        Assert.assertNotNull(mFtpHostsList.getFtphosts(),"FTP Hosts Not found in file");
        Assert.assertTrue(mFtpHostsList.getFtphosts().size() > 0);
        for(FtpConfig ftpConfig : mFtpHostsList.getFtphosts()) {
            FTP_HOSTS ftpHostName = FTP_HOSTS.valueOf(ftpConfig.getName());
            Assert.assertNotNull(ftpHostName, "FTP HostName:"+ftpConfig.getName()+" Not recognized");
        }
    }
}
