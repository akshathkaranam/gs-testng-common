package com.gigsky.tests.common.utils;

import com.gigsky.tests.common.beans.Mccmnc;
import com.gigsky.tests.common.locationupdates.common.LocationSenderFactory;
import com.gigsky.tests.common.locationupdates.common.LocationSource;
import com.gigsky.tests.common.locationupdates.common.beans.LocationEvent;
import com.gigsky.tests.common.locationupdates.common.beans.LocationInfo;
import com.gigsky.tests.common.locationupdates.common.interfaces.LocationSender;
import com.gigsky.tests.common.locationupdates.huawei.beans.HuaweiLocationInfo;
import com.gigsky.tests.common.locationupdates.huawei.beans.HuaweiLocationServerConfig;
import com.gigsky.tests.common.locationupdates.huaweiCSLocation.beans.HuaweiCsLocationInfo;
import com.gigsky.tests.common.locationupdates.procera.beans.ProceraLocationInfo;
import com.gigsky.tests.common.locationupdates.tdc.beans.TDCLocationInfo;
import com.gigsky.tests.common.locationupdates.tdc.beans.TDCLocationServerConfig;

import java.util.List;

public class LocationUtil {


    public LocationInfo getLocationInfo(String imsi, String msisdn, String mcc, String mnc, LocationSource locationSource) throws Exception
    {
        LocationInfo locationInfo = null;
        switch(locationSource) {
            case TDC:
                locationInfo = getLocationInfoOfSource(imsi,msisdn,mcc,mnc,new TDCLocationInfo());
                break;
            case HUAWEI:
                locationInfo = getLocationInfoOfSource(imsi,msisdn,mcc,mnc,new HuaweiLocationInfo());
                break;
            case PROCERA:
                locationInfo = getLocationInfoOfSource(imsi,msisdn,mcc,mnc,new ProceraLocationInfo());
                break;
            case HUAWEI_CS:
                locationInfo =getLocationInfoOfSource(imsi,msisdn,mcc,mnc,new HuaweiCsLocationInfo());
        }

        return locationInfo;
    }


    private LocationInfo getLocationInfoOfSource(String imsi,String msisdn, String mcc,String mnc, LocationInfo locationInfo ){
        Mccmnc mccMnc=new Mccmnc();
        locationInfo.setImsi(Long.valueOf(imsi));
        locationInfo.setMsisdn(Long.valueOf(msisdn));
        locationInfo.setMccmnc(mccMnc);
        locationInfo.getMccmnc().setMcc(Integer.valueOf(mcc));
        locationInfo.getMccmnc().setMnc(Integer.valueOf(mnc));
        return locationInfo;
    }

    public void sendMultipleLocationUpdate(List<LocationInfo> locationInfos, LocationSource locationSource) throws Exception
    {
        LocationSenderFactory locationSenderFactory = LocationSenderFactory.getInstance();
        LocationSender locationSender = locationSenderFactory.getLocationSender(locationSource);
        LocationEvent locationEvent = new LocationEvent();
        locationEvent.setLocationInfoList(locationInfos);
        locationEvent.setServerConfig(this.getLocationServerConfig(locationSource));
        locationSender.sendLocations(locationEvent);
    }

    private Object getLocationServerConfig(LocationSource locationSource) {
        switch(locationSource) {
            case TDC:
                return new TDCLocationServerConfig("http://localhost:8080/gsimsi");
            case HUAWEI:
                return new HuaweiLocationServerConfig("http://localhost:8080/gsimsi");
            default:
                return null;
        }
    }

}
