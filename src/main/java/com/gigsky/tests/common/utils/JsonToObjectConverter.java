package com.gigsky.tests.common.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by vishaljogi on 09/01/17.
 */

public class JsonToObjectConverter<T> {
    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public JsonToObjectConverter() {

    }

    public static <T> T convert(String jsonString, Class<T> beanClass) throws Exception {
        if (jsonString == null) {
            return null;
        }

        return mapper.readValue(jsonString, beanClass);
    }
}