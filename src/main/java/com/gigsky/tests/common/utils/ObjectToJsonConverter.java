package com.gigsky.tests.common.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by vishaljogi on 09/01/17.
 */

public class ObjectToJsonConverter {
    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public ObjectToJsonConverter() {

    }

    public static String convert(Object beanClass) throws Exception {
        if (beanClass == null) {
            return null;
        }

        return mapper.writeValueAsString(beanClass);
    }
}