package com.gigsky.tests.common.EmailConfig;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Created by supreetha on 06/06/16.
 */
public class GS_FileUtil {


    //This method returns absolute path of a file
    public String getAbsolutePath(String file_Path){
        File F = new File(file_Path);
        file_Path = F.getAbsolutePath();
        //System.out.println(file_Path);
        return file_Path;
    }


    //This method prints list of files in a folder
    public void listFilesForFolder(final File folder) {
        for (final File fileEntry : folder.listFiles()) {

            File[] files = folder.listFiles();
            int yy = files.length;


            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {
                System.out.println(fileEntry.getName());
            }
        }
    }


    public String fileCreationCheck(String path){

        //Cross check whether there is any file exists in the directory.

        File folder = new File(path);
        File[] files = folder.listFiles();
        if(files.length > 0)
        {
            return files[0].getName();
        }

        long timeOut = 120000; //waiting for two minutes and passindg timeout in seconds
        //long timeOut = 60000;

        long startTime = System.currentTimeMillis();
        long currentTime = (System.currentTimeMillis());
        long duration = currentTime - startTime;
        path = getAbsolutePath(path); //getting absolute path of a folder

        try {
            WatchService watcher = FileSystems.getDefault().newWatchService();
            Path dir = Paths.get(path);
            dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);

            System.out.println("Watch Service registered for dir: " + dir.getFileName());

            while (duration<timeOut) {
                WatchKey key;
                try {
                    key = watcher.poll(1000, TimeUnit.MILLISECONDS);
                }catch(InterruptedException e){
                    return null;
                }
                if(key!=null) {
                    for (WatchEvent<?> event : key.pollEvents()) {
                        WatchEvent.Kind<?> kind = event.kind();

                        @SuppressWarnings("unchecked")
                        WatchEvent<Path> ev = (WatchEvent<Path>) event;
                        Path fileName = ev.context();

                        System.out.println(kind.name() + ": " + fileName);

                        if (kind == ENTRY_CREATE) {
                            System.out.println("Created: " + fileName);

                            return event.context().toString();
                        }
                    }

                    boolean valid = key.reset();
                    if (!valid) {
                        break;
                    }
                }

                currentTime = (System.currentTimeMillis());
                duration = currentTime - startTime;
            }
        }
        catch(IOException e) {
        }

        return null;
    }


}
