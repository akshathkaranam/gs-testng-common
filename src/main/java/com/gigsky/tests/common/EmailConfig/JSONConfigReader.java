    package com.gigsky.tests.common.EmailConfig;

    import org.json.JSONObject;

    import java.io.BufferedReader;
    import java.io.FileReader;

    /**
     * Created by supreetha on 05/07/16.
     */
    public class JSONConfigReader {

        protected JSONObject configMap;

        public void setConfigMap(String fileName){
            try{
                StringBuilder sb = readLine(fileName);
                configMap = new JSONObject(sb.toString());
            }
            catch (Exception e){
                System.out.println("File "+fileName+" not found");
                e.printStackTrace();
            }
        }

        //This method returns json object for a given key
        public JSONObject getObject(String key){
            configMap = configMap.optJSONObject(key);
            if(configMap==null)
                System.out.println("For Key "+key+" value not found");

            return configMap;
        }

        //This method returns Value of type String for a given key
        public String getString(String key){
            String value = configMap.optString(key);
            if(value==null)
                System.out.println("For Key "+key+" value not found");

            return value;
        }

        //This method returns Value of type String by substituting with argument list for a given key
        public String  getStingWithArguments(String key,String[] argumentList){
            return String.format(configMap.optString(key,key),argumentList);
        }


        public StringBuilder readLine(String filename) throws Exception
        {
            BufferedReader bReader = null;
            try {
                bReader = new BufferedReader(new FileReader(filename));
                StringBuilder sb = new StringBuilder();
                String line = bReader.readLine();

                while (line != null) {
                    sb.append(line);
                    line = bReader.readLine();
                }
                return sb;
            }
            finally {
                bReader.close();
            }
        }



    }
