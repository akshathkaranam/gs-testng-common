package com.gigsky.tests.common.EmailConfig;

import org.json.JSONException;
import org.json.JSONObject;

public class EmailConfigReader extends JSONConfigReader {

    public EmailConfig getEmailDetails(String key) throws JSONException {
        super.setConfigMap("resources/EmailSubjectsAndPathMapping.json");
        JSONObject jValue = getObject(key);
        EmailConfig  eConfig = new EmailConfig(jValue);
        return  eConfig;
    }

    public class EmailConfig {

        private String subject = null;
        private String path = null;

        public EmailConfig(JSONObject obj) throws JSONException {
            JSONObject object = obj;
            if (obj != null) {
                this.subject = obj.getString("Subject");
                this.path = obj.getString("Path");
            } else {
                System.out.println("EmailConfig object not found");
            }
        }

        public String getSubject(){return subject;
        }

        public String getPath(){return path;
        }
    }
}
