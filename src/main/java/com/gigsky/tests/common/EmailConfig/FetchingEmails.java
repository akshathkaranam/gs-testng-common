package com.gigsky.tests.common.EmailConfig;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import javax.mail.*;
import javax.mail.search.FlagTerm;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Properties;

public class FetchingEmails {

    private static String actualMessage = null;

    public String getActualMessage(String user, String password, String emailType) throws MessagingException, JSONException {
        EmailConfigReader emailConfigReader = new EmailConfigReader();
        EmailConfigReader.EmailConfig emailDetails = emailConfigReader.getEmailDetails(emailType);
        Store store = null;
        Folder emailFolder = null;
        String expectedSubject = emailDetails.getSubject();

        String host = "pop.gmail.com";
        try {
            // create properties field
            Properties properties = new Properties();
            properties.put("mail.store.protocol", "pop3");
            properties.put("mail.pop3.host", host);
            properties.put("mail.pop3.port", "995");
            properties.put("mail.pop3.starttls.enable", "true");
            Session emailSession = Session.getDefaultInstance(properties);

            // create the POP3 store object and connect with the pop server
            store = emailSession.getStore("pop3s");

            store.connect(host, user, password);

            //Open InBox in read-write Mode
            emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_WRITE);

            //Get only UnRead Emails List
            Flags seen = new Flags(Flags.Flag.SEEN);
            FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
            Message messages[] = emailFolder.search(unseenFlagTerm);
            System.out.println("unread messages.length---" + messages.length);

            for (int i = messages.length-1; i >=0 ; i--) {
                Message message = messages[i];
                String subject_UI = message.getSubject();
                if(subject_UI.equals(expectedSubject)) {
                    actualMessage = writePart(message);
                    message.setFlag(Flags.Flag.DELETED, true);
                    if(actualMessage.contains(user)) {
                        return actualMessage.replaceAll("\\s","");
                    }
                    else if(subject_UI.contains("alert")){
                        return actualMessage.replaceAll("\\s","");
                    }
                    else
                        return actualMessage.replaceAll("\\s","");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            // close the store and folder objects (Comment below lines to run locally)
            emailFolder.close(true);
            store.close();
        }
        return null;
    }

    private String writePart(Part p) throws Exception {

        System.out.println("CONTENT-TYPE: " + p.getContentType());

        //Get Html Content from Multipart Email
        if (p.isMimeType("multipart/*")) {
            System.out.println("This is a Multipart");
            System.out.println("---------------------------");
            Multipart mp = (Multipart) p.getContent();
            int count = mp.getCount();
            for (int i = 0; i < count; i++) {
                BodyPart part = mp.getBodyPart(i);
                if(part.isMimeType("text/html")){
                    return String.valueOf(part.getContent());
                }
            }
        }
        //Get Html Content from Nested Message
        else if (p.isMimeType("message/rfc822")) {
            System.out.println("This is a Nested Message");
            System.out.println("---------------------------");
            writePart((Part) p.getContent());
        }
        return (String)p.getContent();
    }

    public String getLinkFromEmail(String userName, String pwd, String subject) throws MessagingException, JSONException {
        if(actualMessage==null)
            actualMessage = getActualMessage(userName, pwd, subject);
        Document doc = Jsoup.parse(actualMessage);
        Element link = doc.select("a").first();
        String linkHref = link.attr("href");

        return linkHref;
    }

    public String getExpectedEmail(String emailType, String arguments[]) throws IOException, JSONException {
        EmailConfigReader emailConfigReader = new EmailConfigReader();
        GS_FileUtil fu = new GS_FileUtil();
        EmailConfigReader.EmailConfig emailDetails = emailConfigReader.getEmailDetails(emailType);

        String expectedFilePath = fu.getAbsolutePath(emailDetails.getPath());
        String expectedEmail = FileUtils.readFileToString(new File(expectedFilePath),"UTF-8");
        if(arguments!=null) {
            expectedEmail = MessageFormat.format(expectedEmail, arguments).trim();
            expectedEmail = expectedEmail.replaceAll("\\s", "");
        }
        else {
            expectedEmail = expectedEmail.replaceAll("\\s", "");
        }
        return expectedEmail;
    }
}
