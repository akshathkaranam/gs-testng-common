/*
 * Copyright (c) 2016. GigSky, Inc.
 * All rights reserved.
 *
 * THIS SOFTWARE IS THE CONFIDENTIAL AND PROPRIETARY
 * INFORMATION OF GIGSKY, INC.
 *
 * UNAUTHORIZED USE OR DISCLOSURE IS PROHIBITED.
 *
 * https://www.gigsky.com/
 */
package com.gigsky.tests.common.database.utils;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.utils.TestUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.AssertJUnit;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.*;

/**
 * Created by haribongale on 06/12/16.
 */
public class DBAccessUtils {
    private static final Logger logger = LoggerFactory.getLogger(DBAccessUtils.class);
    private static final String TEMP_PATH = "/tmp";

    private static class Holder {
        static final DBAccessUtils INSTANCE = new DBAccessUtils();
    }

    public static DBAccessUtils getInstance() {
        return Holder.INSTANCE;
    }


    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    /*API for clearing data from ALL tables of a schema*/
    public static void truncateSchmea(DBConfigInfo dbConfigBean) throws Exception {

        logger.info("truncateSchema+++: "+dbConfigBean.getDbName()+" "+dbConfigBean.getSchema());
        Assert.assertNotNull(dbConfigBean);
        List<String> tables = executeMysqlQueryStatement(dbConfigBean, "show tables");
        Assert.assertTrue(tables != null);
        Assert.assertTrue(tables.size() > 0);


        List<String> truncateTableList = new ArrayList<String>();
        truncateTableList.add("SET FOREIGN_KEY_CHECKS = 0;");
        for (String table : tables) {
            truncateTableList.add("truncate table " + table);
        }
        truncateTableList.add("SET FOREIGN_KEY_CHECKS = 1;");

        logger.info("truncating tables:"+tables.size());
        executeMysqlUpdateStatements(dbConfigBean, truncateTableList);
        logger.info("truncateSchema---: "+dbConfigBean.getDbName()+" "+dbConfigBean.getSchema());
    }


    /*Write only queries
    * 4aug17: made API to throw exception if any update fails*/
    public static void executeMysqlUpdateStatements(DBConfigInfo dbConfigBean, List<String> mysqlStatementList) throws Exception {

        Assert.assertNotNull(dbConfigBean);
        Assert.assertNotNull(mysqlStatementList);
        Assert.assertTrue(mysqlStatementList.size() > 0);


        /*Run all Statements*/
        Connection connection = null;
        try {
            connection = getConnection(dbConfigBean);
            connection.setAutoCommit(false);

            for (String mysqlStatement : mysqlStatementList) {
                PreparedStatement preparedStatement = null;
                try {
                    preparedStatement = connection.prepareStatement(mysqlStatement);
                    preparedStatement.executeUpdate();
                } finally {
                    closeStatement(preparedStatement);
                }
            }
            logger.info("Mysql Update of "+mysqlStatementList.size()+" statements Done");
            if (connection != null) {
                connection.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception:", e);
            if (connection != null) {
                connection.rollback();
            }
            throw e;
        }
        finally {
            closeConnection(connection);
        }

    }

    /*Read only single query*/
    public static List<String> executeMysqlQueryStatement(DBConfigInfo dbConfigBean, String mysqlStatement) throws Exception {

        Assert.assertNotNull(dbConfigBean);
        Assert.assertNotNull(mysqlStatement);

        /*Run Statement*/
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<String> outputList = new ArrayList<String>();
        try {
            connection = getConnection(dbConfigBean);

            preparedStatement = connection.prepareStatement(mysqlStatement);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String value = resultSet.getString(1);
                if (value != null) {
                    outputList.add(value);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            closeResultSet(resultSet);
            closeStatement(preparedStatement);
            closeConnection(connection);
        }

        return outputList;

    }

    public static List<Map<String, Object>> executeMySqlSelectQuery(DBConfigInfo dbConfigBean, String sqlQuery) throws Exception {
        Assert.assertNotNull(dbConfigBean);
        Assert.assertNotNull(sqlQuery);

        /*Run Statement*/
        List<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = getConnection(dbConfigBean);
            PreparedStatement prepStmt = connection.prepareStatement(sqlQuery);
            resultSet = prepStmt.executeQuery();
            List<String> columnNames = getColumnNames(resultSet);
            while (resultSet.next()) {
                Map<String, Object> row = new HashMap<String, Object>();
                for (String column : columnNames) {
                    row.put(column, resultSet.getObject(column));
                }
                rows.add(row);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            closeResultSet(resultSet);
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
        return rows;
    }

    public static <T> List<T> executeMySqlSelectQuery(DBConfigInfo dbConfigBean, String sqlQuery, Class<T> clazz) throws Exception {
        Assert.assertNotNull(dbConfigBean);
        Assert.assertNotNull(sqlQuery);

        List<T> rows = new ArrayList<T>();

        List<Map<String, Object>> results = executeMySqlSelectQuery(dbConfigBean, sqlQuery);
        if (CollectionUtils.isNotEmpty(results)) {
            for (Map<String, Object> result : results) {
                T row = createInstance(result, clazz);
                rows.add(row);
            }
        }

        return rows;
    }

    private static <T> T createInstance(Map<String, Object> row, Class<T> clazz)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException {
        T instance = (T) Class.forName(clazz.getName()).newInstance();
        for (String fieldName : row.keySet()) {
            BeanUtils.setProperty(instance, fieldName, row.get(fieldName));
        }
        return instance;
    }

    private static List<String> getColumnNames(ResultSet resultSet) throws SQLException {
        ResultSetMetaData metaData = resultSet.getMetaData();
        List<String> columns = new ArrayList<String>();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            columns.add(metaData.getColumnLabel(i));
        }
        return columns;
    }


    /*Execute a sql file*/
    public static void executeMySqlScript(String scriptUrl, DBConfigInfo dbConfigBean) throws Exception {

        logger.info("executeMysqlScript:" + scriptUrl);
        Assert.assertNotNull(dbConfigBean);
        Assert.assertNotNull(scriptUrl);

        Process p = null;
        try {
            logger.info("Storing to Temporary Location: " + scriptUrl);
            File f = new File(scriptUrl);
            String absPath = TEMP_PATH + "/" + f.getName();
            InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(scriptUrl);
            TestUtils.writeFileToFileSystem(is, absPath);


            logger.info("Running script " + scriptUrl);
            String mysqlCmd = "mysql" + " -u" + dbConfigBean.getUsername();
            mysqlCmd += " -h" + dbConfigBean.getUrl();
            mysqlCmd += " -P" + dbConfigBean.getPort();
            if (dbConfigBean.getPassword() != null && dbConfigBean.getPassword().trim().length() > 0) {
                mysqlCmd += " -p" + dbConfigBean.getPassword();
            }
            mysqlCmd += " --protocol=TCP";
            mysqlCmd += " --default-character-set=utf8";
            mysqlCmd += " " + dbConfigBean.getSchema() + " < " + absPath;

            String[] shellCmds = new String[]{"bash", "-c", ". ~/.bash_profile;cd " + TEMP_PATH + ";" + mysqlCmd};
            p = Runtime.getRuntime().exec(shellCmds);
            p.waitFor();
            int status = p.exitValue();
            String outputMsg = TestUtils.getCmdOutput(p);

            String errorMsg = TestUtils.getCmdErrorOutput(p);

            AssertJUnit.assertTrue("Failed to configure db with dbconfig script at "
                    + scriptUrl + ": " + TestUtils.getFormattedErrorOutput(outputMsg, errorMsg)
                    + "[DEBUG] " + dbConfigBean.toString() + "\n", status == 0);

//            checkDBIntegrity(scriptUrl, dbConfigBean);
            logger.info("done Running script " + scriptUrl);
        }
        finally {
            try {
                if (p != null) {
                    p.getErrorStream().close();
                    p.getInputStream().close();
                    p.destroy();
                }
            }
            catch (Exception e) {
                logger.info("Error during destroying process", e);
            }
        }
    }


    public static void executeMysqlScripts(Map<String, String[]> dbScripts, boolean truncate) throws Exception {

        Iterator it = dbScripts.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();

            String key = (String) pair.getKey();
            String[] scripts = (String[]) pair.getValue();

            /*Execute the scripts*/
            DBConfigInfo dbAccess = DBConfigUtils.getDBAccess(key);
            Assert.assertNotNull(dbAccess);

            if (truncate) {
                DBAccessUtils.truncateSchmea(dbAccess);
            }

            for (String script : scripts) {
                DBAccessUtils.executeMySqlScript(script, dbAccess);
            }

            it.remove(); // avoids a ConcurrentModificationException
        }
    }


    private static void closeResultSet(ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Exception:", e);
        }
    }


    private static void closeStatement(PreparedStatement preparedStatement) {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Exception:", e);
        }
    }


    private static Connection getConnection(DBConfigInfo databaseConfigBean) throws SQLException {

        Assert.assertNotNull(databaseConfigBean, "DBConfigInfo passed to getConnection is NULL");

        Connection conn = null;
        try {
            /*
            "jdbc:mysql://localhost/test?" +
                    "user=minty&password=greatsqldb");
                    */
            String dbUrl = "jdbc:mysql://"
                    + databaseConfigBean.getUrl();
            if (databaseConfigBean.getPort() != null) {
                dbUrl = dbUrl + ":" + databaseConfigBean.getPort();
            }
            dbUrl = dbUrl + "/" + databaseConfigBean.getSchema();
            logger.info("DB Url:" + dbUrl);
            conn = DriverManager.getConnection(dbUrl, databaseConfigBean.getUsername(), databaseConfigBean.getPassword());

        }
        catch (SQLException ex) {
            logger.error("getConnection failure for databaseConfig:" + databaseConfigBean.toString());
            // handle any errors
            logger.info("SQLException: " + ex.getMessage());
            logger.info("SQLState: " + ex.getSQLState());
            logger.info("VendorError: " + ex.getErrorCode());
            logger.error("getConnection on DB:" + databaseConfigBean.getSchema() + " has failed", ex);

            Assert.fail("getConnection on DB:" + databaseConfigBean.getSchema() + " has failed", ex);
        }

        return conn;
    }

    private static void closeConnection(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            logger.error("Exception:", e);
        }
    }

}
