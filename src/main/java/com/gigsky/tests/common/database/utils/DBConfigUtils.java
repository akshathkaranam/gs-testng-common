package com.gigsky.tests.common.database.utils;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gigsky.tests.common.utils.ConfigKeyValueUtils;
import com.gigsky.tests.common.utils.JsonUtils;
import com.gigsky.tests.common.utils.TestUtils;
import com.gigsky.tests.common.database.beans.DBConfigInfo;
import org.codehaus.jackson.util.TokenBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by haribongale on 12/12/16.
 */

public class DBConfigUtils {
    private static String DB_CONFIG_HOSTS_FILE_PATH = "dbconfiginfo.json";
    private static final Logger logger = LoggerFactory.getLogger(DBConfigUtils.class);
    private static Map<String, DBConfigInfo> mDBAccessInfoMap = new HashMap<String, DBConfigInfo>();



    @JsonSerialize
    private static class DBHostsList {
        List<DBConfigInfo> dbhosts;

        public List<DBConfigInfo> getDbhosts() {
            return dbhosts;
        }

        public void setDbhosts(List<DBConfigInfo> dbhosts) {
            this.dbhosts = dbhosts;
        }
    }

    /*
    public static void main(String[] args) throws IOException {
        logger.info("main");
        DBAccessInfo ftpClient = DBAccessUtil.getDBAccess(DB_HOST_NAMES.FTP_CLIENT);
        logger.info("ftpClient:"+ftpClient.getUrl());
    }*/

    public static DBConfigInfo getDBAccess(String dbName) {
        return mDBAccessInfoMap.get(dbName);
    }

    static {

        /*Load the allowable values*/
        /*for(DBNameUtils.DB_NAME enumVal : DBNameUtils.DB_NAME.values()) {
            mDBAccessInfoMap.put(enumVal.getValue(), null);
        }*/

        //Object to JSON in file
        DBHostsList dbHostsList = null;
        String filePath = ConfigKeyValueUtils.getValue(ConfigKeyValueUtils.KEYS.DB_CONFIG_INFO_FILE_PATH.name());
        Assert.assertNotNull(filePath, "DB Config File path is NOT Found");

        try {
            File file = new File(Thread.currentThread().getContextClassLoader().getResource(filePath).getFile());
            logger.info("====== DB Access Info ======");
            TestUtils.printContents(file);
            dbHostsList = JsonUtils.readValue(file, DBHostsList.class);
        } catch (IOException e) {
            logger.error("Exception: ", e);
            logger.error("ERROR: dbhosts file could not be parsed");
        }

        /*run through the DB config and build map*/
        Assert.assertNotNull(dbHostsList,"DB Hosts List Not found in file");
        Assert.assertNotNull(dbHostsList.getDbhosts(),"DB Hosts Not found in file");
        Assert.assertTrue(dbHostsList.getDbhosts().size() > 0);
        for (DBConfigInfo dbAccessInfo : dbHostsList.getDbhosts()) {
            //Assert.assertTrue(mDBAccessInfoMap.containsKey(dbAccessInfo.getSchema()), "DB Name: "+dbAccessInfo.getSchema()+" NOT Found");
            mDBAccessInfoMap.put(dbAccessInfo.getDbName(), dbAccessInfo);
        }
    }
}



