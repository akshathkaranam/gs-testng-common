package com.gigsky.tests.common.facebook.beans;

/**
 * Created by vishaljogi on 07/06/18.
 */
public class FacebookLoginBean {

    String username;
    String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
