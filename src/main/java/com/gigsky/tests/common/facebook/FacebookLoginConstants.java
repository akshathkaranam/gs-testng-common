package com.gigsky.tests.common.facebook;

/**
 * Created by vishaljogi on 07/06/18.
 */
public class FacebookLoginConstants {
    public static final String FB_LOGIN_URL = "https://www.facebook.com";
    public static final String FB_LOGIN_ACCESS_TOKEN_PAGE = "https://developers.facebook.com/tools/explorer/";
    public static final String FB_LOGIN_USERNAME_FIELDNAME = "email";
    public static final String FB_LOGIN_PASSWORD_FIELDNAME = "pass";
    public static final String FB_LOGIN_LOGIN_WEBELEMENT_ID = "login";

    public static final String FB_LOGIN_GET_ACCESS_TOKEN_WEBELEMENT_ID = "get_access_token";
    public static final String FB_LOGIN_GET_ACCESS_TOKEN_SELECTOR_CSS_ID = ".layerConfirm";
    public static final String FB_LOGIN_GET_ACCESS_TOKEN_POPUP_CONFIRM_ID = "_CONFIRM_";
    public static final String FB_LOGIN_ACCESS_TOKEN_TEXT_ID = "access_token";
    public static final String FB_LOGIN_ACCESS_TOKEN_XPATH = "//label/input[contains(@placeholder,'Get User Access Token')]";
    public static final String FB_LOGIN_ACCESS_TOKEN_FIELDNAME = "token";
    public static final String FB_LOGIN_ACCESS_TOKEN_ACCESSORNAME = "FacebookLoginResponse";
    public static final String FB_LOGIN_PAGE_XPATH = "//a[contains(@href,'https://www.facebook.com/login/')]";
}
