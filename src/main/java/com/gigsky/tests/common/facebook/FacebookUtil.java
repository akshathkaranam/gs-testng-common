package com.gigsky.tests.common.facebook;

import com.gigsky.tests.common.facebook.beans.FacebookLoginBean;
import com.gigsky.tests.common.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.AssertJUnit;

import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

/**
 * Created by vishaljogi on 07/06/18.
 */
public class FacebookUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(FacebookUtil.class);
    public static final FacebookUtil instance = new FacebookUtil();
    private static  final String workingDir = System.getProperty("user.dir");

    public static FacebookUtil getInstance() {
        return instance;
    }

    public String runStepFacebookLogin(FacebookLoginBean facebookLoginBean, boolean exceptionOnFailure) throws Exception {
        FirefoxDriver driver = null;

        AssertJUnit.assertTrue("facebooklogin xml element was not found in step\n", (facebookLoginBean != null));
        AssertJUnit.assertTrue("facebooklogin username and/or password is empty\n", (facebookLoginBean.getUsername().length() > 0)
                && (facebookLoginBean.getPassword().length() > 0));

        String errorMessage = "Failed to instantiate webdriver";
        Exception driverException=null;

        try {
            driver = new FirefoxDriver();
            driver.manage().window().setSize(new Dimension(1280, 1024));
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            int MAX_RETRY_COUNT=3;
            int RETRY_COUNT=0;
            String URL=null;
            WebElement element=null;

            /*errorMessage = "Failed to fetch page." + FacebookLoginConstants.FB_LOGIN_URL;
            driver.get(FacebookLoginConstants.FB_LOGIN_URL);*/

            errorMessage = "Failed to get main window handle.";
            final String mainWindowHandle = driver.getWindowHandle();

            while(RETRY_COUNT < MAX_RETRY_COUNT) {
                try {
                    LOGGER.info("Getting FB Page");
                    errorMessage = "Failed to load access token page: " + FacebookLoginConstants.FB_LOGIN_ACCESS_TOKEN_PAGE;
                    driver.get(FacebookLoginConstants.FB_LOGIN_ACCESS_TOKEN_PAGE);

                    LOGGER.info("Clicking on Login button");
                    errorMessage = "Failed to find login page: " + FacebookLoginConstants.FB_LOGIN_PAGE_XPATH;
                    wait.until((ExpectedConditions.visibilityOfElementLocated(By.xpath(FacebookLoginConstants.FB_LOGIN_PAGE_XPATH))));
                    element = driver.findElement(By.xpath(FacebookLoginConstants.FB_LOGIN_PAGE_XPATH));
                    element.click();

                    LOGGER.info("adding username on box");
                    errorMessage = "Failed to find user name field: " + FacebookLoginConstants.FB_LOGIN_USERNAME_FIELDNAME;
                    wait.until((ExpectedConditions.visibilityOfElementLocated(By.name(FacebookLoginConstants.FB_LOGIN_USERNAME_FIELDNAME))));
                    element = driver.findElement(By.name(FacebookLoginConstants.FB_LOGIN_USERNAME_FIELDNAME));

                    errorMessage = "Failed to send username text to browser: username= " + facebookLoginBean.getUsername();
                    element.sendKeys(facebookLoginBean.getUsername());
                    LOGGER.info("adding password on box");
                    errorMessage = "Failed to find password field: " + FacebookLoginConstants.FB_LOGIN_PASSWORD_FIELDNAME;
                    wait.until((ExpectedConditions.visibilityOfElementLocated(By.name(FacebookLoginConstants.FB_LOGIN_PASSWORD_FIELDNAME))));
                    element = driver.findElement(By.name(FacebookLoginConstants.FB_LOGIN_PASSWORD_FIELDNAME));

                    errorMessage = "Failed to send password text to browser. password= " + facebookLoginBean.getPassword();
                    element.sendKeys(facebookLoginBean.getPassword());

                    LOGGER.info("Clicking on Login button2");
                    errorMessage = "Failed to find login button: " + FacebookLoginConstants.FB_LOGIN_LOGIN_WEBELEMENT_ID;
                    wait.until((ExpectedConditions.visibilityOfElementLocated(By.name(FacebookLoginConstants.FB_LOGIN_LOGIN_WEBELEMENT_ID))));
                    element = driver.findElement(By.name(FacebookLoginConstants.FB_LOGIN_LOGIN_WEBELEMENT_ID));

                    errorMessage = "Failed to submit login button.";
                    element.click();
                    LOGGER.info("Searching for token box");
                    LOGGER.info("Fetching Access Token text box using xpath {}", FacebookLoginConstants.FB_LOGIN_ACCESS_TOKEN_TEXT_ID);
                    errorMessage = "Failed to find access token input text box: " + FacebookLoginConstants.FB_LOGIN_ACCESS_TOKEN_TEXT_ID;
                    wait.until((ExpectedConditions.visibilityOfElementLocated(By.xpath(FacebookLoginConstants.FB_LOGIN_ACCESS_TOKEN_XPATH))));
                    element = driver.findElement(By.xpath(FacebookLoginConstants.FB_LOGIN_ACCESS_TOKEN_XPATH));
                    LOGGER.info("element {}", element);

                    break;
                } catch (Exception de) {
                    LOGGER.info("RETRY_COUNT {}", RETRY_COUNT);
                    LOGGER.info(errorMessage);
                    SeleniumUtils.saveHtmlSource(driver, Paths.get(workingDir,("Facebook"+RETRY_COUNT+".html")).toString());
                    SeleniumUtils.saveScreenshot(driver, Paths.get(workingDir,("Facebook"+RETRY_COUNT+".png")).toString());
                    if(RETRY_COUNT < MAX_RETRY_COUNT)
                        RETRY_COUNT++;
                    else
                    {
                        driverException=de;
                        break;
                    }
                }
            }

            if(driverException!=null){
                throw driverException;
            }
            if(element != null) {
                errorMessage = "Failed to get access token from textbox";
                String token = element.getAttribute("value");
                return token;
            }else{
                throw new Exception("Failed to get element token text box");
            }


        }
        catch (Exception e) {

            LOGGER.error(e.toString());
            if (exceptionOnFailure == false) {
                //saveScreenshot(driver);
                //saveHtmlSource(driver,mOutputDirectory + "/" + mTestCaseName + "pageDumpAfterException.html");
                LOGGER.error(e.toString());
                AssertJUnit.assertTrue(errorMessage + "\n\nException message:\n" + e + "\n", false);
            }
            else {
                throw e;
            }
        }
        finally {
            if (driver != null) {
                driver.close();
                driver = null;
            }
        }
        return null;
    }


}
