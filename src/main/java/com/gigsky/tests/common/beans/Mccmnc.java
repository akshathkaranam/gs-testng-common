package com.gigsky.tests.common.beans;

/**
 * Created by jeyarajs on 08/12/16.
 */
public class Mccmnc {

    private int mcc;
    private int mnc;

    public int getMcc() {
        return mcc;
    }

    public void setMcc(int mcc) {
        this.mcc = mcc;
    }

    public int getMnc() {
        return mnc;
    }

    public void setMnc(int mnc) {
        this.mnc = mnc;
    }
}
