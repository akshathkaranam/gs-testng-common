package com.gigsky.tests.common.beans;

import com.gigsky.tests.common.calldatarecords.beans.FtpConfig;

import java.util.List;

/**
 * Created by haribongale on 16/12/16.
 */
/*
{
  "ftphosts": [
    {
      "name": "local_ftp_imsi_tdc",
      "url": "localhost",
      "port": "21",
      "username": "root",
      "password": "mypwd"
    }
  ]
}
 */
public class FtpHostsList {

    public List<FtpConfig> getFtphosts() {
        return ftphosts;
    }

    public void setFtphosts(List<FtpConfig> ftphosts) {
        this.ftphosts = ftphosts;
    }

    List<FtpConfig> ftphosts;

    public FtpConfig getFtpHostInfo(String ftpHostName) {

        if(ftphosts == null || ftphosts.size() <= 0) {
            return null;
        }

        for(FtpConfig ftpHostInfo : getFtphosts()) {
            if(ftpHostName.equalsIgnoreCase(ftpHostInfo.getName())) {
                return ftpHostInfo;
            }
        }

        return null;
    }
}
