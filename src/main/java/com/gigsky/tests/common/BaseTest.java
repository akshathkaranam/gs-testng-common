package com.gigsky.tests.common;

import org.testng.ITest;
import org.testng.ITestResult;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;

public class BaseTest implements ITest {

    public String testCaseName = null;

    @Override
    public String getTestName() {
        return testCaseName;
    }

//    @BeforeMethod(onlyForGroups = {"dataprovider"})
//    public void beforeMethod(Method method, Object[] testData, ITestResult result) {
//        this.testCaseName = method.getName() + "_" + testData[0];
//        result.getMethod().setDescription(testCaseName);}

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(Method method, Object[] testData, ITestResult result) {
        this.testCaseName = method.getName() + "_" + testData[0];
        result.getMethod().setDescription(testCaseName);
    }
}
