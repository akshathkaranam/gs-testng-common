package com.gigsky.tests.common.calldatarecords.datagenerator;

import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by karthik on 23/8/19
 */

/**
 *    groupBy = COUNTRY                         (mandatory)
 *    fromDate=yyyy-mm-dd                       (mandatory)
 *    toDate=yyyy-mm-dd                         (mandatory)
 *    startIndex = 0                            (mandatory)
 *    count = 10                                (mandatory)
 *    filters = filters (country and imsiTypes) (optional)
 *    sortBy=country | sims                     (mandatory)
 *    sortDirection= ASC|DESC                   (mandatory)
 *
 */

public class SimLocActivityResBuilder extends LocationBuilder {

    private static final int MAX_CDR_COUNT = 10;
    List<DBResultSet> resultSet = new ArrayList<>();
    private LocationActivityList locationActivityList = new LocationActivityList();

    private void generateResultSet(Long accountID,String group_by,List<String> countryList, List<String> imsiTypes,String fromdate,String todate){

        Function<EnrichedLocationUpdate, String> identity = null;
        Function<EnrichedLocationUpdate, String> getCountryName = (e) -> {
            return e.getCountryName();
        };

        switch (group_by.toLowerCase()) {
            case "country":
                identity = getCountryName;
                break;
        }
        try {

            final Long fromDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-zzz").parse(fromdate+"-UTC").getTime();
            final Long toDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-zzz").parse(todate+"-UTC").getTime();

            Map<String, List<EnrichedLocationUpdate>> countryWisegroupedCDRs;
            if(!countryList.isEmpty()&&!imsiTypes.isEmpty()) {
                countryWisegroupedCDRs = enrichedLUList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .filter(e -> countryList.contains(e.getCountryCode()))
                        .filter(e -> imsiTypes.contains(e.getImsiProfile()))
                        .collect(Collectors.groupingBy(identity));

            }else if(countryList.isEmpty() && imsiTypes.isEmpty()){
                countryWisegroupedCDRs = enrichedLUList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .collect(Collectors.groupingBy(identity));
//                System.out.println(list);


            }else if(imsiTypes.isEmpty()){
                countryWisegroupedCDRs = enrichedLUList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .filter(e -> countryList.contains(e.getCountryCode()))
                        .collect(Collectors.groupingBy(identity));
            }else{
                countryWisegroupedCDRs = enrichedLUList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .filter(e -> imsiTypes.contains(e.getImsiProfile()))
                        .collect(Collectors.groupingBy(identity));
            }

            for (Map.Entry<String, List<EnrichedLocationUpdate>> countryMap : countryWisegroupedCDRs.entrySet()) {
                resultSet.add(aggregateResultSet(countryMap.getValue()));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public DBResultSet aggregateResultSet(List<EnrichedLocationUpdate> cdrList){
        DBResultSet resultSet = new DBResultSet();
        HashSet<String> hashSet = new HashSet<>();
        for(EnrichedLocationUpdate enrichedCDR:cdrList){
            hashSet.add(enrichedCDR.getIccId());
        }
        Integer totalSimCount= hashSet.size();
        EnrichedLocationUpdate enrichedCDR = cdrList.get(0);
        resultSet.setSimCount(totalSimCount);
        resultSet.setCountryName(enrichedCDR.getCountryName());
        resultSet.setCountryCode(enrichedCDR.getCountryCode());
        return resultSet;
    }

    public void getSIMLocationActivityResponse(Long accountId , Map<String,Object> queryParams /*String groupBy, String sortBy, String sortByDirection, List<String> countryList, List<String> imsiTypes, int startIndex, int count, String fromDate, String toDate*/){
        String imsis= null;
        String countries =null;
        String groupBy = queryParams.get("groupBy").toString();
        String fromDate = queryParams.get("fromDate").toString();
        String toDate = queryParams.get("toDate").toString();
//        int count = (int)queryParams.get("count");
//        int startIndex = (int)queryParams.get("startIndex");
//        String sortByDirection = queryParams.get("sortDirection").toString();
//        String sortBy = queryParams.get("sortBy").toString();
        if(queryParams.get("filters")!=null) {
            String filterList = queryParams.get("filters").toString();
            if (filterList != null && filterList.contains("country") && filterList.contains("imsi")) {
                countries = queryParams.get("filters").toString().split("&")[0].substring(9);
                imsis = queryParams.get("filters").toString().split("&")[1].substring(10);

            } else if (filterList != null && filterList.contains("country")) {
                countries = queryParams.get("filters").toString().substring(9);
            } else {
                imsis = queryParams.get("filters").toString().substring(10);
            }
        }

        List<String> countryList;
        if(countries!=null){
            countryList =  Arrays.asList(countries.split(","));
        }else {
            countryList = new ArrayList<>();
        }
        List<String> imsiTypes;
        if(imsis!=null) {
            imsiTypes = Arrays.asList(imsis.split(","));
        }
        else {
            imsiTypes= new ArrayList<>();
        }
        generateResultSet(accountId,groupBy,countryList,imsiTypes,fromDate,toDate);
        List<DBResultSet> accountIdResultSet = new ArrayList<>(resultSet);

        List<LocationActivity> simCcdrResultList = null;

        int recordsCount=0;
        String lastAvailableDate = null;
        simCcdrResultList = DBResultToDaoResultConverter.convertToLU(accountIdResultSet,
                accountId,  fromDate, toDate);

        recordsCount = simCcdrResultList.size();

//        Comparator<LocationActivity> comparator = getComparator(sortByDirection,sortBy);
//        Collections.sort(simCcdrResultList, comparator);

//        ArrayList<LocationActivity> reqCount = new ArrayList<>();
//        for(int cnt=startIndex;simCcdrResultList.size()>cnt&&cnt<count+startIndex;cnt++){
//            reqCount.add(simCcdrResultList.get(cnt));
//        }
//
//        if(count>recordsCount){
//            count = recordsCount;
//        }

        locationActivityList.setList(simCcdrResultList);
        locationActivityList.setGroupBy(groupBy.toString());
//        locationActivityList.setTotalCount(recordsCount);
//        locationActivityList.setCount(count);
//        locationActivityList.setStartIndex(startIndex);
        locationActivityList.setFromDate(fromDate);
        locationActivityList.setToDate(toDate);

    }



    private Comparator<LocationActivity> getComparator( String sortByDirection,String sort_by){
        Comparator<LocationActivity> comparator = null;

        switch (sort_by.toLowerCase()) {
            case "country":
                if (sortByDirection.equals("ASC")) {
                    comparator = new Comparator<LocationActivity>() {
                        @Override
                        public int compare(LocationActivity o1, LocationActivity o2) {
                            return (o1.getCountry().getName().compareToIgnoreCase(o2.getCountry().getName()));
                        }
                    };

                } else {
                    comparator = new Comparator<LocationActivity>() {
                        @Override
                        public int compare(LocationActivity o1, LocationActivity o2) {
                            return (o2.getCountry().getName().compareToIgnoreCase(o1.getCountry().getName()));
                        }
                    };
                }break;
            case "sims":
                if (sortByDirection.equals("ASC")){
                    comparator = new Comparator<LocationActivity>() {
                        @Override
                        public int compare(LocationActivity o1, LocationActivity o2) {
                            return (o1.getSims()>o2.getSims()?1:0);
                        }
                    };

                } else {
                    comparator = new Comparator<LocationActivity>() {
                        @Override
                        public int compare(LocationActivity o1, LocationActivity o2) {
                            return (o1.getSims()>o2.getSims()?-1:0);
                        }
                    };
                }break;
        }

        return comparator;
    }

    public LocationActivityList getSimLocationActivity(){
        return locationActivityList;
    }

    public static List<LocationTestData> getLocUpdateList(Map<String,String> cdrParams) throws ParseException {
            String fromDate = cdrParams.get("fromDate");
            String toDate = cdrParams.get("toDate");
            //format: yyyy-mm-dd
            String iccId=cdrParams.get("iccId");
            String carrierName=cdrParams.get("carrierName");
            String countryCode = cdrParams.get("countryCode");
            String countryName= cdrParams.get("countryName");
            String mcc=cdrParams.get("mcc");
            String mnc =cdrParams.get("mnc");
            String imsi=cdrParams.get("imsi");
            String msisdn=cdrParams.get("msisdn");
            String imsiProfileName=cdrParams.get("imsiProfileName");
            String source = cdrParams.get("source");
            String connectionTechnologyType = cdrParams.get("connectionTechnologyType");
            String connectionType = cdrParams.get("connectionType");


            List <LocationTestData> cdrTestDataList = new ArrayList<>();
//            List<String> dayKeys = getDayKeysWithTimeBetweenDates(fromDate, toDate);

            cdrTestDataList.add(new LocationTestData().setIccId(iccId).setDayKey(toDate)
                        .setCountryCode(countryCode).setCountryName(countryName).setMcc(mcc).setMnc(mnc).setCarrierName(carrierName)
                        .setImsi(imsi).setMsisdn(msisdn).setImsiProfileName(imsiProfileName).setSource(source)
                        .setConnectionType(connectionType).setConnectionTechnologyType(connectionTechnologyType));


            return cdrTestDataList;
        }


    public static List<String> getDayKeysWithTimeBetweenDates(String fromDate, String toDate) throws ParseException {

        List<String> dayKeys = new ArrayList<>();

        String dayKey = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-zzz");
        Date startDate, endDate;
        startDate = sdf.parse(fromDate+"-UTC");
        endDate = sdf.parse(toDate+"-UTC");
        if(startDate.after(endDate)){
            System.out.println("Invalid from and to date");
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        int cdrCountPerDay;
        while(cal.getTime().before(endDate)|| cal.getTime().equals(endDate)){
            long millis = cal.getTimeInMillis();
            cdrCountPerDay = 1 + (int)((MAX_CDR_COUNT-1) * Math.random());
            for(int i = 0; i < cdrCountPerDay; i++) {
                millis += 305000;
                Date dateTime = new Date(millis);
                dayKey = sdf.format(dateTime);
                dayKeys.add(dayKey);
            }
            cal.add(Calendar.DATE, 1);
        }
        return dayKeys;
    }



}

