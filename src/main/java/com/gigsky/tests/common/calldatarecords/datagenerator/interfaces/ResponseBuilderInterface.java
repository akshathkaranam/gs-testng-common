package com.gigsky.tests.common.calldatarecords.datagenerator.interfaces;

import com.gigsky.tests.common.calldatarecords.datagenerator.TestAccountCDRConfig;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.GROUP_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_DIRECTION;

import java.util.List;
import java.util.Map;

/**
 * Created by apandit on 8/26/19.
 */
public interface ResponseBuilderInterface {
    public void generateResultSet(Object id, GROUP_BY groupByType);
    public Object getAccountDataUsageResponse(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection, int startIndex, int count, String fromDate, String toDate, List<TestAccountCDRConfig> accountCDRConfigList);
    public Object getSimDataUsageResponse(String iccId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection, int startIndex, int count, String fromDate, String toDate, List<TestAccountCDRConfig> accountCDRConfigList);
    public Object getUserDataUsageResponse(Long userId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection, int startIndex, int count, String fromDate, String toDate, List<TestAccountCDRConfig> accountCDRConfigList);

    public AccountDataUsage getAccountDataUsageDistributedBySims(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection,
                                                                 int startIndex, int count, String fromDate, String toDate,
                                                                 List<TestAccountCDRConfig> accountCDRConfigList,
                                                                 GetEnterpriseAccountDetails account, GetUserListOfEntAccount userDetails);
    public AccountDataUsage getAccountDataUsageDistributedByUsers(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection,
                                                                  int startIndex, int count, String fromDate, String toDate,
                                                                  List<TestAccountCDRConfig> accountCDRConfigList,
                                                                  GetEnterpriseAccountDetails account, GetUserListOfEntAccount userDetails);
    public AccountDataUsage getAccountDataUsageDistributedByZones(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection,
                                                                  int startIndex, int count, String fromDate, String toDate,
                                                                  List<TestAccountCDRConfig> accountCDRConfigList,
                                                                  GetZoneListOfAnAcc accountZoneList, GetEnterpriseAccountDetails account);
    public AccountDataUsage getAccountDataUsageDistributedByGroups(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection,
                                                                   int startIndex, int count, String fromDate, String toDate,
                                                                   List<TestAccountCDRConfig> accountCDRConfigList,
                                                                   GetEnterpriseAccountDetails account, List<GetEnterpriseAccountDetails> accountList);
}
