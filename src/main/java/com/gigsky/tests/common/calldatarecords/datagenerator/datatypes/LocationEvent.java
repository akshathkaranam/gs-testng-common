package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;


import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


/* Created on 20/07/2019 */

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)

public class LocationEvent {


    @JsonProperty
    private String type = "LocationEvent";

    @JsonProperty
    private String mcc;
    @JsonProperty
    private String mnc;
    @JsonProperty
    private String carrierName;
    @JsonProperty
    private Long accountId;
    @JsonProperty
    private String accountName;
    @JsonProperty
    private String imsi;
    @JsonProperty
    private String msisdn;
    @JsonProperty
    private String iccId;
    @JsonProperty
    private String imsiProfile;
    @JsonProperty
    private String timeAtGSBackend ;
    @JsonProperty
    private String connectionType;
    @JsonProperty
    private String source;
    @JsonProperty
    private  String connectionTechnologyType;

    @JsonProperty
    private String locationTime;
    @JsonProperty
    private List<CountryList> countryList;

    public String getTimeAtGSBackend() {
        return timeAtGSBackend;
    }

    public void setTimeAtGSBackend(String timeAtGSBackend) {
        this.timeAtGSBackend = timeAtGSBackend;
    }

    public String getLocationTime() {
        return locationTime;
    }

    public void setLocationTime(String locationTime) {
        this.locationTime = locationTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getMnc() {
        return mnc;
    }

    public void setMnc(String mnc) {
        this.mnc = mnc;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }


    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getImsiProfile() {
        return imsiProfile;
    }

    public void setImsiProfile(String imsiProfile) {
        this.imsiProfile = imsiProfile;
    }

    public List<CountryList> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<CountryList> countryList) {
        this.countryList = countryList;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getConnectionTechnologyType() {
        return connectionTechnologyType;
    }

    public void setConnectionTechnologyType(String connectionTechnologyType) {
        this.connectionTechnologyType = connectionTechnologyType;
    }
//    public String getsGWAddress() {
//        return sGWAddress;
//    }
//
//    public void setsGWAddress(String sGWAddress) {
//        this.sGWAddress = sGWAddress;
//    }
//    public String getTimeAtGSBackend() {
//        return timeAtGSBackend;
//    }
//
//    public void setTimeAtGSBackend(String timeAtGSBackend) {
//        this.timeAtGSBackend = timeAtGSBackend;
//    }
//        public Long getLocalSequenceNumber() {
//        return localSequenceNumber;
//    }
//
//    public void setLocalSequenceNumber(Long localSequenceNumber) {
//        this.localSequenceNumber = localSequenceNumber;
//    }
//    public Long getRecordDurationInSec() {
//        return recordDurationInSec;
//    }
//
//    public void setRecordDurationInSec(Long recordDurationInSec) {
//        this.recordDurationInSec = recordDurationInSec;
//    }
//
//    public String getNodeID() {
//        return nodeID;
//    }
//
//    public void setNodeID(String nodeID) {
//        this.nodeID = nodeID;
//    }
//    public String getServingNodePLMNIdentifier() {
//        return servingNodePLMNIdentifier;
//    }
//
//    public void setServingNodePLMNIdentifier(String servingNodePLMNIdentifier) {
//        this.servingNodePLMNIdentifier = servingNodePLMNIdentifier;
//    }
//    public String getPdpPDNType() {
//        return pdpPDNType;
//    }
//
//    public void setPdpPDNType(String pdpPDNType) {
//        this.pdpPDNType = pdpPDNType;
//    }
//    public String getServedPDPPDNAddress() {
//        return servedPDPPDNAddress;
//    }
//
//    public void setServedPDPPDNAddress(String servedPDPPDNAddress) {
//        this.servedPDPPDNAddress = servedPDPPDNAddress;
//    }
//public Long getChargingID() {
//    return chargingID;
//}
//
//    public void setChargingID(Long chargingID) {
//        this.chargingID = chargingID;
//    }

}
