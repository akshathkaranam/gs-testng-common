package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * created by karthik on 11/6/19
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationInfo extends BaseEvent implements Comparable<LocationInfo>{
    protected String iccId;
    protected String mcc;
    protected String mnc;
    protected String msisdn;
    protected String imsi;
    protected String connectionTechnologyType;
    protected String connectionType;
    protected String source;
    protected Long locationTime;
    protected Long locId;
    protected Long timeAtGSBackend;
    protected String imsiProfile;
    protected Long recordStartTime;

    public Long getRecordStartTime() { return recordStartTime; }

    public void setRecordStartTime(Long recordStartTime) { this.recordStartTime = recordStartTime; }

    public String getImsiProfile() {
        return imsiProfile;
    }

    public void setImsiProfile(String imsiProfileName) {
        this.imsiProfile = imsiProfileName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getImsi() { return imsi; }

    public void setImsi(String imsi) { this.imsi = imsi; }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getConnectionTechnologyType() {
        return connectionTechnologyType;
    }

    public void setConnectionTechnologyType(String connectionTechnologyType) {
        this.connectionTechnologyType = connectionTechnologyType;
    }

    public Long getTimeAtGSBackend() {
        return timeAtGSBackend;
    }

    public void setTimeAtGSBackend(Long timeAtGSBackend) {
        this.timeAtGSBackend = timeAtGSBackend;
    }

    public Long getLocationTime() {
        return locationTime;
    }

    public void setLocationTime(Long locationTime) {
        this.locationTime = locationTime;
    }
    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccid) {
        this.iccId = iccid;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getMnc() {
        return mnc;
    }

    public void setMnc(String mnc) {
        this.mnc = mnc;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    public Long getLocId() {
        return locId;
    }

    public void setLocId(Long locid) {
        this.locId = locid;
    }

    public void copyInfo(LocationInfo locationInfo){
        this.iccId = locationInfo.getIccId();
        this.mcc = locationInfo.getMcc();
        this.mnc = locationInfo.getMnc();
        this.msisdn = locationInfo.getMsisdn();
        this.connectionTechnologyType = locationInfo.getConnectionTechnologyType();
        this.connectionType = locationInfo.getConnectionType();
        this.timeAtGSBackend = locationInfo.getTimeAtGSBackend();
        this.locId=locationInfo.getLocId();
        this.eventTime = locationInfo.getEventTime();
        this.ingestionTime = locationInfo.getIngestionTime();
        this.source = locationInfo.getSource();
        this.locationTime = locationInfo.getLocationTime();
        this.imsi = locationInfo.getImsi();
        this.imsiProfile = locationInfo.getImsiProfile();
        this.recordStartTime = locationInfo.getRecordStartTime();

    }

    public LocationInfo(){
        super("LocationInfo");
    }
    @Override
    public String toString() {
        return "LocationInfo{" +
                " eventTime=" + eventTime +
                " connectionType=" + connectionType + '\'' +
                ", iccId=" + iccId +
                ", mcc='" + mcc + '\'' +
                ", mnc='" + mnc + '\'' +
                ", imsi=" + imsi +
                ", imsiProfile=" + imsiProfile +
                ", createTime='" + timeAtGSBackend + '\'' +
                ", locationTime=" + locationTime +
                '}';
    }

    @Override
    public int compareTo(LocationInfo other) {
        return Long.compare(this.eventTime, other.eventTime);
    }


}
