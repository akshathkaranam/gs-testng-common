package com.gigsky.tests.common.calldatarecords.impl;

import com.gigsky.cdrmodule.psm.decoder.PSMGPRSRecord;
import com.gigsky.cdrmodule.psm.encoder.PSMCDREncoder;
import com.gigsky.tests.common.calldatarecords.beans.CDRData;
import com.gigsky.tests.common.calldatarecords.beans.CDRInfo;
import com.gigsky.tests.common.calldatarecords.beans.FtpConfig;
import com.gigsky.tests.common.calldatarecords.beans.FtpData;
import com.gigsky.tests.common.utils.CDRUtils;
import com.gigsky.tests.common.utils.FtpUtils;
import org.apache.commons.net.ftp.FTP;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

/**
 * Created by jeyarajs on 08/12/16.
 */
public class CDRSender {

    public static final CDRSender instance = new CDRSender();
    public static final Random random = new Random();

    private CDRSender() {

    }

    public static CDRSender getInstance() {
        return instance;
    }

    public void sendCDRs(CDRInfo cdrInfo) throws Exception {
        List<PSMGPRSRecord> cdrRecordList = new LinkedList<PSMGPRSRecord>();
        for (CDRData cdrData : cdrInfo.getCdrDataList()) {
            cdrRecordList.add(formCDRRecord(cdrData));
        }
        uploadCDRsToFtp(cdrRecordList, cdrInfo.getFtpConfig());
    }

    public void clearCDRs(FtpConfig ftpConfig) throws Exception {
        FtpUtils.deleteAllFiles(getFtpData(ftpConfig, null));
    }

    public enum CDR_TYPE{
        NORMAL("NORMAL"),
        CAPTIVE("CAPTIVE");

        private String cdrType;
        CDR_TYPE(String cdrType) {
            this.cdrType = cdrType;
        }

        public String getCdrType(){return cdrType;}
    }

    private PSMGPRSRecord formCDRRecord(CDRData cdrData) {
        PSMGPRSRecord gprsRecord = new PSMGPRSRecord();
        gprsRecord.setServedIMSI(cdrData.getImsi());
        gprsRecord.setServedMSISDN(cdrData.getMsisdn());
        gprsRecord.setRecordOpeningTime(cdrData.getRecordOpeningTime());
        gprsRecord.setRecordDurationInSecs(cdrData.getRecordDurationInSeconds());
        gprsRecord.setMcc(cdrData.getMccmnc().getMcc());
        gprsRecord.setMnc(cdrData.getMccmnc().getMnc());
        gprsRecord.setCdrClosureReason(cdrData.getCdrClosureReason());
        gprsRecord.setAccessPointName(cdrData.getAccessPointName());
        gprsRecord.setNodeID(cdrData.getNodeId());
        gprsRecord.setServedIMEI(cdrData.getImei());
        long timeUsed = System.currentTimeMillis() + random.nextInt(50000);
        gprsRecord.setLocalSequenceNumber(timeUsed);
        PSMGPRSRecord.ServiceVolume serviceVolume =
                new PSMGPRSRecord.ServiceVolume();
        serviceVolume.downlinkVolume = cdrData.getDownlinkVolume();
        serviceVolume.uplinkVolume = cdrData.getUplinkVolume();
        serviceVolume.trafficType = cdrData.getTrafficType();
        PSMGPRSRecord.ManagementExtension mgmtExt =
                new PSMGPRSRecord.ManagementExtension();
        mgmtExt.psmCdrExtension = new ArrayList<PSMGPRSRecord.ServiceVolume>();
        mgmtExt.psmCdrExtension.add(serviceVolume);
        List<PSMGPRSRecord.ManagementExtension> mgmtExtList =
                new ArrayList<PSMGPRSRecord.ManagementExtension>();
        mgmtExtList.add(mgmtExt);
        gprsRecord.setManagementExtensionList(mgmtExtList);
        return gprsRecord;
    }

    private void uploadCDRsToFtp(List<PSMGPRSRecord> cdrList,
                                 FtpConfig ftpConfig) throws Exception {
        PSMCDREncoder.PSMEncodedStream stream;
        stream = generateCDRStream(cdrList);
        InputStream in = new ByteArrayInputStream(stream.outputStream);
        try {
            FtpData ftpData = getFtpData(ftpConfig, stream.fileName);
            FtpUtils.storeFile(ftpData, in);
        } finally {
            in.close();
        }
    }

    private FtpData getFtpData(FtpConfig ftpConfig,
                               String fileName) {
        FtpData ftpData = new FtpData();
        ftpData.setFtpConfig(ftpConfig);
        ftpData.setFileName(fileName);
        ftpData.setConnectTimeout(7 * 1000);
        ftpData.setDefaultTimeout(7 * 1000);
        ftpData.setDataTimeout(60 * 1000);
        ftpData.setFileType(FTP.BINARY_FILE_TYPE);
        return ftpData;
    }

    private PSMCDREncoder.PSMEncodedStream generateCDRStream(List<PSMGPRSRecord> aCDRList) throws Exception {
        PSMCDREncoder encoder = new PSMCDREncoder();
        Date d = new Date();
        long number = d.getTime();
        PSMCDREncoder.PSMEncodedStream encodeStream =
                encoder.generateCDRStream(aCDRList, number);
        return encodeStream;
    }

    private Calendar getCurrentUtcTime(){
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        timeZone.setDefault(TimeZone.getTimeZone("UTC"));
        return Calendar.getInstance(timeZone);
    }

    public CDRData generateCDRFile(Long imsi, Long msisdn, int mcc, int mnc, int uplinkVolumeInBytes, int downlinkVolumeInBytes, Calendar recordOpeningTime) {
        long recordDurationInSecs = 80l;
        String trafficType = "NORMAL";
        long imei = 80000l;
        CDRData cdrData = CDRUtils.generateCDRData(imsi, msisdn, mcc, mnc, uplinkVolumeInBytes, downlinkVolumeInBytes, recordDurationInSecs, trafficType, imei, recordOpeningTime);
        return cdrData;
    }

    public CDRData generateCDRFile(String imsi, String msisdn, String mcc, String mnc, String uplinkVolumeInBytes, String downlinkVolumeInBytes, Calendar recordOpeningTime) {
        long recordDurationInSecs = 80l;
        String trafficType = "NORMAL";
        long imei = 80000l;
        CDRData cdrData = CDRUtils.generateCDRData(Long.valueOf(imsi), Long.valueOf(msisdn), Integer.valueOf(mcc), Integer.valueOf(mnc), Integer.valueOf(uplinkVolumeInBytes), Integer.valueOf(downlinkVolumeInBytes),
                recordDurationInSecs, trafficType, imei, recordOpeningTime);
        return cdrData;
    }

    public CDRData generateCDRFile(String imsi, String msisdn, String mcc, String mnc, String uplinkVolumeInBytes, String downlinkVolumeInBytes, Calendar recordOpeningTime, CDR_TYPE cdrType) {
        long recordDurationInSecs = 80l;
        String trafficType = cdrType.getCdrType();
        long imei = 80000l;
        CDRData cdrData = CDRUtils.generateCDRData(Long.valueOf(imsi), Long.valueOf(msisdn), Integer.valueOf(mcc), Integer.valueOf(mnc), Integer.valueOf(uplinkVolumeInBytes), Integer.valueOf(downlinkVolumeInBytes),
                recordDurationInSecs, trafficType, imei, recordOpeningTime);
        return cdrData;
    }


    public CDRData generateCDRFile(String imsi, String msisdn, String mcc, String mnc, String uplinkVolumeInBytes, String downlinkVolumeInBytes) {
        long recordDurationInSecs = 80l;
        String trafficType = "NORMAL";
        long imei = 80000l;
        CDRData cdrData = CDRUtils.generateCDRData(Long.valueOf(imsi), Long.valueOf(msisdn), Integer.valueOf(mcc), Integer.valueOf(mnc), Integer.valueOf(uplinkVolumeInBytes), Integer.valueOf(downlinkVolumeInBytes),
                recordDurationInSecs, trafficType, imei, getCurrentUtcTime());
        return cdrData;
    }

    public void sendBulkCDR(List<CDRData> cdrDataList) throws Exception {
        CDRSender cdrSender = CDRSender.getInstance();
        CDRInfo cdrInfo = CDRUtils.generateCDRInfo(FtpUtils.FTP_HOSTS.FTP_IMSI_TDC, cdrDataList);
        cdrSender.sendCDRs(cdrInfo);

    }
}
