package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "minimumCommitment",
        "list"
})
public class BucketPricingInfo {

    @JsonProperty("minimumCommitment")
    private Integer minimumCommitment;
    @JsonProperty("list")
    private java.util.List<BucketPricingList> list = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("minimumCommitment")
    public Integer getMinimumCommitment() {
        return minimumCommitment;
    }

    @JsonProperty("minimumCommitment")
    public void setMinimumCommitment(Integer minimumCommitment) {
        this.minimumCommitment = minimumCommitment;
    }

    @JsonProperty("list")
    public java.util.List<BucketPricingList> getList() {
        return list;
    }

    @JsonProperty("list")
    public void setList(java.util.List<BucketPricingList> list) {
        this.list = list;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
