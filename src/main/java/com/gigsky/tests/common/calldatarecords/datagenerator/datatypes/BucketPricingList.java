package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "bucketId",
        "unitType",
        "unitValue",
        "pricePerUnit",
        "startRange",
        "endRange",
        "status"
})
public class BucketPricingList {

    @JsonProperty("bucketId")
    private Integer bucketId;
    @JsonProperty("unitType")
    private String unitType;
    @JsonProperty("unitValue")
    private String unitValue;
    @JsonProperty("pricePerUnit")
    private String pricePerUnit;
    @JsonProperty("startRange")
    private String startRange;
    @JsonProperty("endRange")
    private String endRange;
    @JsonProperty("status")
    private String status;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("bucketId")
    public Integer getBucketId() {
        return bucketId;
    }

    @JsonProperty("bucketId")
    public void setBucketId(Integer bucketId) {
        this.bucketId = bucketId;
    }

    @JsonProperty("unitType")
    public String getUnitType() {
        return unitType;
    }

    @JsonProperty("unitType")
    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    @JsonProperty("unitValue")
    public String getUnitValue() {
        return unitValue;
    }

    @JsonProperty("unitValue")
    public void setUnitValue(String unitValue) {
        this.unitValue = unitValue;
    }

    @JsonProperty("pricePerUnit")
    public String getPricePerUnit() {
        return pricePerUnit;
    }

    @JsonProperty("pricePerUnit")
    public void setPricePerUnit(String pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    @JsonProperty("startRange")
    public String getStartRange() {
        return startRange;
    }

    @JsonProperty("startRange")
    public void setStartRange(String startRange) {
        this.startRange = startRange;
    }

    @JsonProperty("endRange")
    public String getEndRange() {
        return endRange;
    }

    @JsonProperty("endRange")
    public void setEndRange(String endRange) {
        this.endRange = endRange;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
