package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/* Created on 20/07/2019 */

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class CountryList {
    @JsonProperty
    private String type = "Country";
    @JsonProperty
    private String code;
    @JsonProperty
    private String name ;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}


