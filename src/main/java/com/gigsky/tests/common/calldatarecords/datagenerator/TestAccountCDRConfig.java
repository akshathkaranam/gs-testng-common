package com.gigsky.tests.common.calldatarecords.datagenerator;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by apandit on 07/29/18.
 */
public class TestAccountCDRConfig {

    List<SimData> simDataList = new ArrayList<>();
    private Long accountId;
    private Long subAccountId;
    private String timeZone = "+00:00";
    private int cdrCount = 0;

    public TestAccountCDRConfig(Long accountId) throws ParseException {
        this.accountId = accountId;
        this.subAccountId = accountId;
    }

    public TestAccountCDRConfig(Long accountId, Long subAccountId) throws ParseException {
        this.accountId = accountId;
        this.subAccountId = subAccountId;
    }


    public TestAccountCDRConfig(Long accountId, int cdrCount){
        this.accountId = accountId;
        this.cdrCount = cdrCount;
        this.subAccountId = accountId;
    }

    public TestAccountCDRConfig(Long accountId, String timeZone) throws ParseException {
        this.accountId = accountId;
        this.timeZone = timeZone;
        this.subAccountId = accountId;
    }

    public void setAccountDataUsage(List<CDRTestData> cdrDataList) throws ParseException {
        for(CDRTestData cdrTestData: cdrDataList){
                addSimUsage(cdrTestData);
            }
    }

    public void setLocationInfo(List<LocationTestData> cdrDataList) throws ParseException {
        for(LocationTestData cdrTestData: cdrDataList){
            addSimUsageLoc(cdrTestData);
        }
    }

    public void setAccountDataUsage(List<CDRTestData> cdrDataList, int cdrCount) throws ParseException {
        int count = 0;
        for(CDRTestData cdrTestData: cdrDataList){

            if(cdrCount !=0){
                if(count < cdrCount) {
                    addSimUsage(cdrTestData);
                    count++;
                }
            }
            else{
                addSimUsage(cdrTestData);
            }

        }
    }


   public void addSimUsage(CDRTestData cdrTestData) throws ParseException {
       SimData simData = new SimData(cdrTestData);
       simData.setAccountId(this.accountId);
       simData.setTimeZone(this.timeZone);
//       simData
       if(simData.getSubAccountId()==null) {
           simData.setSubAccountId(this.subAccountId);
       }
       simData.setIccId(simData.getIccId());
       simDataList.add(simData);
   }

    public void addSimUsageLoc(LocationTestData cdrTestData) throws ParseException {
        SimData simData = new SimData(cdrTestData);
        simData.setAccountId(this.accountId);
        simData.setTimeZone(this.timeZone);
//       simData
        if(simData.getSubAccountId()==null) {
            simData.setSubAccountId(this.subAccountId);
        }
        simData.setIccId(simData.getIccId());
        simDataList.add(simData);
    }

    public Long getSubAccountId(){ return subAccountId;}

   public List<SimData> getSimDataList(){
       return simDataList;
   }

   public void clearAccountDataUsage(){
       simDataList.clear();
   }

}
