package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "alertEnabled ",
        "alertIntervalInSecs",
        "alertEmailForAdminEnabled",
        "alertEmailForUserEnabled",
        "defaultCreditLimitForSims"
})
public class AlertSetting {

    @JsonProperty("type")
    private String type;
    @JsonProperty("alertEnabled ")
    private String alertEnabled;
    @JsonProperty("alertIntervalInSecs")
    private Integer alertIntervalInSecs;
    @JsonProperty("alertEmailForAdminEnabled")
    private String alertEmailForAdminEnabled;
    @JsonProperty("alertEmailForUserEnabled")
    private String alertEmailForUserEnabled;
    @JsonProperty("defaultCreditLimitForSims")
    private List<DefaultCreditLimitForSim> defaultCreditLimitForSims = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("alertEnabled ")
    public String getAlertEnabled() {
        return alertEnabled;
    }

    @JsonProperty("alertEnabled ")
    public void setAlertEnabled(String alertEnabled) {
        this.alertEnabled = alertEnabled;
    }

    @JsonProperty("alertIntervalInSecs")
    public Integer getAlertIntervalInSecs() {
        return alertIntervalInSecs;
    }

    @JsonProperty("alertIntervalInSecs")
    public void setAlertIntervalInSecs(Integer alertIntervalInSecs) {
        this.alertIntervalInSecs = alertIntervalInSecs;
    }

    @JsonProperty("alertEmailForAdminEnabled")
    public String getAlertEmailForAdminEnabled() {
        return alertEmailForAdminEnabled;
    }

    @JsonProperty("alertEmailForAdminEnabled")
    public void setAlertEmailForAdminEnabled(String alertEmailForAdminEnabled) {
        this.alertEmailForAdminEnabled = alertEmailForAdminEnabled;
    }

    @JsonProperty("alertEmailForUserEnabled")
    public String getAlertEmailForUserEnabled() {
        return alertEmailForUserEnabled;
    }

    @JsonProperty("alertEmailForUserEnabled")
    public void setAlertEmailForUserEnabled(String alertEmailForUserEnabled) {
        this.alertEmailForUserEnabled = alertEmailForUserEnabled;
    }

    @JsonProperty("defaultCreditLimitForSims")
    public List<DefaultCreditLimitForSim> getDefaultCreditLimitForSims() {
        return defaultCreditLimitForSims;
    }

    @JsonProperty("defaultCreditLimitForSims")
    public void setDefaultCreditLimitForSims(List<DefaultCreditLimitForSim> defaultCreditLimitForSims) {
        this.defaultCreditLimitForSims = defaultCreditLimitForSims;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
