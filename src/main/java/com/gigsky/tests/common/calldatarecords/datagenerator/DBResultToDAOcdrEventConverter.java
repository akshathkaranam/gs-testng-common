package com.gigsky.tests.common.calldatarecords.datagenerator;

import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;

import java.text.SimpleDateFormat;
import java.util.*;

public class DBResultToDAOcdrEventConverter {

    public static List<CDREvent>convert(List<DBResultSet> dbResultSets,String accountName,String iccid,String countryName){
        List<CDREvent> cdrEventList = new ArrayList<>();

//        for(DBResultSet dbResultSet : dbResultSets){
            for(EnrichedCDR enrichedCDR: dbResultSets.get(0).getEnrichedCDRList()){
//                System.out.println(enrichedCDR.getCountryName()+countryName);
//                System.out.println(enrichedCDR.getIccId() + "--"+iccid);
                if(enrichedCDR.getIccId().equals(iccid) && enrichedCDR.getCountryName().equals(countryName)) {
                    CDREvent cdrEvent = new CDREvent();
                    CountryList country = new CountryList();
                    cdrEvent.setAccessPointName(enrichedCDR.getAccessPointName());
                    cdrEvent.setAccountId(enrichedCDR.getEnterpriseAccountId());
                    cdrEvent.setAccountName(accountName);
                    cdrEvent.setCarrierName(enrichedCDR.getCarrierName());
                    cdrEvent.setCdrUsageType(enrichedCDR.getCdrType());
                    cdrEvent.setMsisdn(enrichedCDR.getMsisdn());
                    cdrEvent.setCdrTrafficType(enrichedCDR.getCdrTrafficType());
//                cdrEvent.setChargingID(enrichedCDR.getChargingID());
                    cdrEvent.setDownlinkVolume(enrichedCDR.getDownlinkVolume());
                    cdrEvent.setUplinkVolume(enrichedCDR.getUplinkVolume());
                    cdrEvent.setRecordStartTime(convEpochToDate(enrichedCDR.getRecordStartTime()));
                    cdrEvent.setIccId(enrichedCDR.getIccId());
                    cdrEvent.setImsi(enrichedCDR.getImsi());
//                cdrEvent.setLocalSequenceNumber(enrichedCDR.getLocalSequenceNumber());
                    cdrEvent.setMcc(enrichedCDR.getMcc());
                    cdrEvent.setMnc(enrichedCDR.getMnc());
                    cdrEvent.setDataInBytes(enrichedCDR.getUplinkVolume(), enrichedCDR.getDownlinkVolume());
                    cdrEvent.setImsiProfile(enrichedCDR.getImsiProfile());
                    country.setName(enrichedCDR.getCountryName());
                    country.setCode(enrichedCDR.getCountryCode());
                    cdrEvent.setCountryList(new ArrayList<>(Arrays.asList(country)));
                    cdrEventList.add(cdrEvent);
                }

            }

//        }
        return cdrEventList.size()>0? cdrEventList:null;
    }

    public static List<LocationEvent>convertToLU(List<DBResultSet> dbResultSets,String accountName,String iccid,String countryName){
        List<LocationEvent> locationEventList = new ArrayList<>();

//        for(DBResultSet dbResultSet : dbResultSets){
        for(EnrichedLocationUpdate enrichedLocationUpdate: dbResultSets.get(0).getEnrichedLocationUpdateList()){
//                System.out.println(enrichedCDR.getCountryName()+countryName);
//                System.out.println(enrichedCDR.getIccId() + "--"+iccid);
            if(enrichedLocationUpdate.getIccId().equals(iccid) && enrichedLocationUpdate.getCountryName().equals(countryName)) {
                LocationEvent locationEvent = new LocationEvent();
                CountryList country = new CountryList();
                locationEvent.setAccountId(enrichedLocationUpdate.getEnterpriseAccountId());
                locationEvent.setAccountName(accountName);
                locationEvent.setCarrierName(enrichedLocationUpdate.getCarrierName());
                locationEvent.setMsisdn(enrichedLocationUpdate.getMsisdn());
                locationEvent.setConnectionType(enrichedLocationUpdate.getConnectionType());
                locationEvent.setLocationTime(convEpochToDate(enrichedLocationUpdate.getRecordStartTime()));
                locationEvent.setIccId(enrichedLocationUpdate.getIccId());
                locationEvent.setImsi(enrichedLocationUpdate.getImsi());
//                locationEvent.setLocalSequenceNumber(enrichedLocationUpdate.getLocalSequenceNumber());
                locationEvent.setMcc(enrichedLocationUpdate.getMcc());
                locationEvent.setMnc(enrichedLocationUpdate.getMnc());
                locationEvent.setConnectionTechnologyType(enrichedLocationUpdate.getConnectionTechnologyType());
                locationEvent.setSource(enrichedLocationUpdate.getSource());
                locationEvent.setImsiProfile(enrichedLocationUpdate.getImsiProfile());
                country.setName(enrichedLocationUpdate.getCountryName());
                country.setCode(enrichedLocationUpdate.getCountryCode());
                locationEvent.setCountryList(new ArrayList<>(Arrays.asList(country)));
                locationEventList.add(locationEvent);
            }

        }

//        }
        return locationEventList.size()>0? locationEventList:null;
    }
    public static String convEpochToDate(Long millis){
        Date date = new Date(millis);
        // format of the date
        SimpleDateFormat jdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        jdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return jdf.format(date);
    }
}
