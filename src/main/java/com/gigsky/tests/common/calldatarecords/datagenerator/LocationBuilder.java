package com.gigsky.tests.common.calldatarecords.datagenerator;

import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.EnrichedLocationUpdate;

import java.util.*;


/**
 * Created by karthik on 23/9/19
 */
public class LocationBuilder {

    private String connectionType = "CA";
    protected List<EnrichedLocationUpdate> enrichedLUList = new ArrayList<>();
    protected List<TestAccountCDRConfig> testAccountCDRConfigs;

    public void parse(List<TestAccountCDRConfig> accountCDRConfigList) {
        testAccountCDRConfigs = accountCDRConfigList;
        EnrichedLocationUpdate enrichedLocationUpdate = null;
        Long currentTime = System.currentTimeMillis();
        String iccId;
        Long userId, timeStamp;
        Long locId = 1048660l;
        String imsi, msisdn, zoneName;


        for (TestAccountCDRConfig accountConfig : accountCDRConfigList) {
            for (SimData simData : accountConfig.getSimDataList()) {

                userId = simData.getUserId();
                iccId = simData.getIccId();
                imsi = simData.getImsi();
                msisdn = simData.getMsisdn();
                timeStamp = simData.getTimeStamp();

                enrichedLocationUpdate = new EnrichedLocationUpdate(timeStamp, "UPSERT", locId++, iccId, imsi, msisdn,
                        simData.getMcc(), simData.getMnc(), connectionType, timeStamp, simData.groupId, simData.accountId,
                     userId, null, simData.countryCode, simData.countryName, simData.imsiProfileName, simData.getTimeZone());

//                (Long timestamp, String operationType, Long locationId,   String iccId, String imsi, String msisdn, String mcc,
//                        String mnc, Long startTime, Long groupAccountId, Long enterpriseAccountId, \Long userId, Long roamingProfileId,
//                        String countryCode, String countryName, String imsiProfileName, String timeZone)
                enrichedLocationUpdate.setProcessTime(timeStamp);
                enrichedLocationUpdate.setCarrierName(simData.getCarrierName());
                enrichedLocationUpdate.setIngestionTime(currentTime);
                enrichedLocationUpdate.setImsiProfile(simData.getImsiProfileName());
                enrichedLocationUpdate.setInitialDelayTimeMs(simData.getInitialDelayMs());
                enrichedLUList.add(enrichedLocationUpdate);

            }
        }
    }

    public List<EnrichedLocationUpdate> getEnrichedLUList() {
        return enrichedLUList;
    }

}


