package com.gigsky.tests.common.calldatarecords.datagenerator;

import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.GROUP_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_DIRECTION;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by vinayr on 05/03/18.
 */
public class CommonUtil {

    private static final Logger logger = LoggerFactory.getLogger(CommonUtil.class);
    private static final int LAST_DAY_OF_YEAR = 31;

    private static final String MONTH_KEY_FORMAT = "yyyy-MM";
    private static final String WEEK_KEY_FORMAT = "yyyy-ww";
    private static final String DAY_KEY_FORMAT = "yyyy-MM-dd";
    private static final int WEEK_NUMBER_INDEX = 4;

    public static String buildMonthKey(Long timestamp, String timeZone) {
        return getKey(timestamp, timeZone, MONTH_KEY_FORMAT);
    }

    public static String buildDayKey(Long timestamp, String timeZone) {
        return getKey(timestamp, timeZone, DAY_KEY_FORMAT);
    }

    public static String buildWeekKey(Long timestamp, String timeZone) {

        TimeZone targetTz = TimeZone.getTimeZone("GMT" + timeZone);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);

        calendar.setFirstDayOfWeek(Calendar.MONDAY);

        calendar.setTimeZone(targetTz);

        int year, week;
        year = calendar.get(Calendar.YEAR);
        week = calendar.get(Calendar.WEEK_OF_YEAR);
        //Case Monday Dec 31 2018
        if (calendar.get(Calendar.MONTH) == Calendar.DECEMBER && calendar.get(Calendar.DAY_OF_MONTH) == LAST_DAY_OF_YEAR
                && calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
            year++;
        }
        return String.format("%d-%02d", year, week);
    }


    /*
    public static void main(String[] args) throws Exception {
        Long epocTime = 1529886626000L;
        System.out.println(buildDayKey(epocTime, "-08:00"));
        System.out.println(buildDayKey(epocTime, "-01:00"));
        System.out.println(buildDayKey(epocTime, "+00:00"));
        System.out.println(buildDayKey(epocTime, "+05:30"));

    }*/


    private static String getKey(Long utcTimestamp, String timeZone, String keyFormat) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(utcTimestamp);

        TimeZone targetTz = TimeZone.getTimeZone("GMT" + timeZone);
        SimpleDateFormat sdfFormat = new SimpleDateFormat(keyFormat);
        sdfFormat.setTimeZone(targetTz);

        return sdfFormat.format(calendar.getTime());
    }

//    public static String convertDayToWeekKey(String dayKey) {
//        try {
//            SimpleDateFormat sdfDayFormat = new SimpleDateFormat(DAY_KEY_FORMAT);
//            DateTime dateTime = new DateTime(sdfDayFormat.parse(dayKey));
//            int month = dateTime.getMonthOfYear();
//            int day = dateTime.getDayOfMonth();
//            int dayOfWeek = dateTime.dayOfWeek().get();
//            int year = dateTime.getYear();
//
//            if(month == DateTimeConstants.DECEMBER && day == LAST_DAY_OF_YEAR && dayOfWeek != DateTimeConstants.SUNDAY){
//                year++;
//            }
//
//            return String.format("%d-%02d",year, dateTime.getWeekOfWeekyear());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    public static String convertDayToMonthKey(String dayKey) {
        try {
            SimpleDateFormat sdfDayFormat = new SimpleDateFormat(DAY_KEY_FORMAT);
            SimpleDateFormat sdfMonthFormat = new SimpleDateFormat(MONTH_KEY_FORMAT);
            return sdfMonthFormat.format(sdfDayFormat.parse(dayKey));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Convert String to milliseconds
    public static Long getTimeInMS(String time) {
        Calendar timeInMS = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
        try {
            timeInMS.setTime(sdf.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timeInMS.getTimeInMillis();
    }


    public static Long getTimeInMSEx(String time) {

        Calendar timeInMS = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
        time = time + " +0000";
        try {
            timeInMS.setTime(sdf.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timeInMS.getTimeInMillis();
    }

    public static Long getTimeInMSForTimezone(String time, String timezone) {
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd HH:mm:ss yyyy z", Locale.ENGLISH);
        try {
            calendar.setTime(sdf.parse(time + " GMT" + timezone));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return calendar.getTimeInMillis();
    }

    public static Function<EnrichedCDR, String> getClassifier(GROUP_BY groupByType){
        Function<EnrichedCDR, String> identity = null;
        Function<EnrichedCDR, String> getDayKey = (e) -> {
            return e.getDayKey();
        };
        Function<EnrichedCDR, String> getWeekKey = (e) -> {
            return e.getWeekKey();
        };
        Function<EnrichedCDR, String> getMonthKey = (e) -> {
            return e.getMonthKey();
        };
        Function<EnrichedCDR, Long> getEnterpriseAccountId = (e) -> {
            return e.getEnterpriseAccountId();
        };

        switch (groupByType) {
            case DAY:
                identity = getDayKey;
                break;
            case WEEK:
                identity = getWeekKey;
                break;
            case MONTH:
                identity = getMonthKey;
                break;
        }

        return identity;
    }

    public static DBResultSet aggregateResultSet(List<EnrichedCDR> cdrList) {
        DBResultSet resultSet = new DBResultSet();

        EnrichedCDR enrichedCDR = cdrList.get(0);

        Long totalUplinkBytes, totalDownlinkBytes, totalDataUsageBytes;
        totalUplinkBytes = cdrList.stream().collect(Collectors.summingLong((e) -> e.getUplinkVolume()));
        totalDownlinkBytes = cdrList.stream().collect(Collectors.summingLong((e) -> e.getDownlinkVolume()));
        totalDataUsageBytes = totalUplinkBytes + totalDownlinkBytes;

        resultSet.setTotalUsageInBytes(totalDataUsageBytes);
        resultSet.setDayKey(enrichedCDR.getDayKey());
        resultSet.setWeekKey(enrichedCDR.getWeekKey());
        resultSet.setMonthKey(enrichedCDR.getMonthKey());
        resultSet.setAccountId(enrichedCDR.getEnterpriseAccountId());

        return resultSet;
    }

    public static Comparator<DataUsage> getComparator(SORT_BY sortBy, SORT_DIRECTION sortByDirection) {
        Comparator<DataUsage> comparator = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        switch (sortBy) {
            case DATA_USAGE:
                if (sortByDirection == SORT_DIRECTION.ASC) {
                    comparator = new Comparator<DataUsage>() {
                        @Override
                        public int compare(DataUsage o1, DataUsage o2) {
                            return (o1.getDataUsedInBytes() < o2.getDataUsedInBytes()) ? -1 : (o1.getDataUsedInBytes() > o2.getDataUsedInBytes()) ? 1 : 0;

                        }
                    };
                } else {
                    comparator = new Comparator<DataUsage>() {
                        @Override
                        public int compare(DataUsage o1, DataUsage o2) {
                            return (o1.getDataUsedInBytes() > o2.getDataUsedInBytes()) ? -1 : (o1.getDataUsedInBytes() < o2.getDataUsedInBytes()) ? 1 : 0;

                        }
                    };
                }
                break;
            case DATE: {

                if (sortByDirection == SORT_DIRECTION.ASC) {
                    comparator = new Comparator<DataUsage>() {
                        @Override
                        public int compare(DataUsage o1, DataUsage o2) {
                            Date date1 = null, date2 = null;
                            try {
                                date1 = sdf.parse(o1.getFromDate());
                                date2 = sdf.parse(o2.getFromDate());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            return (date1.before(date2)) ? -1 : (date1.after(date2)) ? 1 : 0;

                        }
                    };
                } else {
                    comparator = new Comparator<DataUsage>() {
                        @Override
                        public int compare(DataUsage o1, DataUsage o2) {
                            Date date1 = null, date2 = null;
                            try {
                                date1 = sdf.parse(o1.getFromDate());
                                date2 = sdf.parse(o2.getFromDate());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            return (date1.after(date2)) ? -1 : (date1.before(date2)) ? 1 : 0;
                        }
                    };
                }
            }
            break;
        }

        return comparator;
    }

    public static Comparator<DataUsageList> getDataUsageListComparator(SORT_DIRECTION sortByDirection){
        Comparator<DataUsageList> comparator;
        if (sortByDirection == SORT_DIRECTION.ASC) {
            comparator = new Comparator<DataUsageList>() {
                @Override
                public int compare(DataUsageList o1, DataUsageList o2) {
                    return (o1.getTotalDataUsedInBytes().longValue() < o2.getTotalDataUsedInBytes().longValue()) ? -1 : (o1.getTotalDataUsedInBytes().longValue() > o2.getTotalDataUsedInBytes().longValue()) ? 1 : 0;

                }
            };
        } else {
            comparator = new Comparator<DataUsageList>() {
                @Override
                public int compare(DataUsageList o1, DataUsageList o2) {
                    return (o1.getTotalDataUsedInBytes().longValue() > o2.getTotalDataUsedInBytes().longValue()) ? -1 : (o1.getTotalDataUsedInBytes().longValue() < o2.getTotalDataUsedInBytes().longValue()) ? 1 : 0;

                }
            };
        }

        return comparator;
    }
    public static DataUsage convertToDataUsage(DataUsageResultSetBean dataUsageResultSetBean,
                                         AtomicLong totalBytesUsed, GROUP_BY grpByPeriod) {
        DataUsage dataUsage = new DataUsage();
        dataUsage.setDataUsedInBytes(Long.valueOf(dataUsageResultSetBean.getData_usage_bytes()));
        dataUsage.setFromDate(dataUsageResultSetBean.getStart_date() + " 00:00:00");
        dataUsage.setToDate(dataUsageResultSetBean.getEnd_date() + " 00:00:00");

        if (grpByPeriod == GROUP_BY.WEEK) {
            int weekNumber = 0;
            try {
                weekNumber = Integer.parseInt(dataUsageResultSetBean.getP_week().substring(WEEK_NUMBER_INDEX));
            } catch (Exception ex) {
            }

            dataUsage.setWeek(weekNumber);
        }

        totalBytesUsed.set(totalBytesUsed.addAndGet(dataUsage.getDataUsedInBytes()));

        return dataUsage;
    }

    public static void buildDataUsageList(DataUsageResultSetBean dataUsageResultSetBean, GetDataUsage dataUsageList,
                                          GROUP_BY grpByPeriod) {
        if (dataUsageList != null) {
            if (dataUsageList.getList() == null) {
                dataUsageList.setList(new ArrayList<DataUsage>());
            }
            List<DataUsage> dataUsages = dataUsageList.getList();
            //Convert the db record to DataUsage format
            AtomicLong totalDataUsedInBytes = new AtomicLong(dataUsageList.getTotalDataUsedInBytes());
            if (totalDataUsedInBytes == null) {
                dataUsageList.setTotalDataUsedInBytes(0);
                totalDataUsedInBytes =new  AtomicLong(dataUsageList.getTotalDataUsedInBytes());
            }
            DataUsage dataUsage = CommonUtil.convertToDataUsage(dataUsageResultSetBean, totalDataUsedInBytes, grpByPeriod);

            //Add this to the list
            dataUsages.add(dataUsage);

            dataUsageList.setList(dataUsages);
        }
    }

    public static void buildDataUsageList(DataUsageResultSetBean dataUsageResultSetBean, DataUsageList dataUsageList,
                                          GROUP_BY grpByPeriod) {
        if (dataUsageList != null) {
            if (dataUsageList.getList() == null) {
                dataUsageList.setList(new ArrayList<DataUsage>());
            }
            List<DataUsage> dataUsages = dataUsageList.getList();
            //Convert the db record to DataUsage format
            AtomicLong totalDataUsedInBytes = dataUsageList.getTotalDataUsedInBytes();
            if (totalDataUsedInBytes == null) {
                dataUsageList.setTotalDataUsedInBytes(new AtomicLong(0));
                totalDataUsedInBytes = dataUsageList.getTotalDataUsedInBytes();
            }
            DataUsage dataUsage = CommonUtil.convertToDataUsage(dataUsageResultSetBean, totalDataUsedInBytes, grpByPeriod);

            //Add this to the list
            dataUsages.add(dataUsage);

            dataUsageList.setList(dataUsages);
        }
    }

    public static void addAccountAndUserDetails(List<DBResultSet> resultSetList, GetEnterpriseAccountDetails account, GetUserListOfEntAccount userDetails){
        Map<Long, GetUserDetails> userDetailsMap = new HashMap<>();
        if(userDetails != null) {
            for (GetUserDetails user : userDetails.getList()) {
                userDetailsMap.put(Long.valueOf(user.getUserId()), user);
            }
        }
        for(DBResultSet resultSet: resultSetList){
            if(resultSet.getUserId() != null && userDetailsMap.containsKey(resultSet.getUserId())){
                GetUserDetails user = userDetailsMap.get(resultSet.getUserId());
                resultSet.setFirstName(user.getFirstName());
                resultSet.setLastName(user.getLastName());
                resultSet.setLocation(user.getLocation());
            }

            resultSet.setAccountName(account.getAccountName());
        }
    }

    public static DataUsage getDataUsageForUnlistGroupsZones(String fromDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());

        DataUsage dataUsage = new DataUsage();
        dataUsage.setDataUsedInBytes(0l);
        dataUsage.setDataUsageCost(new BigDecimal(0));
        dataUsage.setFromDate(fromDate);
        dataUsage.setToDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + " 00:00:00");
        return dataUsage;
    }
}
