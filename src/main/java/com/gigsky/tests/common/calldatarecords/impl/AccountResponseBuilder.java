package com.gigsky.tests.common.calldatarecords.impl;

import com.gigsky.tests.common.calldatarecords.datagenerator.CDRBuilder;
import com.gigsky.tests.common.calldatarecords.datagenerator.CommonUtil;
import com.gigsky.tests.common.calldatarecords.datagenerator.DBResultToDaoResultConverter;
import com.gigsky.tests.common.calldatarecords.datagenerator.TestAccountCDRConfig;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.GROUP_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_DIRECTION;
import com.gigsky.tests.common.calldatarecords.datagenerator.interfaces.ResponseBuilderInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by apandit on 7/30/19.
 */
public class AccountResponseBuilder extends CDRBuilder{
    private List<DBResultSet> resultSet = new ArrayList<>();
    private GetDataUsage accountDataUsageList = new GetDataUsage();

    @Override
    public void generateResultSet(Object id, GROUP_BY groupByType) {

        Function<EnrichedCDR, String> identity = CommonUtil.getClassifier(groupByType);

        Long accountId = Long.parseLong(String.valueOf(id));
        //One cdr per day
        Map<String, List<EnrichedCDR>> timeWiseGroupedCDRs = enrichedCDRList.stream().filter(e -> e.getEnterpriseAccountId().equals(accountId)).collect(
                        Collectors.groupingBy(identity));

        for (Map.Entry<String, List<EnrichedCDR>> groupByTypeMap : timeWiseGroupedCDRs.entrySet()) {
            resultSet.add(CommonUtil.aggregateResultSet(groupByTypeMap.getValue()));
        }

    }

    @Override
    public Object getAccountDataUsageResponse(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection, int startIndex, int count, String fromDate, String toDate, List<TestAccountCDRConfig> accountCDRConfigList) {
        parse(accountCDRConfigList);
        generateResultSet(accountId, groupByPeriod);
        List<DBResultSet> accountIdResultSet = resultSet.stream().skip(startIndex).limit(count).collect(Collectors.toList());

        List<DataUsageResultSetBean> dataUsageResultSetList = null;

        int recordsCount = 0;
        String lastAvailableDate = null;
        AtomicLong rowCount = new AtomicLong();
        dataUsageResultSetList = DBResultToDaoResultConverter.convert(accountIdResultSet,
                accountId, null, null, fromDate, toDate, rowCount);

        for (DataUsageResultSetBean dataUsageResultSet : dataUsageResultSetList) {
            CommonUtil.buildDataUsageList(dataUsageResultSet, accountDataUsageList, groupByPeriod);
        }

        List<DataUsage> dataUsageList = accountDataUsageList.getList();

        if(dataUsageList != null) {
            recordsCount = dataUsageList.size();
            Comparator<DataUsage> comparator = CommonUtil.getComparator(sortBy, sortByDirection);
            Collections.sort(accountDataUsageList.getList(), comparator);
        }
        else{
            recordsCount = 0;
        }



        int listIndex = 0;
        if (sortByDirection == SORT_DIRECTION.ASC) {

            listIndex = dataUsageResultSetList.size() - 1;
        }


        if (recordsCount > 0) {
            lastAvailableDate = accountDataUsageList.getList().get(listIndex).getToDate();
        }

        accountDataUsageList.setGroupByPeriod(groupByPeriod.name());
        accountDataUsageList.setTotalCount((int)(long)rowCount.get());
        accountDataUsageList.setCount(recordsCount);
        accountDataUsageList.setLastAvailableDate(lastAvailableDate);

        return accountDataUsageList;

    }

    public GetDataUsage getAccountDataUsageList() {
        return accountDataUsageList;
    }
}
