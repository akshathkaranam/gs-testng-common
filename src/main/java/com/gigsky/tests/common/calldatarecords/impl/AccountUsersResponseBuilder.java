package com.gigsky.tests.common.calldatarecords.impl;

import com.gigsky.tests.common.calldatarecords.datagenerator.CDRBuilder;
import com.gigsky.tests.common.calldatarecords.datagenerator.CommonUtil;
import com.gigsky.tests.common.calldatarecords.datagenerator.DBResultToDaoResultConverter;
import com.gigsky.tests.common.calldatarecords.datagenerator.TestAccountCDRConfig;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.GROUP_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_DIRECTION;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by apandit on 9/12/19.
 */
public class AccountUsersResponseBuilder extends CDRBuilder {

    private List<DBResultSet> resultSet = new ArrayList<>();

    @Override
    public void generateResultSet(Object id, GROUP_BY groupByType) {

        Function<EnrichedCDR, String> identity = CommonUtil.getClassifier(groupByType);

        Long accountId = Long.parseLong(String.valueOf(id));
        //One cdr per day
        Map<Long, Map<String, List<EnrichedCDR>>> userWiseGroupedCDRs = enrichedCDRList.stream().filter(e -> e.getEnterpriseAccountId().equals(accountId)).filter(e -> e.getUserId() != null).collect(
                Collectors.groupingBy(e -> e.getUserId(),
                        Collectors.groupingBy(identity)));

        for (Map.Entry<Long, Map<String, List<EnrichedCDR>>> userMap : userWiseGroupedCDRs.entrySet()) {
            for (Map.Entry<String, List<EnrichedCDR>> groupByTypeMap : userMap.getValue().entrySet()) {
                DBResultSet dbResultSet = CommonUtil.aggregateResultSet(groupByTypeMap.getValue());
                dbResultSet.setUserId(userMap.getKey());
                resultSet.add(dbResultSet);
            }
        }

    }

    @Override
    public AccountDataUsage getAccountDataUsageDistributedByUsers(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection,
                                                                  int startIndex, int count, String fromDate, String toDate,
                                                                  List<TestAccountCDRConfig> accountCDRConfigList,
                                                                  GetEnterpriseAccountDetails account, GetUserListOfEntAccount userDetails) {

        parse(accountCDRConfigList);
        generateResultSet(accountId, groupByPeriod);
        List<DBResultSet> userIdResultSet = resultSet.stream().skip(startIndex).limit(count).collect(Collectors.toList());

        CommonUtil.addAccountAndUserDetails(userIdResultSet, account, userDetails);

        List<DataUsageResultSetBean> dataUsageResultSetList = null;
        AccountDataUsage accountDataUsageByUsers = null;
//        int recordsCount = 0;
//        String lastAvailableDate = null;
        AtomicLong rowCount = new AtomicLong();
        dataUsageResultSetList = DBResultToDaoResultConverter.convert(userIdResultSet,
                accountId, null, null, fromDate, toDate, rowCount);

        accountDataUsageByUsers = buildAccountDataUsageByUsers(groupByPeriod, rowCount.get(), dataUsageResultSetList,
                sortByDirection);
        return accountDataUsageByUsers;
    }

    private AccountDataUsage buildAccountDataUsageByUsers(GROUP_BY grpByPeriod, long totalRecordCount,
                                                          List<DataUsageResultSetBean> dataUsageResultSetBeanList, SORT_DIRECTION sortDirection) {
        AccountDataUsage accountDataUsageByUsers = new AccountDataUsage();
        Map<String, DataUsageList> dataUsageListForAllUsers = new LinkedHashMap<String, DataUsageList>();
        AtomicLong totalDataUsedInBytesOfAllUsers = new AtomicLong(0);
        BigDecimal totalDataUsageCost = new BigDecimal(0.0);

        List<DataUsageList> dataUsageListOfUsers = accountDataUsageByUsers.getList();
        if (dataUsageListOfUsers == null) {
            dataUsageListOfUsers = new ArrayList<DataUsageList>();
        }

        if (CollectionUtils.isNotEmpty(dataUsageResultSetBeanList)) {
            //For every record that the query returned
            for (DataUsageResultSetBean dataUsageResultSetBean : dataUsageResultSetBeanList) {
                String userUid = dataUsageResultSetBean.getUser_uid();
                //Check if the map already contains entry for this user
                //If not create one
                if (!dataUsageListForAllUsers.containsKey(userUid)) {
                    dataUsageListForAllUsers.put(userUid, new DataUsageList());
                }
                //Get the entry for this user
                DataUsageList dataUsageListOfUser = dataUsageListForAllUsers.get(userUid);

                CommonUtil.buildDataUsageList(dataUsageResultSetBean, dataUsageListOfUser, grpByPeriod);

                //Set other user parameters
                dataUsageListOfUser.setAccountName(dataUsageResultSetBean.getEnt_account_name());
                dataUsageListOfUser.setUserId(Long.valueOf(dataUsageResultSetBean.getUser_uid()));
                dataUsageListOfUser.setUserFirstName(dataUsageResultSetBean.getUser_account_first_name());
                dataUsageListOfUser.setUserLastName(dataUsageResultSetBean.getUser_account_last_name());
                dataUsageListOfUser.setUserLocation(dataUsageResultSetBean.getUser_account_location());


            }

            //Add all these user's data usage list to the main Data Usage report
            for (Map.Entry<String, DataUsageList> dataUsageListForSingleUser : dataUsageListForAllUsers.entrySet()) {
                dataUsageListOfUsers.add(dataUsageListForSingleUser.getValue());
                totalDataUsedInBytesOfAllUsers.set(totalDataUsedInBytesOfAllUsers.addAndGet(
                        dataUsageListForSingleUser.getValue().getTotalDataUsedInBytes().get()));
//                if (dataUsageListForSingleUser.getValue().getTotalDataUsageCost() != null) {
//                    totalDataUsageCost = totalDataUsageCost.add(dataUsageListForSingleUser.getValue().getTotalDataUsageCost());
//                }
            }

            Comparator<DataUsageList> comparator = CommonUtil.getDataUsageListComparator(sortDirection);
            Collections.sort(dataUsageListOfUsers, comparator);

        }
        accountDataUsageByUsers.setList(dataUsageListOfUsers);
        int count = 0;
        if (CollectionUtils.isNotEmpty(accountDataUsageByUsers.getList())) {
            count = accountDataUsageByUsers.getList().size();
        }
        accountDataUsageByUsers.setGroupByPeriod(grpByPeriod.name());
        // If the group by period is anything other than MONTH, then total row count returned by stored procedure will be wrong. So
        //re-write it using the actual number of records in the list.
        if ((grpByPeriod == GROUP_BY.WEEK) || (grpByPeriod == GROUP_BY.DAY)) {
            accountDataUsageByUsers.setTotalCount((long) count);
        } else {
            accountDataUsageByUsers.setTotalCount(totalRecordCount);
        }
        accountDataUsageByUsers.setCount(count);
        accountDataUsageByUsers.setTotalDataUsedInBytes(totalDataUsedInBytesOfAllUsers);
//        accountDataUsageByUsers.setTotalDataUsageCost(
//                totalDataUsageCost.setScale(NUMBER_OF_DECIMAL_PLACES, BigDecimal.ROUND_HALF_UP));
        return accountDataUsageByUsers;
    }
}
