package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

//import com.gigsky.analytics.utils.CommonUtil;
import com.gigsky.tests.common.calldatarecords.datagenerator.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by vinayr on 02/03/18.
 */

/*
{
  "type": "EnrichedCDR",
  "messageTime": 1518417035000,
  "operationType": "UPSERT",
  "id": 1,
  "cdrType": "NORMAL",
  "iccId": "1001100000000000001",
  "imsi": "100110000000001",
  "msisdn": 100110000000001,
  "mcc": 202,
  "mnc": 1,
  "uplinkVolume": 12345,
  "downlinkVolume": 12345,
  "recordStartTime": 1518417035000,
  "recordDurationInSecs": 60,
  "cdrReceivedTime": 1518417035000,
  "groupAccountId": 1,
  "enterpriseAccountId": 2,
  "userId": 1,
  "roamingProfileId": 12345,
  "countryCode": "AE",
  "countryName": "United Arab Emirates",
  "imsiProfileName": "TDC",
  "zoneId": 1,
  "zoneName": "2-Zone1",
  "timeZone": "+00:00"
}
 */
public class EnrichedCDR extends CDR {
	private static final Logger logger = LoggerFactory.getLogger(EnrichedCDR.class);
	// from Roaming Profile
	protected String carrierName;
	protected String tadigCode;
	private String countryCode;
	private String countryName;

	// from SIM Info
	private Long groupAccountId;
	private Long enterpriseAccountId;
	private Long userId;
	private Long roamingProfileId;
	private Long subAccountId;
	// from Account Info
	private Long zoneId;
	private String zoneName;
	private String timeZone;

    //initial delay time
	private Long initialDelayTimeMs = 0l;


	public EnrichedCDR() {
		eventType = "EnrichedCDR";
		operationType = "UPSERT";

	}


	public EnrichedCDR(Long timestamp, String operationType, Long cdrId, Long partitionID, String cdrType, String iccId, String imsi, String msisdn, String mcc, String mnc,
					   Long uplinkVolume, Long downlinkVolume, Long startTime, Long durationInSecs, Long receivedTime,
					   Long groupAccountId, Long enterpriseAccountId, Long subAccountId, Long userId, Long roamingProfileId,
					   String countryCode, String countryName, String imsiProfileName, Long zoneId, String zoneName, String timeZone) {

		this.eventType = "EnrichedCDR";
		this.operationType = operationType;

		this.eventTime = timestamp;
		this.cdrId = cdrId;
		this.partitionID = partitionID;
		this.cdrType = cdrType;
		this.iccId = iccId;
		this.imsi = imsi;
		this.msisdn = msisdn;
		this.mcc = mcc;
		this.mnc = mnc;
		this.uplinkVolume = uplinkVolume;
		this.downlinkVolume = downlinkVolume;
		this.recordStartTime = startTime;
		this.durationInSecs = durationInSecs;
		this.cdrReceivedTime = receivedTime;

		this.groupAccountId = groupAccountId;
		this.enterpriseAccountId = enterpriseAccountId;
		this.subAccountId = subAccountId;
		this.userId = userId;
		this.roamingProfileId = roamingProfileId;

		this.countryCode = countryCode;
		this.countryName = countryName;
		//this.imsiProfileName = imsiProfileName;
		this.zoneId = zoneId;
		this.zoneName = zoneName;
		this.timeZone = timeZone;
	}

	public Long getGroupAccountId() {
		return groupAccountId;
	}

	public void setGroupAccountId(Long groupAccountId) {
		this.groupAccountId = groupAccountId;
	}

	public Long getEnterpriseAccountId() {
		return enterpriseAccountId;
	}

	public void setEnterpriseAccountId(Long enterpriseAccountId) {
		this.enterpriseAccountId = enterpriseAccountId;
	}


	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getSubAccountId() {
		return subAccountId;
	}

	public void setSubAccountId(Long subAccountId) {
		this.subAccountId = subAccountId;
	}

	public Long getRoamingProfileId() {
		return roamingProfileId;
	}

	public void setRoamingProfileId(Long roamingProfileId) {
		this.roamingProfileId = roamingProfileId;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getTadigCode() {
		return tadigCode;
	}

	public void setTadigCode(String tadigCode) {
		this.tadigCode = tadigCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public Long getZoneId() {
		return zoneId;
	}

	public void setZoneId(Long zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public Long getInitialDelayTimeMs() {
		return initialDelayTimeMs;
	}

	public void setInitialDelayTimeMs(Long initialDelayTimeMs) {
		this.initialDelayTimeMs = initialDelayTimeMs;
	}


	@Override
	public String toString() {
		return "EnrichedCDR{" +
				"carrierName='" + carrierName + '\'' +
				", tadigCode='" + tadigCode + '\'' +
				", countryCode='" + countryCode + '\'' +
				", countryName='" + countryName + '\'' +
				", iccId='" + iccId + '\'' +
				", imsi='" + imsi + '\'' +
				", msisdn='" + msisdn + '\'' +
				", mcc='" + mcc + '\'' +
				", mnc='" + mnc + '\'' +
				", uplinkBytes='" + uplinkVolume + '\'' +
				", downlinkBytes='" + downlinkVolume + '\'' +
				", recordStartTime='" + recordStartTime + '\'' +
				", eventTime='" + eventTime + '\'' +
				", groupAccountId=" + groupAccountId +
				", enterpriseAccountId=" + enterpriseAccountId +
				", userId=" + userId +
				", roamingProfileId=" + roamingProfileId +
				", zoneId=" + zoneId +
				", zoneName='" + zoneName + '\'' +
				", timeZone='" + timeZone + '\'' +
				", partitionId= " + partitionID +
				'}';
	}

	//For test cases
	public String getDayKey(){
		return CommonUtil.buildDayKey(getEventTime(), getTimeZone());
	}
	public String getWeekKey(){
		return CommonUtil.buildWeekKey(getEventTime(), getTimeZone());
	}
	public String getMonthKey(){
		return CommonUtil.buildMonthKey(getEventTime(), getTimeZone());
	}

}