package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QueryParams {

    public Map<String, Object> queryParam1 = new HashMap<>();
    public Map<String, Object> queryParam2 = new HashMap<>();
    public Map<String, Object> queryParam3 = new HashMap<>();

    public Map<String, String> cdrQueryParam1 = new HashMap<>();
    public Map<String, String> cdrQueryParam2 = new HashMap<>();
    public Map<String, String> cdrQueryParam3 = new HashMap<>();
    public Map<String, String> cdrQueryParam4 = new HashMap<>();

    public  void queryParamsUpdate() {
        List<String> countryList = new ArrayList<>();
        countryList.add("IN");
        countryList.add("AU");
        List<String> imsiTypes = new ArrayList<>();
        queryParam1.put("fromDate","2018-01-01 09:00:00");
        queryParam1.put("groupBy","COUNTRY");
        queryParam1.put("sortBy","date");
        queryParam1.put("sortDirection","DESC");
        queryParam1.put("startIndex",0);
        queryParam1.put("filters","country=="+"IN");
//        queryParam1.put("filters","imsiTypes=="+"ATT");
        queryParam1.put("toDate","2018-01-05 12:00:00");
        queryParam1.put("count",10);

        queryParam3.put("fromDate","2018-01-01 09:00:00");
        queryParam3.put("groupBy","COUNTRY");
        queryParam3.put("sortBy","sim");
        queryParam3.put("sortDirection","DESC");
        queryParam3.put("startIndex",0);
//        queryParam3.put("filters","country=="+"AU,IN,F");
        queryParam3.put("filters","country=="+"IN"+"&"+"imsiType=="+"TDC");
        queryParam3.put("toDate","2018-01-05 12:00:00");
        queryParam3.put("count",10);


        queryParam2.put("count",100);
        queryParam2.put("filters","country==AT");
        queryParam2.put("fromDate","2019-07-02 09:00:00");
        queryParam2.put("sortBy","date");
        queryParam2.put("sortDirection","ASC");
        queryParam2.put("startIndex",0);
        queryParam2.put("toDate","2019-10-02 09:00:00");


    }

    public void cdrQueryParamUpdate(){

        cdrQueryParam1.put("fromDate","2018-01-01 09:00:00");
        cdrQueryParam1.put("toDate","2018-01-05 09:00:00");
        cdrQueryParam1.put("iccId","100304800110000");
        cdrQueryParam1.put("carrierName","MYacc1");
        cdrQueryParam1.put("countryCode","IN");
        cdrQueryParam1.put("countryName","INDIA");
        cdrQueryParam1.put("mcc","400");
        cdrQueryParam1.put("mnc","401");
        cdrQueryParam1.put("imsi","3456");
        cdrQueryParam1.put("msisdn","7890");
        cdrQueryParam1.put("cdrUsageType","CAPTIVE");
        cdrQueryParam1.put("imsiProfileName","TDC");

        cdrQueryParam2.put("fromDate","2018-01-01 09:00:00");
        cdrQueryParam2.put("toDate","2018-01-05 09:00:00");
        cdrQueryParam2.put("iccId","100304800110000");
        cdrQueryParam2.put("carrierName","MYacc2");
        cdrQueryParam2.put("countryCode","JP");
        cdrQueryParam2.put("countryName","JAPAN");
        cdrQueryParam2.put("mcc","400");
        cdrQueryParam2.put("mnc","401");
        cdrQueryParam2.put("imsi","1222");
        cdrQueryParam2.put("msisdn","7896");
        cdrQueryParam2.put("imsiProfileName","ATT");

        cdrQueryParam4.put("fromDate","2018-01-01 09:00:00");
        cdrQueryParam4.put("toDate","2018-01-05 09:00:00");
        cdrQueryParam4.put("iccId","100304800110000");
        cdrQueryParam4.put("carrierName","MYacc2");
        cdrQueryParam4.put("countryCode","JP");
        cdrQueryParam4.put("countryName","JAPAN");
        cdrQueryParam4.put("mcc","400");
        cdrQueryParam4.put("mnc","401");
        cdrQueryParam4.put("imsi","1222");
        cdrQueryParam4.put("msisdn","7896");
        cdrQueryParam4.put("imsiProfileName","ATT");

        cdrQueryParam3.put("fromDate","2018-01-01 09:00:00");
        cdrQueryParam3.put("toDate","2018-01-05 09:00:00");
        cdrQueryParam3.put("iccId","100304800110888");
        cdrQueryParam3.put("carrierName","MYacc1");
        cdrQueryParam3.put("countryCode","AU");
        cdrQueryParam3.put("countryName","AUSTRALIA");
        cdrQueryParam3.put("mcc","400");
        cdrQueryParam3.put("mnc","401");
        cdrQueryParam3.put("imsi","3456");
        cdrQueryParam3.put("msisdn","7890");
        cdrQueryParam3.put("imsiProfileName","TDC");
        cdrQueryParam1.put("cdrType", "CAPTIVE");
    }

}
