package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;






/* Created on 20/07/2019 */

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class CDRActivity {
    @JsonProperty
    private String type = "CDRActivity";
    @JsonProperty
    private Integer sims;
    @JsonProperty
    private Country country ;

    public String getType() {
        return type;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) { this.country = country; }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSims()
    {
        return sims;
    }
    public void setSims(Integer sims)
    {
        this.sims = sims;
    }

}
