/*
 * Copyright (c) 2016. GigSky, Inc.
 * All rights reserved.
 *
 * THIS SOFTWARE IS THE CONFIDENTIAL AND PROPRIETARY
 * INFORMATION OF GIGSKY, INC.
 *
 * UNAUTHORIZED USE OR DISCLOSURE IS PROHIBITED.
 *
 * https://www.gigsky.com/
 */

package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created on 12/5/15.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DataUsageResultSetBean {

    //All common fields
    private String ent_account_id;
    private String start_date;
    private String end_date;
    private String data_usage_bytes;
    private String p_month;
    private String p_week;
    private String p_day;

    // Enterprise account related data
    private String ent_account_name;
    private String account_type;

    //Enterprise user related data
    private String user_uid;
    private String user_account_first_name;
    private String user_account_last_name;
    private String user_account_location;
    private String user_account_email;

    //Enterprise sim related data
    private String iccId;

    //Enterprise group related data
    private String company_name;
    private String group_account_id;
    private String group_account_name;

    //Eneterprise zone related data
    private String zone_id;
    private String zone_name;

    // to hold the row count
    private long totalRecords;

    private String data_usage_cost;


    private String monthKey;
    private String dayKey;
    private String weekKey;

    public String getMonthKey() {
        return monthKey;
    }

    public void setMonthKey(String monthKey) {
        this.monthKey = monthKey;
    }

    public String getDayKey() {
        return dayKey;
    }

    public void setDayKey(String dayKey) {
        this.dayKey = dayKey;
    }

    public String getWeekKey() {
        return weekKey;
    }

    public void setWeekKey(String weekKey) {
        this.weekKey = weekKey;
    }

    public String getEnt_account_id() {
        return ent_account_id;
    }

    public void setEnt_account_id(Object ent_account_id) {
        this.ent_account_id = String.valueOf(ent_account_id);
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(Object start_date) {
        this.start_date = start_date != null? start_date.toString() : null;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Object end_date) {
        this.end_date = end_date != null? end_date.toString() : null;
    }

    public String getData_usage_bytes() {
        return data_usage_bytes;
    }

    public void setData_usage_bytes(Object data_usage_bytes) {
        this.data_usage_bytes = String.valueOf(data_usage_bytes);
    }

    public String getP_month() {
        return p_month;
    }

    public void setP_month(Object p_month) {
        this.p_month = String.valueOf(p_month);
    }

    public String getP_week() {
        return p_week;
    }

    public void setP_week(Object p_week) {
        this.p_week = String.valueOf(p_week);
    }

    public String getP_day() {
        return p_day;
    }

    public void setP_day(Object p_day) {
        this.p_day = String.valueOf(p_day);
    }

    public String getEnt_account_name() {
        return ent_account_name;
    }

    public void setEnt_account_name(Object ent_account_name) {
        this.ent_account_name = (String) ent_account_name;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(Object account_type) {
        this.account_type = (String) account_type;
    }

    public String getUser_uid() {
        return user_uid;
    }

    public void setUser_uid(Object user_uid) {
        this.user_uid = String.valueOf(user_uid);
    }

    public String getUser_account_first_name() {
        return user_account_first_name;
    }

    public void setUser_account_first_name(Object user_account_first_name) {
        this.user_account_first_name = (String) user_account_first_name;
    }

    public String getUser_account_last_name() {
        return user_account_last_name;
    }

    public void setUser_account_last_name(Object user_account_last_name) {
        this.user_account_last_name = (String) user_account_last_name;
    }

    public String getUser_account_location() {
        return user_account_location;
    }

    public void setUser_account_location(Object user_account_location) {
        this.user_account_location = (String) user_account_location;
    }

    public String getUser_account_email() {
        return user_account_email;
    }

    public void setUser_account_email(Object user_account_email) {
        this.user_account_email = (String) user_account_email;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(Object iccId) {
        this.iccId = (String) iccId;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(Object company_name) {
        this.company_name = (String) company_name;
    }

    public String getGroup_account_name() {
        return group_account_name;
    }

    public void setGroup_account_name(Object group_account_name) {
        this.group_account_name = (String) group_account_name;
    }

    public String getZone_id() {
        return zone_id;
    }

    public void setZone_id(Object zone_id) {
        this.zone_id = String.valueOf(zone_id);
    }

    public String getZone_name() {
        return zone_name;
    }

    public void setZone_name(Object zone_name) {
        this.zone_name = (String) zone_name;
    }

    public long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public String getGroup_account_id() {
        return group_account_id;
    }

    public void setGroup_account_id(Object group_account_id) {
        this.group_account_id = String.valueOf(group_account_id);
    }

    public String getData_usage_cost() {
        return data_usage_cost;
    }

    public void setData_usage_cost(Object data_usage_cost) {
        this.data_usage_cost = String.valueOf(data_usage_cost);
    }
}
