/*
 * Copyright (c) 2016. GigSky, Inc.
 * All rights reserved.
 *
 * THIS SOFTWARE IS THE CONFIDENTIAL AND PROPRIETARY
 * INFORMATION OF GIGSKY, INC.
 *
 * UNAUTHORIZED USE OR DISCLOSURE IS PROHIBITED.
 *
 * https://www.gigsky.com/
 */

package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created on 11/29/15.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DataUsageList {
    @JsonProperty
    private String type = "DataUsageList";
    @JsonProperty
    private String lastAvailableDate;
    @JsonProperty
    private String groupByPeriod;
    @JsonProperty
    private AtomicLong totalDataUsedInBytes;
    @JsonProperty
    private BigDecimal totalDataUsageCost;
    @JsonProperty
    private Long zoneId;
    @JsonProperty
    private String zoneName;
    @JsonProperty
    private String zoneNickName;
    @JsonProperty
    private Long accountId;
    @JsonProperty
    private String accountName;
    @JsonProperty
    private String accountType;
    @JsonProperty
    private Long userId;
    @JsonProperty
    private String userFirstName;
    @JsonProperty
    private String userLastName;
    @JsonProperty
    private String userLocation;
    @JsonProperty
    private String simIccid;
    @JsonProperty
    private List<DataUsage> list;
    @JsonProperty
    private Integer count;
    @JsonProperty
    private Long totalCount;
    @JsonProperty
    private Double minimumCommitment;
    @JsonProperty
    private List<DataUsageList> dataUsageList;

    public String getLastAvailableDate() {
        return lastAvailableDate;
    }

    public void setLastAvailableDate(String lastAvailableDate) {
        this.lastAvailableDate = lastAvailableDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGroupByPeriod() {
        return groupByPeriod;
    }

    public void setGroupByPeriod(String groupByPeriod) {
        this.groupByPeriod = groupByPeriod;
    }

    public AtomicLong getTotalDataUsedInBytes() {
        return totalDataUsedInBytes;
    }

    public void setTotalDataUsedInBytes(AtomicLong totalDataUsedInBytes) {
        this.totalDataUsedInBytes = totalDataUsedInBytes;
    }

    public List<DataUsage> getList() {
        return list;
    }

    public void setList(List<DataUsage> list) {
        this.list = list;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public String getSimIccid() {
        return simIccid;
    }

    public void setSimIccid(String simIccid) {
        this.simIccid = simIccid;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(String userLocation) {
        this.userLocation = userLocation;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public Long getZoneId() {
        return zoneId;
    }

    public void setZoneId(Long zoneId) {
        this.zoneId = zoneId;
    }

    public List<DataUsageList> getDataUsageList() {
        return dataUsageList;
    }

    public void setDataUsageList(List<DataUsageList> dataUsageList) {
        this.dataUsageList = dataUsageList;
    }

    public BigDecimal getTotalDataUsageCost() {
        return totalDataUsageCost;
    }

    public void setTotalDataUsageCost(BigDecimal totalDataUsageCost) {
        this.totalDataUsageCost = totalDataUsageCost;
    }

    public String getZoneNickName() {
        return zoneNickName;
    }

    public void setZoneNickName(String zoneNickName) {
        this.zoneNickName = zoneNickName;
    }

    public Double getMinimumCommitment() {
        return minimumCommitment;
    }

    public void setMinimumCommitment(Double minimumCommitment) {
        this.minimumCommitment = minimumCommitment;
    }
}
