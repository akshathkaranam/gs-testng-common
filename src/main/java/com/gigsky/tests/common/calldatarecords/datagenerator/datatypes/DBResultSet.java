package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

//import org.bson.types.BSONTimestamp;

import java.util.List;

/**
 * Created by haribongale on 29/03/18.
 */
public class DBResultSet {

    private String monthKey;
    private String dayKey;
    private String weekKey;
    private Long totalUsageInBytes;
    private Integer simCount;
    private String countryCode;



    /*Account split by zones (monthwise)*/
    private Long zoneId;
    private String zoneName;
    private String zoneNickName;

    /*Account split by zones (daywise)*/

    /*Account split by zones (weekwise)*/

    /*Account split by groups (monthwise)*/
    private Long groupAccountId;
    private String accountName;
    private String accountType;
    /*Account split by groups (daywise)*/

    /*Account split by groups (weekwise)*/

    /*Account usage (daywise)*/

    /*Account usage (monthwise)*/

    /*User usage (daywise)*/
    private Long userId;
    private String firstName;
    private String lastName;
    private String emailId;
    private String location;

    /*User usage (monthwise)*/

    /*Sim usage (daywise)*/

    /*Sim usage (monthwise)*/

    /*Sim usage split by zones (monthwise)*/

    /*Get list of sims for a month sorted by data usage*/

    /*Get list of users for a month sorted by data usage*/

    /*Get data usage for list of sims*/
    private String iccId;

    private String countryName;

    private List<EnrichedCDR> enrichedCDRList;

    private List<EnrichedLocationUpdate> enrichedLocationUpdateList;


    /*Get data usage for list of users*/

    /*Get accountId and updateTime for Probe data*/

    private Long accountId;
//    private BSONTimestamp updateTime;


    public List<EnrichedCDR> getEnrichedCDRList() {
        return enrichedCDRList;
    }

    public void setEnrichedCDRList(List<EnrichedCDR> enrichedCDRList) {
        this.enrichedCDRList = enrichedCDRList;
    }
    public void setEnrichedLUList(List<EnrichedLocationUpdate> enrichedLUList) {
        this.enrichedLocationUpdateList = enrichedLUList;
    }
    public List<EnrichedLocationUpdate> getEnrichedLocationUpdateList(){
        return enrichedLocationUpdateList;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Integer getSimCount() {
        return simCount;
    }

    public void setSimCount(Integer simCount) {
        this.simCount = simCount;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getDayKey() {
        return dayKey;
    }

    public void setDayKey(String dayKey) {
        this.dayKey = dayKey;
    }

    public Long getGroupAccountId() {
        return groupAccountId;
    }

    public void setGroupAccountId(Long groupAccountId) {
        this.groupAccountId = groupAccountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getMonthKey() {
        return monthKey;
    }

    public void setMonthKey(String monthKey) {
        this.monthKey = monthKey;
    }

    public Long getTotalUsageInBytes() {
        return totalUsageInBytes;
    }

    public void setTotalUsageInBytes(Long totalUsageInBytes) {
        this.totalUsageInBytes = totalUsageInBytes;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getWeekKey() {
        return weekKey;
    }

    public void setWeekKey(String weekKey) {
        this.weekKey = weekKey;
    }

    public Long getZoneId() {
        return zoneId;
    }

    public void setZoneId(Long zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getZoneNickName() {
        return zoneNickName;
    }

    public void setZoneNickName(String zoneNickName) {
        this.zoneNickName = zoneNickName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

//    public BSONTimestamp getUpdateTime() {
//        return updateTime;
//    }

//    public void setUpdateTime(BSONTimestamp updateTime) {
//        this.updateTime = updateTime;
//    }
}
