package com.gigsky.tests.common.calldatarecords.beans;

import java.util.List;

/**
 * Created by jeyarajs on 15/12/16.
 */
public class CDRInfo {

    private List<CDRData> cdrDataList;
    private FtpConfig ftpConfig;

    public List<CDRData> getCdrDataList() {
        return cdrDataList;
    }

    public void setCdrDataList(List<CDRData> cdrDataList) {
        this.cdrDataList = cdrDataList;
    }

    public FtpConfig getFtpConfig() {
        return ftpConfig;
    }

    public void setFtpConfig(FtpConfig ftpConfig) {
        this.ftpConfig = ftpConfig;
    }
}
