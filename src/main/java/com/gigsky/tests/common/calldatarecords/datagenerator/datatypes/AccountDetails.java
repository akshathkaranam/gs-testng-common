package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "enterpriseAccountDetails"
})
public class AccountDetails {

    @JsonProperty("type")
    private String type;
    @JsonProperty("enterpriseAccountDetails")
    private List<EnterpriseAccountDetail> enterpriseAccountDetails = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("enterpriseAccountDetails")
    public List<EnterpriseAccountDetail> getEnterpriseAccountDetails() {
        return enterpriseAccountDetails;
    }

    @JsonProperty("enterpriseAccountDetails")
    public void setEnterpriseAccountDetails(List<EnterpriseAccountDetail> enterpriseAccountDetails) {
        this.enterpriseAccountDetails = enterpriseAccountDetails;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
