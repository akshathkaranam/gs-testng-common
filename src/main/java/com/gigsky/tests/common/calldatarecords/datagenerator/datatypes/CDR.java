package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CDR extends BaseEvent implements Comparable<CDR>{

    private static final Logger logger = LoggerFactory.getLogger(CDR.class);
//	{
//		"type": "CDR",
//			"messageTime": 1518417035000,
//			"operationType": "UPSERT",
//			"id": 1,
//			"cdrType": "NORMAL",
//			"iccId": "1001100000000000001",
//			"imsi": "100110000000001",
//			"msisdn": 100110000000001,
//			"mcc": 202,
//			"mnc": 1,
//			"uplinkVolume": 12345,
//			"downlinkVolume": 12345,
//			"recordStartTime": 1518417035000,
//			"recordDurationInSecs": 60,
//			"cdrReceivedTime": 1518417035000
//	}


    public CDR()
    {
        super("CDR");
    }


    public CDR(Long timestamp, Long cdrId, Long partitionID, String cdrType, String iccId, String imsi, String msisdn, String mcc, String mnc, Long uplinkVolume, Long downlinkVolume, Long durationInSecs, String imsiProfile)
    {
        super("CDR");

        this.eventType = "CDR";
        this.operationType = "UPSERT";
        this.eventTime = timestamp;
        this.cdrId = cdrId;
        this.partitionID = partitionID;
        this.cdrType = cdrType;
        this.iccId = iccId;
        this.imsi = imsi;
        this.msisdn = msisdn;
        this.mcc = mcc;
        this.mnc = mnc;
        this.uplinkVolume = uplinkVolume;
        this.downlinkVolume = downlinkVolume;
        this.durationInSecs = durationInSecs;
        this.cdrReceivedTime = timestamp;
        this.recordStartTime = timestamp;
        this.imsiProfile = imsiProfile;
    }

    public void copyInfo(CDR other)
    {
        this.eventTime = other.eventTime;
        this.cdrId = other.cdrId;
        this.partitionID = other.partitionID;
        this.cdrType = other.cdrType;
        this.iccId = other.iccId;
        this.imsi = other.imsi;
        this.msisdn = other.msisdn;
        this.mcc = other.mcc;
        this.mnc = other.mnc;
        this.uplinkVolume = other.uplinkVolume;
        this.downlinkVolume = other.downlinkVolume;
        this.durationInSecs = other.durationInSecs;
        this.cdrReceivedTime = other.cdrReceivedTime;
        this.recordStartTime = other.recordStartTime;
        this.imsiProfile = other.imsiProfile;
        this.ingestionTime = other.ingestionTime;

        this.updateTime=other.updateTime;
        this.accessPointName=other.accessPointName;
        this.chargingID=other.chargingID;
        this.localSequenceNumber=other.localSequenceNumber;
        this.pdpPDNType=other.pdpPDNType;
        this.servedPDPPDNAddress=other.servedPDPPDNAddress;
        this.nodeID=other.nodeID;
        this.sGWAddress=other.sGWAddress;
        this.servingNodePLMNIdentifier=other.servingNodePLMNIdentifier;
        this.timeAtGSBackend=other.timeAtGSBackend;
        this.cdrTrafficType=other.cdrTrafficType;
    }


    protected Long cdrId;
    protected String cdrType;
    protected String iccId;
    protected String imsi;
    protected String imsiProfile;
    protected String msisdn;
    protected String mcc;
    protected String mnc;
    protected Long uplinkVolume;
    protected Long downlinkVolume;
    protected Long durationInSecs;
    protected Long cdrReceivedTime;
    protected Long recordStartTime;
    protected Long updateTime;

    //additional fields
    private Long timeAtGSBackend;
    private Long chargingID;
    private String accessPointName;
    private String nodeID;
    private Long localSequenceNumber;
    private String servingNodePLMNIdentifier;
    private String sGWAddress;
    private String pdpPDNType;
    private String servedPDPPDNAddress;
    protected Long partitionID;
    protected String cdrTrafficType;

    public String getCdrTrafficType() {
        return cdrTrafficType;
    }

    public void setCdrTrafficType(String cdrTrafficType) {
        this.cdrTrafficType = cdrTrafficType;
    }

    public Long getPartitionID() {
        return partitionID;
    }

    public void setPartitionID(Long partitionID) {
        this.partitionID = partitionID;
    }

    public Long getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getImsiProfile() { return imsiProfile; }

    public void setImsiProfile(String imsiProfile) {
        this.imsiProfile = imsiProfile;
    }

    public Long getCdrId() { return cdrId; }

    public void setCdrId(Long cdrId) {
        this.cdrId = cdrId;
    }

    public Long getCdrReceivedTime() {
        return cdrReceivedTime;
    }

    public void setCdrReceivedTime(Long cdrReceivedTime) {
        this.cdrReceivedTime = cdrReceivedTime;
    }

    public String getCdrType() {
        return cdrType;
    }

    public void setCdrType(String cdrType) {
        this.cdrType = cdrType;
    }

    public Long getDownlinkVolume() {
        return downlinkVolume;
    }

    public void setDownlinkVolume(Long downlinkVolume) {
        this.downlinkVolume = downlinkVolume;
    }

    public Long getDurationInSecs() {
        return durationInSecs;
    }

    public void setDurationInSecs(Long durationInSecs) {
        this.durationInSecs = durationInSecs;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getMnc() {
        return mnc;
    }

    public void setMnc(String mnc) {
        this.mnc = mnc;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Long getRecordStartTime() {
        return recordStartTime;
    }

    public void setRecordStartTime(Long recordStartTime) {
        this.recordStartTime = recordStartTime;
    }

    public Long getUplinkVolume() {
        return uplinkVolume;
    }

    public void setUplinkVolume(Long uplinkVolume) {
        this.uplinkVolume = uplinkVolume;
    }

    public Long getTimeAtGSBackend() { return timeAtGSBackend; }

    public void setTimeAtGSBackend(Long timeAtGSBackend) { this.timeAtGSBackend = timeAtGSBackend; }

    public Long getChargingID() { return chargingID; }

    public void setChargingID(Long chargingID) { this.chargingID = chargingID; }

    public String getAccessPointName() { return accessPointName; }

    public void setAccessPointName(String accessPointName) { this.accessPointName = accessPointName; }

    public String getNodeID() { return nodeID; }

    public void setNodeID(String nodeID) { this.nodeID = nodeID; }

    public Long getLocalSequenceNumber() { return localSequenceNumber; }

    public void setLocalSequenceNumber(Long localSequenceNumber) { this.localSequenceNumber = localSequenceNumber; }

    public String getServingNodePLMNIdentifier() { return servingNodePLMNIdentifier; }

    public void setServingNodePLMNIdentifier(String servingNodePLMNIdentifier) { this.servingNodePLMNIdentifier = servingNodePLMNIdentifier; }

    public String getSGWAddress() { return sGWAddress; }

    public void setSGWAddress(String sGWAddress) { this.sGWAddress = sGWAddress; }

    public String getPdpPDNType() { return pdpPDNType; }

    public void setPdpPDNType(String pdpPDNType) { this.pdpPDNType = pdpPDNType; }

    public String getServedPDPPDNAddress() { return servedPDPPDNAddress; }

    public void setServedPDPPDNAddress(String servedPDPPDNAddress) { this.servedPDPPDNAddress = servedPDPPDNAddress; }


    @Override
    public int compareTo(CDR other) {
        return Long.compare(this.eventTime, other.eventTime);
    }

//    @Override
//    public String toString() {
//        return "CDR{" +
//                " eventTime=" + eventTime +
//                " cdrId=" + cdrId +
//                ", partitionId=" + partitionID +
//                ", cdrType='" + cdrType + '\'' +
//                ", iccId='" + iccId + '\'' +
//                ", imsi='" + imsi + '\'' +
//                ", imsiProfile='" + imsiProfile + '\'' +
//                ", msisdn='" + msisdn + '\'' +
//                ", mcc='" + mcc + '\'' +
//                ", mnc='" + mnc + '\'' +
//                ", uplinkVolume=" + uplinkVolume +
//                ", downlinkVolume=" + downlinkVolume +
//                ", durationInSecs=" + durationInSecs +
//                ", cdrReceivedTime=" + cdrReceivedTime +
//                ", recordStartTime=" + recordStartTime +
//                ", operationType='" + operationType + '\'' +
//                '}';
//    }
//
//    public static CDR fromString(String line)
//    {
//        //logger.info("CDR  : " + line);
//
//        ObjectMapper mapper = new ObjectMapper();
//        CDR cdr = null;
//
//        try {
//            JsonNode jsonNode = mapper.readTree(line);
//            cdr = mapper.readValue(line, CDR.class);
//            cdr.setEventTime(cdr.getRecordStartTime());
//            cdr.setIngestionTime(System.currentTimeMillis());
//            cdr.setSGWAddress(jsonNode.get("sGWAddress").asText());
//            cdr.setOperationType(jsonNode.get("operationType").asText());
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return cdr;
//    }
}
