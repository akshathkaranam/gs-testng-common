/*
 * Copyright (c) 2016. GigSky, Inc.
 * All rights reserved.
 *
 * THIS SOFTWARE IS THE CONFIDENTIAL AND PROPRIETARY
 * INFORMATION OF GIGSKY, INC.
 *
 * UNAUTHORIZED USE OR DISCLOSURE IS PROHIBITED.
 *
 * https://www.gigsky.com/
 */

package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.math.BigDecimal;

/**
 * Created on 11/29/15.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DataUsage {
    @JsonProperty
    private String type = "DataUsage";
    @JsonProperty
    private Long dataUsedInBytes;
    @JsonProperty
    private Long amountSpent;
    @JsonProperty
    private String fromDate;
    @JsonProperty
    private String toDate;
    @JsonProperty
    private Integer week;
    @JsonProperty
    private BigDecimal dataUsageCost;


    public Long getAmountSpent() {
        return amountSpent;
    }

    public void setAmountSpent(Long amountSpent) {
        this.amountSpent = amountSpent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getDataUsedInBytes() {
        return dataUsedInBytes;
    }

    public void setDataUsedInBytes(Long dataUsedInBytes) {
        this.dataUsedInBytes = dataUsedInBytes;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public BigDecimal getDataUsageCost() {
        return dataUsageCost;
    }

    public void setDataUsageCost(BigDecimal dataUsageCost) {
        this.dataUsageCost = dataUsageCost;
    }
}
