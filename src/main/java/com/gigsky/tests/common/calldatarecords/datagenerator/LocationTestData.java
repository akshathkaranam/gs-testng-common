package com.gigsky.tests.common.calldatarecords.datagenerator;



public class LocationTestData {

    String iccId;
    Long subAccountId;
    Long zoneId;
    String zoneName;
    Long groupId;
    Long userId;
    String dayKey;

    String countryCode;
    String countryName;
    String imsi;
    String msisdn;
    String imsiProfileName;
    String mcc, mnc;
    Long initialDelayTimeMs;
    String carrierName;
    String source;
    String connectionType;
    String connectionTechnologyType;

    public String getIccId() {
        return iccId;
    }

    public LocationTestData setIccId(String iccId) {
        this.iccId = iccId;
        return this;
    }

    public Long getZoneId() {
        return zoneId;
    }

    public LocationTestData setZoneId(Long zoneId) {
        this.zoneId = zoneId;return this;
    }

    public String getZoneName() {
        return zoneName;
    }

    public LocationTestData setZoneName(String zoneName) {
        this.zoneName = zoneName; return this;
    }

    public Long getSubAccountId() {
        return subAccountId;
    }

    public LocationTestData setSubAccountId(Long subAccountId) {
        this.subAccountId = subAccountId;return this;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public LocationTestData setCountryCode(String code) {
        this.countryCode = code;return this;
    }

    public String getCountryName() {
        return countryName;
    }

    public LocationTestData setCountryName(String countryName) {
        this.countryName = countryName;return this;
    }

    public String getMcc() {
        return mcc;
    }

    public LocationTestData setMcc(String mcc) {
        this.mcc = mcc; return this;
    }

    public String getMnc() {
        return mnc;
    }

    public LocationTestData setMnc(String mnc) {
        this.mnc = mnc; return this;
    }

    public Long getGroupId() {
        return groupId;
    }

    public LocationTestData setGroupId(Long groupId) {
        this.groupId = groupId; return this;
    }

    public Long getUserId() {
        return userId;
    }

    public LocationTestData setUserId(Long userId) {
        this.userId = userId; return this;
    }

    public String getImsi() {
        return imsi;
    }

    public LocationTestData setImsi(String imsi) {
        this.imsi = imsi; return this;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public LocationTestData setMsisdn(String msisdn) {
        this.msisdn = msisdn; return this;
    }

    public String getImsiProfileName() {
        return imsiProfileName;
    }

    public LocationTestData setImsiProfileName(String imsiProfileName) {
        this.imsiProfileName = imsiProfileName; return this;
    }

    public String getDayKey() {
        return dayKey;
    }

    public LocationTestData setDayKey(String dayKey) {
        this.dayKey = dayKey; return this;
    }

    public String getCarrierName() {
        return this.carrierName;
    }

    public LocationTestData setCarrierName(String carrierName) {
        this.carrierName = carrierName;return this;
    }

    public Long getInitialDelayTimeMs() {
        return initialDelayTimeMs;
    }

    public LocationTestData setInitialDelayTimeMs(Long initialDelayTimeMs) {
        this.initialDelayTimeMs = initialDelayTimeMs;
        return this;
    }

    public String getSource() {
        return source;
    }

    public LocationTestData setSource(String source) {
        this.source = source;
        return this;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public LocationTestData setConnectionType(String connectionType) {
        this.connectionType = connectionType;
        return this;
    }

    public String getConnectionTechnologyType() {
        return connectionTechnologyType;
    }

    public LocationTestData setConnectionTechnologyType(String connectionTechnologyType) {
        this.connectionTechnologyType = connectionTechnologyType;
        return this;
    }
}
