package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;


import java.util.ArrayList;
import java.util.List;

public class EnrichedLocationUpdate extends LocationInfo {

    protected String accountType;
    protected String accountName;
    protected Long parentAccountId;
    protected String timeZone;
    protected String carrierName;
    private String countryCode;
    private String countryName;
    protected String roamingProfileRevision;
    protected String roamingProfileName;
    protected Long userId;
    protected String status;
    protected Long enterpriseAccountId;
    protected Long roamingProfileId;
    protected Long groupAccountId;
    //initial delay time
    private Long initialDelayTimeMs = 0l;
    public EnrichedLocationUpdate(){
        eventType = "EnrichedLocationUpdate";
        operationType = "UPSERT";
    }

    public EnrichedLocationUpdate(Long timestamp, String operationType, Long locationId,   String iccId, String imsi, String msisdn, String mcc,
                                  String mnc, String connectionType, Long startTime, Long groupAccountId, Long enterpriseAccountId, Long userId, Long roamingProfileId,
                                  String countryCode, String countryName, String imsiProfileName, String timeZone) {

        this.eventType = "EnrichedLocationUpdate";
        this.operationType = operationType;
        this.eventTime = timestamp;
        this.locId = locationId;
        this.iccId = iccId;
        this.imsi = imsi;
        this.msisdn = msisdn;
        this.mcc = mcc;
        this.mnc = mnc;
        this.connectionType = connectionType;
        this.imsiProfile =imsiProfileName;
        this.recordStartTime = startTime;
        this.userId = userId;
        this.groupAccountId = groupAccountId;
        this.enterpriseAccountId = enterpriseAccountId;
        this.roamingProfileId = roamingProfileId;
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.timeZone = timeZone;
        this.locationTime = startTime;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getEnterpriseAccountId() {
        return enterpriseAccountId;
    }

    public void setEnterpriseAccountId(Long enterpriseAccountId) {
        this.enterpriseAccountId = enterpriseAccountId;
    }

    public Long getRoamingProfileId() {
        return roamingProfileId;
    }

    public void setRoamingProfileId(Long roamingProfileId) {
        this.roamingProfileId = roamingProfileId;
    }

    public Long getGroupAccountId() {
        return groupAccountId;
    }

    public void setGroupAccountId(Long groupAccountId) {
        this.groupAccountId = groupAccountId;
    }

    public String getRoamingProfileRevision() {
        return roamingProfileRevision;
    }

    public void setRoamingProfileRevision(String roamingProfileRevision) {
        this.roamingProfileRevision = roamingProfileRevision;
    }

    public String getRoamingProfileName() {
        return roamingProfileName;
    }

    public void setRoamingProfileName(String roamingProfileName) {
        this.roamingProfileName = roamingProfileName;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Long getParentAccountId() {
        return parentAccountId;
    }

    public void setParentAccountId(Long parentAccountId) {
        this.parentAccountId = parentAccountId;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

//    public void updateAccountInfo(EnrichedAccountInfo accountInfo){
//        this.accountName = accountInfo.getAccountName();
//        this.accountType = accountInfo.getAccountType();
//        this.parentAccountId = accountInfo.getParentAccountId();
//        this.timeZone = accountInfo.getTimeZone();
//    }
//    public void copyInfo(RPInfoEnrichedLocation rpInfoEnrichedLocation){
//        this.rpCarrierList = rpInfoEnrichedLocation.getRpCarrierList();
//        this.roamingProfileName = rpInfoEnrichedLocation.getRoamingProfileName();
//        this.roamingProfileRevision = rpInfoEnrichedLocation.getRoamingProfileRevision();
//        super.copyInfo(rpInfoEnrichedLocation);
//
//    }

    @Override
    public String toString(){
        return "EnrichedLocationInfo{ "+
                " locationUpdateid=" + locId +
                ", connectionType" + connectionType +
                ", countryCode='" + countryCode + '\'' +
                ", countryName='" + countryName + '\'' +
                ", iccId='" + iccId + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", mcc='" + mcc + '\'' +
                ", mnc='" + mnc + '\'' +
                ", eventTime='" + eventTime + '\'' +
                ", roamingProfileName=" + roamingProfileName +
                ", roamingProfileRevision=" + roamingProfileRevision +
                ", groupAccountId=" + groupAccountId +
                ", enterpriseAccountId=" + enterpriseAccountId +
                ", parentAccountID=" + parentAccountId +
                ", userId=" + userId +
                ", roamingProfileId=" + roamingProfileId +
                ", timeAtGSBackend=" + timeAtGSBackend +
                ", source=" + source +
                ", carrierName=" + carrierName +
                ", connectionTechnologyType=" + connectionTechnologyType +
                ", status=" + status +
                ", locationTime=" + locationTime +
                ", timeZone='" + timeZone + '\'' +
                '}';

    }
}
