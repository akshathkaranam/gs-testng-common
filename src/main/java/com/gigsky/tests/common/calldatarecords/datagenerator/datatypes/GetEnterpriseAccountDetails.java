package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "accountType",
        "accountId",
        "accountName",
        "status",
        "accountDescription",
        "accountSubscriptionType",
        "alertVersionSupported",
        "parentAccountId",
        "createdOn",
        "company",
        "alertSetting",
        "pricingModelVersionSupported"
})
public class GetEnterpriseAccountDetails{

    @JsonProperty("type")
    private String type;
    @JsonProperty("accountType")
    private String accountType;
    @JsonProperty("accountId")
    private String accountId;
    @JsonProperty("accountName")
    private String accountName;
    @JsonProperty("status")
    private String status;
    @JsonProperty("accountDescription")
    private String accountDescription;
    @JsonProperty("accountSubscriptionType")
    private String accountSubscriptionType;
    @JsonProperty("alertVersionSupported")
    private String alertVersionSupported;
    @JsonProperty("parentAccountId")
    private String parentAccountId;
    @JsonProperty("createdOn")
    private String createdOn;
    @JsonProperty("company")
    private Company company;
    @JsonProperty("alertSetting")
    private AlertSetting alertSetting;
    @JsonProperty("pricingModelVersionSupported")
    private String pricingModelVersionSupported;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("accountType")
    public String getAccountType() {
        return accountType;
    }

    @JsonProperty("accountType")
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    @JsonProperty("accountId")
    public String getAccountId() {
        return accountId;
    }

    @JsonProperty("accountId")
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @JsonProperty("accountName")
    public String getAccountName() {
        return accountName;
    }

    @JsonProperty("accountName")
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("accountDescription")
    public String getAccountDescription() {
        return accountDescription;
    }

    @JsonProperty("accountDescription")
    public void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }

    @JsonProperty("accountSubscriptionType")
    public String getAccountSubscriptionType() {
        return accountSubscriptionType;
    }

    @JsonProperty("accountSubscriptionType")
    public void setAccountSubscriptionType(String accountSubscriptionType) {
        this.accountSubscriptionType = accountSubscriptionType;
    }

    @JsonProperty("alertVersionSupported")
    public String getAlertVersionSupported() {
        return alertVersionSupported;
    }

    @JsonProperty("alertVersionSupported")
    public void setAlertVersionSupported(String alertVersionSupported) {
        this.alertVersionSupported = alertVersionSupported;
    }

    @JsonProperty("parentAccountId")
    public String getParentAccountId() {
        return parentAccountId;
    }

    @JsonProperty("parentAccountId")
    public void setParentAccountId(String parentAccountId) {
        this.parentAccountId = parentAccountId;
    }

    @JsonProperty("createdOn")
    public String getCreatedOn() {
        return createdOn;
    }

    @JsonProperty("createdOn")
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @JsonProperty("company")
    public Company getCompany() {
        return company;
    }

    @JsonProperty("company")
    public void setCompany(Company company) {
        this.company = company;
    }

    @JsonProperty("alertSetting")
    public AlertSetting getAlertSetting() {
        return alertSetting;
    }

    @JsonProperty("alertSetting")
    public void setAlertSetting(AlertSetting alertSetting) {
        this.alertSetting = alertSetting;
    }

    @JsonProperty("pricingModelVersionSupported")
    public String getPricingModelVersionSupported() {
        return pricingModelVersionSupported;
    }

    @JsonProperty("pricingModelVersionSupported")
    public void setPricingModelVersionSupported(String pricingModelVersionSupported) {
        this.pricingModelVersionSupported = pricingModelVersionSupported;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
