package com.gigsky.tests.common.calldatarecords.datagenerator;

import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.DBResultSet;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.EnrichedLocationUpdate;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.LocationEvent;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.LocationEventList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by karthik on 7/8/19
 */

/**
 *    fromDate=yyyy-mm-dd                       (mandatory)
 *    toDate=yyyy-mm-dd                         (mandatory)
 *    startIndex = 0                            (mandatory)
 *    count = 10                                (mandatory)
 *    filters = filters (country and imsiTypes) (optional)
 *    sortBy=country | date                     (mandatory)
 *    sortDirection= ASC|DESC                   (mandatory)
 *
 */
public class SimLocEventResBuilder extends LocationBuilder {

    HashSet<String> iccId = new HashSet<>();
    HashSet<String> countryName = new HashSet<>();
    List<DBResultSet> resultSet = new ArrayList<>();
    private LocationEventList locationEventBean = new LocationEventList();

    private void generateResultSet(Long accountID, List<String> countryList, List<String> imsiTypes,String fromdate,String todate) {

        try {
            final Long fromDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-zzz").parse(fromdate+"-UTC").getTime();
            final Long toDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-zzz").parse(todate+"-UTC").getTime();

            List<EnrichedLocationUpdate> filteredList;
            if(!countryList.isEmpty()&&!imsiTypes.isEmpty()) {
                filteredList = enrichedLUList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .filter(e -> countryList.contains(e.getCountryCode()))
                        .filter(e -> imsiTypes.contains(e.getImsiProfile()))
                        .collect(Collectors.toList());

            }else if(countryList.isEmpty() && imsiTypes.isEmpty()){
                filteredList = enrichedLUList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .collect(Collectors.toList());

            }else if(imsiTypes.isEmpty()){
                filteredList = enrichedLUList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .filter(e -> countryList.contains(e.getCountryCode()))
                        .collect(Collectors.toList());
            }else{
                filteredList = enrichedLUList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .filter(e -> imsiTypes.contains(e.getImsiProfile()))
                        .collect(Collectors.toList());
            }

            resultSet.add(aggregate(filteredList));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    public DBResultSet aggregate(List<EnrichedLocationUpdate> cdrList){
        for(EnrichedLocationUpdate enrichedLU:cdrList){
            iccId.add(enrichedLU.getIccId());
            countryName.add(enrichedLU.getCountryName());

        }
        DBResultSet resultSet = new DBResultSet();
        resultSet.setEnrichedLUList(cdrList);
        return resultSet;
    }

    public void getSIMLocationUpdateResponse(Long accountId ,String accountName,  Map<String,Object> queryParams ){
        String imsis= null;
        String countries =null;
        String fromDate = queryParams.get("fromDate").toString();
        String toDate = queryParams.get("toDate").toString();
        if(queryParams.get("filters")!=null) {
            String filterList = queryParams.get("filters").toString();
            if (filterList != null && filterList.contains("country") && filterList.contains("imsi")) {
                countries = queryParams.get("filters").toString().split("&")[0].substring(9);
                imsis = queryParams.get("filters").toString().split("&")[1].substring(10);

            } else if (filterList != null && filterList.contains("country")) {
                countries = queryParams.get("filters").toString().substring(9);
            } else {
                imsis = queryParams.get("filters").toString().substring(10);
            }
        }
        int count = (int)queryParams.get("count");
        int startIndex = (int)queryParams.get("startIndex");
        String sortByDirection = queryParams.get("sortDirection").toString();
        String sortBy = queryParams.get("sortBy").toString();

        List<String> countryList;
        if(countries!=null){
            countryList =  Arrays.asList(countries.split(","));
        }else {
            countryList = new ArrayList<>();
        }
        List<String> imsiTypes;
        if(imsis!=null) {
            imsiTypes = Arrays.asList(imsis.split(","));
        }
        else {
            imsiTypes= new ArrayList<>();
        }
        generateResultSet(accountId,countryList,imsiTypes,fromDate,toDate);
        List<DBResultSet> accountIdResultSet = new ArrayList<>(resultSet);


        Comparator<LocationEvent> comparator = getComparator(sortBy, sortByDirection);
        ArrayList<LocationEvent> reqCount = new ArrayList<>();

        ArrayList<String > iccids =new ArrayList<>();
        Iterator<String> iccID = iccId.iterator();
        while (iccID.hasNext()) {
            iccids.add(iccID.next());
        }

        ArrayList<String > countrynames =new ArrayList<>();
        Iterator<String> ctr = countryName.iterator();
        while (ctr.hasNext()) {
            countrynames.add(ctr.next());
        }

        Iterator<String> iterator=countryName.iterator();
        for(int cnt=0;cnt<iccids.size();cnt++){
            for(int ccnt=0;ccnt<countrynames.size();ccnt++){
                List<LocationEvent> simCDREventList = DBResultToDAOcdrEventConverter.convertToLU(accountIdResultSet, accountName, iccids.get(cnt),countrynames.get(ccnt));
                if(simCDREventList!=null && simCDREventList.size()>0){
                    Collections.sort(simCDREventList, comparator);
                    reqCount.add(simCDREventList.get(0));
                }
            }
        }

        Collections.sort(reqCount,comparator);

        ArrayList<LocationEvent> pagination = new ArrayList<>();
        for(int cnt=startIndex;reqCount.size()>cnt&&cnt<count+startIndex;cnt++){
            pagination.add(reqCount.get(cnt));

        }

//        locationEventBean.setTotalCount(reqCount.size());
        locationEventBean.setCount(pagination.size());
        locationEventBean.setFromDate(fromDate);
        locationEventBean.setToDate(toDate);
        if(pagination.size()>0){
            locationEventBean.setList(pagination);
        }
        locationEventBean.setStartIndex(startIndex);

    }

    private Comparator<LocationEvent> getComparator(String sortBy, String sortByDirection){
        Comparator<LocationEvent> comparator = null;
        switch (sortBy.toLowerCase()){
            case "date":{
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-zzz");
                if(sortByDirection.equals("ASC")){
                    comparator = new Comparator<LocationEvent>() {
                        @Override
                        public int compare(LocationEvent o1, LocationEvent o2) {
                            Long o1Date=null,o2Date=null;
                            try {
                                o1Date = dateFormat.parse(o1.getLocationTime() + "-UTC").getTime();
                                o2Date = dateFormat.parse(o2.getLocationTime() + "-UTC").getTime();
                            } catch (ParseException p) {
                                p.getMessage();
                            }
                            return (o1Date > o2Date ? 1 : 0);
                        }
                    };
                }else{
                    comparator = new Comparator<LocationEvent>() {
                        @Override
                        public int compare(LocationEvent o1, LocationEvent o2) {
                            Long o1Date=null,o2Date=null;
                            try {
                                o1Date = dateFormat.parse(o1.getLocationTime() + "-UTC").getTime();
                                o2Date = dateFormat.parse(o2.getLocationTime() + "-UTC").getTime();
                            } catch (ParseException p) {
                                p.getMessage();
                            }
                            return (o1Date > o2Date ? -1 : 0);
                        }
                    };
                }
            }break;
        }

        return comparator;

    }


    public LocationEventList getSimLocationEvent(){
        return locationEventBean;
    }
}
