package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;


/**
 * Created on 20/07/2019
 */

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LocationActivityList {


    @JsonProperty
    private String type = "LocationActivityList";
    @JsonProperty
    private String fromDate;
    @JsonProperty
    private String toDate;
    @JsonProperty
    private String groupBy;
    @JsonProperty @Ignore
    private Integer startIndex;
    @JsonProperty @Ignore
    private Integer count;
    @JsonProperty @Ignore
    private Integer totalCount;
    @JsonProperty
    private List<LocationActivity> list;




    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getFromDate()
    {
        return fromDate;
    }
    public void setFromDate(String fromDate)
    {
        this.fromDate = fromDate;
    }

    public String getToDate()
    {
        return toDate;
    }
    public void setToDate(String toDate)
    {
        this.toDate = toDate;
    }

    public String getGroupBy()
    {
        return groupBy;
    }
    public void setGroupBy(String groupBy)
    {
        this.groupBy = groupBy;
    }

    public Integer getStartIndex()
    {
        return startIndex;
    }
    public void setStartIndex(Integer startIndex)
    {
        this.startIndex = startIndex;
    }

    public Integer getCount()
    {
        return count;
    }
    public void setCount(Integer count)
    {
        this.count = count;
    }

    public Integer getTotalCount()
    {
        return totalCount;
    }
    public void setTotalCount(Integer totalCount)
    {
        this.totalCount = totalCount;
    }

    public List<LocationActivity> getList()
    {
        return list;
    }

    public void setList(List<LocationActivity> list)
    {
        this.list = list;
    }




}
