package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

/**
 * Created by apandit on 7/1/19.
 */
public class SimLocationActivityBean {
    /*{
    “sims”: 50,
	“countryCode”: “US”,
	“countryName”: “United States”
    }
*/
    Integer sims;
    String countryCode;
    String countryName;
    Long totalCount ;


    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }


    public Integer getSims() {
        return sims;
    }

    public void setSims(Integer sims) {
        this.sims = sims;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

}
