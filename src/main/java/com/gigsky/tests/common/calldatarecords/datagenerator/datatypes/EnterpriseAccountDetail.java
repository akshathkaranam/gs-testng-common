package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "rootAccountDetails",
        "parentAccountDetails",
        "selfAccountDetails"
})
public class EnterpriseAccountDetail {

    @JsonProperty("type")
    private String type;
    @JsonProperty("rootAccountDetails")
    private RootAccountDetails rootAccountDetails;
    @JsonProperty("parentAccountDetails")
    private List<ParentAccountDetail> parentAccountDetails = null;
    @JsonProperty("selfAccountDetails")
    private SelfAccountDetails selfAccountDetails;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("rootAccountDetails")
    public RootAccountDetails getRootAccountDetails() {
        return rootAccountDetails;
    }

    @JsonProperty("rootAccountDetails")
    public void setRootAccountDetails(RootAccountDetails rootAccountDetails) {
        this.rootAccountDetails = rootAccountDetails;
    }

    @JsonProperty("parentAccountDetails")
    public List<ParentAccountDetail> getParentAccountDetails() {
        return parentAccountDetails;
    }

    @JsonProperty("parentAccountDetails")
    public void setParentAccountDetails(List<ParentAccountDetail> parentAccountDetails) {
        this.parentAccountDetails = parentAccountDetails;
    }

    @JsonProperty("selfAccountDetails")
    public SelfAccountDetails getSelfAccountDetails() {
        return selfAccountDetails;
    }

    @JsonProperty("selfAccountDetails")
    public void setSelfAccountDetails(SelfAccountDetails selfAccountDetails) {
        this.selfAccountDetails = selfAccountDetails;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
