package com.gigsky.tests.common.calldatarecords.datagenerator;


//import com.mongodb.client.AggregateIterable;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class DBResultToDaoResultConverter {
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static List<DataUsageResultSetBean> convert(List<DBResultSet> dbResultSetAggregateIterable, Long accountId, String iccId, Long userId, String fromDate, String toDate, AtomicLong rowCount) {
        List<DataUsageResultSetBean> dataUsageResultSetBeanList = new ArrayList<>();

        try {

            for (DBResultSet dbResultSet : dbResultSetAggregateIterable) {
                DataUsageResultSetBean dataUsageResultSet = new DataUsageResultSetBean();

                //Set accountId
                dataUsageResultSet.setEnt_account_id((accountId == null || accountId == 0) ? null : accountId);

                //Set account name
                dataUsageResultSet.setEnt_account_name((StringUtils.isEmpty(dbResultSet.getAccountName())) ?
                        null : dbResultSet.getAccountName());
                //Set iccId
                dataUsageResultSet.setIccId((StringUtils.isEmpty(iccId)) ? null : iccId);

                if (StringUtils.isNotEmpty(dbResultSet.getIccId())) {
                    dataUsageResultSet.setIccId(dbResultSet.getIccId());
                }

                //Set userId
                dataUsageResultSet.setUser_uid((userId == null || userId == 0) ? null : userId);

                //Setting userId from dbResult set
                if (dbResultSet.getUserId() != null) {
                    dataUsageResultSet.setUser_uid(dbResultSet.getUserId());
                }

                //Set userFirstName
                dataUsageResultSet
                        .setUser_account_first_name((StringUtils.isEmpty(dbResultSet.getFirstName())) ?
                                null : dbResultSet.getFirstName());

                //Set userLastName
                dataUsageResultSet.setUser_account_last_name((StringUtils.isEmpty(dbResultSet.getLastName())) ?
                        null : dbResultSet.getLastName());

                //Set userEmailId
                dataUsageResultSet.setUser_account_email((StringUtils.isEmpty(dbResultSet.getEmailId())) ?
                        null : dbResultSet.getEmailId());

                //Set userLocation
                dataUsageResultSet.setUser_account_location((StringUtils.isEmpty(dbResultSet.getLocation())) ?
                        null : dbResultSet.getLocation());

                //Set Zone related details
                dataUsageResultSet.setZone_id((dbResultSet.getZoneId() == null || dbResultSet.getZoneId() == 0) ?
                        null : dbResultSet.getZoneId());

                dataUsageResultSet.setZone_name((StringUtils.isEmpty(dbResultSet.getZoneName())) ?
                        null : dbResultSet.getZoneName());

                //Data usage
                dataUsageResultSet.setData_usage_bytes(
                        (dbResultSet.getTotalUsageInBytes() == null || dbResultSet.getTotalUsageInBytes() == 0) ?
                                null : dbResultSet.getTotalUsageInBytes());


                //Day, week, Month
                String monthKey = dbResultSet.getMonthKey(); //ex: 2018-11
                String weekKey = dbResultSet.getWeekKey();
                String dayKey = dbResultSet.getDayKey();    //ex: 2018-11-14

                if (monthKey != null) {

                    dataUsageResultSet.setStart_date(getFromDateFromMonthKey(monthKey));
                    dataUsageResultSet.setEnd_date(getToDateFromMonthKey(monthKey));
                } else if (weekKey != null) {
                    dataUsageResultSet.setStart_date(getFromDateFromWeekKey(weekKey, fromDate));
                    dataUsageResultSet.setEnd_date(getToDateFromWeekKey(weekKey, toDate));
                } else if (dayKey != null) {

                    dataUsageResultSet.setStart_date(getDateFromDayKey(dayKey));
                    dataUsageResultSet.setEnd_date(getDateFromDayKey(dayKey));
                }

                //Group related details
                dataUsageResultSet.setGroup_account_id(
                        (dbResultSet.getGroupAccountId() == null || dbResultSet.getGroupAccountId() == 0) ?
                                null : dbResultSet.getGroupAccountId());
                dataUsageResultSet.setGroup_account_name((StringUtils.isEmpty(dbResultSet.getAccountName())) ?
                        null : dbResultSet.getAccountName());

                dataUsageResultSet.setAccount_type(
                        (StringUtils.isEmpty(dbResultSet.getAccountType()) ?
                                null : dbResultSet.getAccountType())
                );

                //Add the Object to the list
                dataUsageResultSetBeanList.add(dataUsageResultSet);

                dataUsageResultSet.setDayKey(dbResultSet.getDayKey());
                dataUsageResultSet.setMonthKey(dbResultSet.getMonthKey());

                if (weekKey != null) {
                    String[] weekKeySplit = weekKey.split("-");
                    String year = weekKeySplit[0];
                    String week = weekKeySplit[1];
                    dataUsageResultSet.setP_week(year + week);
                }
            }
//            logger.info("Inside convert 2");
        } catch (Exception e) {
//            logger.error("Exception occured", e);
//            logger.info("Exception occured", e);
        }

        rowCount.set(dataUsageResultSetBeanList.size());
//        System.out.printf(String.valueOf(dataUsageResultSetBeanList));
//        logger.info("DAO layer response: "+String.valueOf(dataUsageResultSetBeanList));
        return dataUsageResultSetBeanList;
    }

    //yyyy-mm into yyyy-mm-dd HH:mm:ss
    //First day of the month
    public static String getFromDateFromMonthKey(String monthKey) {
        return monthKey + "-01";
    }

    //yyyy-mm into yyyy-mm-dd HH:mm:ss
    //Computes the last day of the month
    public static String getToDateFromMonthKey(String monthKey) throws ParseException {
        String toDate = monthKey + "-01";
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        Date convertedDate = dateFormat.parse(toDate);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(convertedDate);
        int lastDOM = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        return monthKey + "-" + String.format("%02d", lastDOM);
    }

    //yyyy-mm-dd into yyyy-mm-dd HH:mm:ss
    public static String getDateFromDayKey(String dayKey) {
        return dayKey;
    }

    public static String getFromDateFromWeekKey(String weekKey, String fromDate) {

        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            Date convertedFromDate = dateFormat.parse(fromDate);

            String[] weekKeySplit = weekKey.split("-");
            String year = weekKeySplit[0];
            String week = weekKeySplit[1];
            DateTime startDt = new DateTime()
                    .withYear(Integer.parseInt(year))
                    .withWeekOfWeekyear(Integer.parseInt(week))
                    .withDayOfWeek(1); //first Day of Week

            if (startDt.toDate().before(convertedFromDate)) {
                startDt = new DateTime(convertedFromDate);
            }

            int startDayMonth = startDt.getMonthOfYear();
            int startOfWeek = startDt.getDayOfMonth();

            return year + "-" + String.format("%02d", startDayMonth) + "-" + String.format("%02d", startOfWeek);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    //yyyy-ww where 'ww' is week number
    public static String getToDateFromWeekKey(String weekKey, String toDate) {

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            Date convertedToDate = dateFormat.parse(toDate);
            String[] weekKeySplit = weekKey.split("-");
            String year = weekKeySplit[0];
            String week = weekKeySplit[1];

            DateTime endDt = new DateTime()
                    .withYear(Integer.parseInt(year))
                    .withWeekOfWeekyear(Integer.parseInt(week))
                    .withDayOfWeek(7); //last Day of Week

            if (endDt.toDate().after(convertedToDate)) {
                endDt = new DateTime(convertedToDate);
            }

            int endDayMonth = endDt.getMonthOfYear();
            int endOfWeek = endDt.getDayOfMonth();

            return year + "-" + String.format("%02d", endDayMonth) + "-" + String.format("%02d", endOfWeek);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static List<CDRActivity> convert(List<DBResultSet> dbResultSets, Long accountId, String fromDate, String toDate) {
        List<CDRActivity> simLocationActivityBeanList = new ArrayList<>();

        for (DBResultSet dbResultSet : dbResultSets) {
            Country country =new Country();
            CDRActivity simLocationActivityBean = new CDRActivity();
            simLocationActivityBean.setSims(dbResultSet.getSimCount());
            if(accountId!=null){
                country.setCode(dbResultSet.getCountryCode());
                country.setName(dbResultSet.getCountryName());
            }
            simLocationActivityBean.setCountry(country);
            simLocationActivityBeanList.add(simLocationActivityBean);

        }
        return simLocationActivityBeanList;
    }

    public static List<LocationActivity> convertToLU(List<DBResultSet> dbResultSets, Long accountId, String fromDate, String toDate) {
        List<LocationActivity> simLocationActivityBeanList = new ArrayList<>();

        for (DBResultSet dbResultSet : dbResultSets) {
            Country country =new Country();
            LocationActivity simLocationActivityBean = new LocationActivity();
            simLocationActivityBean.setSims(dbResultSet.getSimCount());
            if(accountId!=null){
                country.setCode(dbResultSet.getCountryCode());
                country.setName(dbResultSet.getCountryName());
            }
            simLocationActivityBean.setCountry(country);
            simLocationActivityBeanList.add(simLocationActivityBean);

        }
        return simLocationActivityBeanList;
    }


}

