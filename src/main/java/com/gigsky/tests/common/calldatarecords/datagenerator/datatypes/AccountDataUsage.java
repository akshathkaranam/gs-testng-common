/*
 * Copyright (c) 2016. GigSky, Inc.
 * All rights reserved.
 *
 * THIS SOFTWARE IS THE CONFIDENTIAL AND PROPRIETARY
 * INFORMATION OF GIGSKY, INC.
 *
 * UNAUTHORIZED USE OR DISCLOSURE IS PROHIBITED.
 *
 * https://www.gigsky.com/
 */

package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

/**
 * Created on 11/29/15.
 */

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AccountDataUsage {
    @JsonProperty
    private String type = "AccountDataUsageList";
    @JsonProperty
    private Integer count;
    @JsonProperty
    private Long totalCount;
    @JsonProperty
    private String groupByPeriod;
    @JsonProperty
    private AtomicLong totalDataUsedInBytes;
    @JsonProperty
    private BigDecimal totalDataUsageCost;
    @JsonProperty
    private List<DataUsageList> list;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGroupByPeriod() {
        return groupByPeriod;
    }

    public void setGroupByPeriod(String groupByPeriod) {
        this.groupByPeriod = groupByPeriod;
    }

    public AtomicLong getTotalDataUsedInBytes() {
        return totalDataUsedInBytes;
    }

    public void setTotalDataUsedInBytes(AtomicLong totalDataBytesUsedInBytes) {
        this.totalDataUsedInBytes = totalDataBytesUsedInBytes;
    }

    public List<DataUsageList> getList() {
        return list;
    }

    public void setList(List<DataUsageList> list) {
        this.list = list;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public BigDecimal getTotalDataUsageCost() {
        return totalDataUsageCost;
    }

    public void setTotalDataUsageCost(BigDecimal totalDataUsageCost) {
        this.totalDataUsageCost = totalDataUsageCost;
    }
}
