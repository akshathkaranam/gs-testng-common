package com.gigsky.tests.common.calldatarecords.datagenerator;

import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.CDREvent;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.CDREventList;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.DBResultSet;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.EnrichedCDR;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by karthik on 7/8/19
 */

/**
 *    fromDate=yyyy-mm-dd                       (mandatory)
 *    toDate=yyyy-mm-dd                         (mandatory)
 *    startIndex = 0                            (mandatory)
 *    count = 10                                (mandatory)
 *    filters = filters (country and imsiTypes) (optional)
 *    sortBy=country | date                     (mandatory)
 *    sortDirection= ASC|DESC                   (mandatory)
 *
 */
public class SimCdrEventResBuilder extends CDRBuilder {

    HashSet<String> iccId = new HashSet<>();
    HashSet<String> countryName = new HashSet<>();
    List<DBResultSet> resultSet = new ArrayList<>();
    private CDREventList simCDREventBean = new CDREventList();

    private void generateResultSet(Long accountID, List<String> countryList, List<String> imsiTypes,String fromdate,String todate) {

        try {
            final Long fromDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-zzz").parse(fromdate+"-UTC").getTime();
            final Long toDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-zzz").parse(todate+"-UTC").getTime();

          List<EnrichedCDR> filteredList;
            if(!countryList.isEmpty()&&!imsiTypes.isEmpty()) {
                filteredList = enrichedCDRList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .filter(e -> countryList.contains(e.getCountryCode()))
                        .filter(e -> imsiTypes.contains(e.getImsiProfile()))
                        .collect(Collectors.toList());

            }else if(countryList.isEmpty() && imsiTypes.isEmpty()){
                filteredList = enrichedCDRList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .collect(Collectors.toList());

            }else if(imsiTypes.isEmpty()){
                filteredList = enrichedCDRList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .filter(e -> countryList.contains(e.getCountryCode()))
                        .collect(Collectors.toList());
            }else{
                filteredList = enrichedCDRList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .filter(e -> imsiTypes.contains(e.getImsiProfile()))
                        .collect(Collectors.toList());
            }

            resultSet.add(aggregate(filteredList));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    public DBResultSet aggregate(List<EnrichedCDR> cdrList){
        for(EnrichedCDR enrichedCDR:cdrList){
            iccId.add(enrichedCDR.getIccId());
            countryName.add(enrichedCDR.getCountryName());

        }
        DBResultSet resultSet = new DBResultSet();
        resultSet.setEnrichedCDRList(cdrList);
        return resultSet;
    }

    public void getSIMCDREventResponse(Long accountId ,String accountName,  Map<String,Object> queryParams ){
        String imsis= null;
        String countries =null;
        String fromDate = queryParams.get("fromDate").toString();
        String toDate = queryParams.get("toDate").toString();
        if(queryParams.get("filters")!=null) {
            String filterList = queryParams.get("filters").toString();
            if (filterList != null && filterList.contains("country") && filterList.contains("imsi")) {
                countries = queryParams.get("filters").toString().split("&")[0].substring(9);
                imsis = queryParams.get("filters").toString().split("&")[1].substring(10);

            } else if (filterList != null && filterList.contains("country")) {
                countries = queryParams.get("filters").toString().substring(9);
            } else {
                imsis = queryParams.get("filters").toString().substring(10);
            }
        }
        int count = (int)queryParams.get("count");
        int startIndex = (int)queryParams.get("startIndex");
        String sortByDirection = queryParams.get("sortDirection").toString();
        String sortBy = queryParams.get("sortBy").toString();

        List<String> countryList;
        if(countries!=null){
            countryList =  Arrays.asList(countries.split(","));
        }else {
            countryList = new ArrayList<>();
        }
        List<String> imsiTypes;
        if(imsis!=null) {
           imsiTypes = Arrays.asList(imsis.split(","));
        }
        else {
            imsiTypes= new ArrayList<>();
        }
        generateResultSet(accountId,countryList,imsiTypes,fromDate,toDate);
        List<DBResultSet> accountIdResultSet = new ArrayList<>(resultSet);


        Comparator<CDREvent> comparator = getComparator(sortBy, sortByDirection);
        ArrayList<CDREvent> reqCount = new ArrayList<>();

        ArrayList<String > iccids =new ArrayList<>();
        Iterator<String> iccID = iccId.iterator();
        while (iccID.hasNext()) {
            iccids.add(iccID.next());
        }

        ArrayList<String > countrynames =new ArrayList<>();
        Iterator<String> ctr = countryName.iterator();
        while (ctr.hasNext()) {
            countrynames.add(ctr.next());
        }

        Iterator<String> iterator=countryName.iterator();
        for(int cnt=0;cnt<iccids.size();cnt++){
           for(int ccnt=0;ccnt<countrynames.size();ccnt++){
                List<CDREvent> simCDREventList = DBResultToDAOcdrEventConverter.convert(accountIdResultSet, accountName, iccids.get(cnt),countrynames.get(ccnt));
                if(simCDREventList!=null && simCDREventList.size()>0){
                Collections.sort(simCDREventList, comparator);
                reqCount.add(simCDREventList.get(0));
                }
            }
        }

        Collections.sort(reqCount,comparator);

        ArrayList<CDREvent> pagination = new ArrayList<>();
        for(int cnt=startIndex;reqCount.size()>cnt&&cnt<count+startIndex;cnt++){
            pagination.add(reqCount.get(cnt));

        }

//        simCDREventBean.setTotalCount(reqCount.size());
        simCDREventBean.setCount(pagination.size());
        simCDREventBean.setFromDate(fromDate);
        simCDREventBean.setToDate(toDate);
        if(pagination.size()>0){
        simCDREventBean.setList(pagination);
        }
        simCDREventBean.setStartIndex(startIndex);

    }

    private Comparator<CDREvent> getComparator(String sortBy, String sortByDirection){
        Comparator<CDREvent> comparator = null;
        switch (sortBy.toLowerCase()){
            case "date":{
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-zzz");
                if(sortByDirection.equals("ASC")){
                    comparator = new Comparator<CDREvent>() {
                        @Override
                        public int compare(CDREvent o1, CDREvent o2) {
                            Long o1Date=null,o2Date=null;
                            try {
                               o1Date = dateFormat.parse(o1.getRecordStartTime() + "-UTC").getTime();
                               o2Date = dateFormat.parse(o2.getRecordStartTime() + "-UTC").getTime();
                            } catch (ParseException p) {
                                p.getMessage();
                            }
                            return (o1Date > o2Date ? 1 : 0);
                        }
                    };
                }else{
                    comparator = new Comparator<CDREvent>() {
                        @Override
                        public int compare(CDREvent o1, CDREvent o2) {
                            Long o1Date=null,o2Date=null;
                            try {
                                o1Date = dateFormat.parse(o1.getRecordStartTime() + "-UTC").getTime();
                                o2Date = dateFormat.parse(o2.getRecordStartTime() + "-UTC").getTime();
                            } catch (ParseException p) {
                                p.getMessage();
                            }
                            return (o1Date > o2Date ? -1 : 0);
                        }
                    };
                }
            }break;
        }

        return comparator;

    }


    public CDREventList getSimCDREvent(){
        return simCDREventBean;
    }
}
