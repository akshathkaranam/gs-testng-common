package com.gigsky.tests.common.calldatarecords.datagenerator;

import com.gigsky.cdrmodule.psm.decoder.PSMGPRSRecord;
import com.gigsky.tests.common.beans.Mccmnc;
import com.gigsky.tests.common.calldatarecords.beans.CDRData;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.GROUP_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_DIRECTION;
import com.gigsky.tests.common.calldatarecords.datagenerator.interfaces.ResponseBuilderInterface;

import java.util.*;


/**
 * Created by apandit on 12/27/18.
 */
public class CDRBuilder  implements ResponseBuilderInterface {

    private String cdrType = "NORMAL";
    protected List<EnrichedCDR> enrichedCDRList = new ArrayList<>();
    protected List<TestAccountCDRConfig> testAccountCDRConfigs;

    public void parse(List<TestAccountCDRConfig> accountCDRConfigList) {
        testAccountCDRConfigs = accountCDRConfigList;
        EnrichedCDR enrichedCDR = null;
        Long currentTime = System.currentTimeMillis();
        String iccId;
        Long accountIdKey, zoneId, userId, timeStamp, groupId, subAccountId;
        Long cdrId = 1048660l;
        String imsi, msisdn, zoneName;


        for (TestAccountCDRConfig accountConfig : accountCDRConfigList) {
            for (SimData simData : accountConfig.getSimDataList()) {
                zoneId = simData.getZoneId();
                zoneName = simData.getZoneName();
                accountIdKey = simData.getAccountId();
                groupId = simData.getGroupId();
                userId = simData.getUserId();
                iccId = simData.getIccId();
                imsi = simData.getImsi();
                msisdn = simData.getMsisdn();
                timeStamp = simData.getTimeStamp();
                subAccountId = simData.getSubAccountId();
                if (simData.getCdrType() != null) {
                    cdrType = simData.getCdrType();
                }
                enrichedCDR = new EnrichedCDR(timeStamp, "UPSERT", cdrId++, 1l, cdrType, iccId, imsi, msisdn, simData.getMcc(), simData.getMnc(), simData.getUplinkBytes(), simData.getDownlinkBytes(), timeStamp, 60l, timeStamp, groupId, accountIdKey, subAccountId, userId, null, simData.countryCode, simData.countryName, simData.imsiProfileName, zoneId, zoneName == null ? "Zone-" + zoneId : zoneName, simData.getTimeZone());
                enrichedCDR.setProcessTime(timeStamp);
                enrichedCDR.setAccessPointName("gigsky");
                enrichedCDR.setNodeID("psm");
                enrichedCDR.setCdrTrafficType("DATA");
                enrichedCDR.setCarrierName(simData.getCarrierName());
                enrichedCDR.setLocalSequenceNumber(1200000000234l);
                enrichedCDR.setIngestionTime(currentTime);
                enrichedCDR.setImsiProfile(simData.getImsiProfileName());
                enrichedCDR.setInitialDelayTimeMs(simData.getInitialDelayMs());
                enrichedCDRList.add(enrichedCDR);

            }
        }
    }

    public List<EnrichedCDR> getEnrichedCDRList() {
        return enrichedCDRList;
    }

    public List<CDRData> convertToCDRData(List<TestAccountCDRConfig> accountCDRConfigList) {
        List<CDRData> cdrDataList = new ArrayList<>();
        String trafficType = "NORMAL";
        parse(accountCDRConfigList);

        Collections.sort(enrichedCDRList, new Comparator<EnrichedCDR>() {
            @Override
            public int compare(EnrichedCDR o1, EnrichedCDR o2) {
                return (o1.getRecordStartTime() < o2.getRecordStartTime()) ? -1 : (o1.getRecordStartTime() > o2.getRecordStartTime()) ? 1 : 0;
            }
        });

        for (EnrichedCDR eCdr : enrichedCDRList) {
            CDRData cdrData = new CDRData();
            Calendar recordOpeningTime = Calendar.getInstance();
            recordOpeningTime.setTimeInMillis(eCdr.getRecordStartTime());
            Mccmnc mccMnc = new Mccmnc();
            mccMnc.setMcc(Integer.parseInt(eCdr.getMcc()));
            mccMnc.setMnc(Integer.parseInt(eCdr.getMnc()));
            cdrData.setCDRData(Long.valueOf(eCdr.getImsi()), Long.valueOf(eCdr.getMsisdn()), mccMnc, null, eCdr.getUplinkVolume().intValue(), eCdr.getDownlinkVolume().intValue(), recordOpeningTime);
            trafficType = eCdr.getCdrType();
            cdrData.setTrafficType(PSMGPRSRecord.ServiceVolume.TrafficType.valueOf(trafficType));
            cdrDataList.add(cdrData);
        }

        return cdrDataList;
    }

    @Override
    public void generateResultSet(Object id, GROUP_BY groupByType) {
    }

    @Override
    public Object getAccountDataUsageResponse(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection, int startIndex, int count, String fromDate, String toDate, List<TestAccountCDRConfig> accountCDRConfigList) {
        return null;
    }

    @Override
    public Object getSimDataUsageResponse(String iccId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection, int startIndex, int count, String fromDate, String toDate, List<TestAccountCDRConfig> accountCDRConfigList) {
        return null;
    }

    @Override
    public Object getUserDataUsageResponse(Long userId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection, int startIndex, int count, String fromDate, String toDate, List<TestAccountCDRConfig> accountCDRConfigList) {
        return null;
    }

    @Override
    public AccountDataUsage getAccountDataUsageDistributedBySims(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection,
                                                                 int startIndex, int count, String fromDate, String toDate,
                                                                 List<TestAccountCDRConfig> accountCDRConfigList,
                                                                 GetEnterpriseAccountDetails account, GetUserListOfEntAccount userDetails) {
        return null;
    }

    @Override
    public AccountDataUsage getAccountDataUsageDistributedByUsers(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection,
                                                                  int startIndex, int count, String fromDate, String toDate,
                                                                  List<TestAccountCDRConfig> accountCDRConfigList,
                                                                  GetEnterpriseAccountDetails account, GetUserListOfEntAccount userDetails) {
        return null;
    }

    @Override
    public AccountDataUsage getAccountDataUsageDistributedByZones(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection,
                                                                  int startIndex, int count, String fromDate, String toDate,
                                                                  List<TestAccountCDRConfig> accountCDRConfigList, GetZoneListOfAnAcc accountZoneList, GetEnterpriseAccountDetails account) {
        return null;
    }

    @Override
    public AccountDataUsage getAccountDataUsageDistributedByGroups(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection,
                                                                   int startIndex, int count, String fromDate, String toDate,
                                                                   List<TestAccountCDRConfig> accountCDRConfigList,
                                                                   GetEnterpriseAccountDetails account, List<GetEnterpriseAccountDetails> accountList){
        return null;
    }
}


