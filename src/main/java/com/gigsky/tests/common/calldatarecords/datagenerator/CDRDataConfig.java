package com.gigsky.tests.common.calldatarecords.datagenerator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigsky.tests.common.beans.Mccmnc;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.CDREventList;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.GROUP_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_DIRECTION;
import com.gigsky.tests.common.calldatarecords.impl.AccountResponseBuilder;
import com.gigsky.tests.common.calldatarecords.impl.CDRSender;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by apandit on 7/29/18.
 */
public class CDRDataConfig {
    private List<CDRTestData> cdrTestDataList = new ArrayList<>();
    private static final int MAX_CDR_COUNT = 10;

    public List<CDRTestData> getCdrTestDataList() {
        return cdrTestDataList;
    }

    public void setCdrTestDataList(List<CDRTestData> cdrTestDataList) {
        this.cdrTestDataList = cdrTestDataList;
    }


    public static List<CDRTestData> getCDRListDailyUsageLimit(String fromDate, String toDate, //format: yyyy-mm-dd
                                             String iccId, Long userId,
                                             Long groupId, Long zoneId,
                                             String countryCode, String countryName,
                                             String mcc, String mnc,
                                             String imsi, String msisdn, String imsiProfileName,
                                             Long dailyUsageUpperLimit) throws ParseException {

        List<CDRTestData> cdrTestDataList = new ArrayList<>();
        Long uplinkBytes, downlinkBytes;
        List<String> dayKeys = getDayKeysBetweenDates(fromDate, toDate);

        Double randomDivider;
        Double part;
        for(String day : dayKeys){
            randomDivider = Math.random();
            part = dailyUsageUpperLimit * randomDivider;
            uplinkBytes = Math.round(part.longValue()*(double)0.25);
            downlinkBytes = part.longValue() - uplinkBytes;
            if(downlinkBytes < 0)
                downlinkBytes = 0l;

            cdrTestDataList.add(new CDRTestData().setIccId(iccId).setUplinkBytes(uplinkBytes).setDownlinkBytes(downlinkBytes).setDayKey(day).
                    setZoneId(zoneId).setUserId(userId).setGroupId(groupId).setCountryCode(countryCode).setCountryName(countryName).setMcc(mcc).setMnc(mnc).
                    setImsi(imsi).setMsisdn(msisdn).setImsiProfileName(imsiProfileName));
        }

        return cdrTestDataList;
    }

    public static List<CDRTestData> getCDRDataList(Map<String,String> cdrParams) throws ParseException {
        String fromDate = cdrParams.get("fromDate");
        String toDate = cdrParams.get("toDate");
        //format: yyyy-mm-dd
        String iccId=cdrParams.get("iccId");
        String carrierName=cdrParams.get("carrierName");
        String countryCode = cdrParams.get("countryCode");
        String countryName= cdrParams.get("countryName");
        String mcc=cdrParams.get("mcc");
        String mnc =cdrParams.get("mnc");
        String imsi=cdrParams.get("imsi");
        String msisdn=cdrParams.get("msisdn");
        String imsiProfileName=cdrParams.get("imsiProfileName");
        String cdrType = cdrParams.get("cdrUsageType");

        Long dailyUsageUpperLimit = 45000l;
        List<CDRTestData> cdrTestDataList = new ArrayList<>();
        Long uplinkBytes, downlinkBytes;
        List<String> dayKeys = getDayKeysWithTimeBetweenDates(fromDate, toDate);

        Double randomDivider;
        Double part;
        for (String day : dayKeys) {
            randomDivider = Math.random();
            part = dailyUsageUpperLimit * randomDivider;
            uplinkBytes = Math.round(part.longValue() * (double) 0.25);
            downlinkBytes = part.longValue() - uplinkBytes;
            if (downlinkBytes < 0)
                downlinkBytes = 0l;

            cdrTestDataList.add(new CDRTestData().setIccId(iccId).setUplinkBytes(uplinkBytes).setDownlinkBytes(downlinkBytes).setDayKey(day)
                    .setCountryCode(countryCode).setCountryName(countryName).setMcc(mcc).setMnc(mnc).setCarrierName(carrierName)
                    .setImsi(imsi).setMsisdn(msisdn).setImsiProfileName(imsiProfileName).setCdrType(cdrType));
        }

        return cdrTestDataList;
    }

    public List<CDRTestData> getCDRListTotalUsageLimit(String fromDate, String toDate, //format: yyyy-mm-dd
                                                       String iccId, Long userId,
                                                       Long groupId, Long zoneId,
                                                       String countryCode, String countryName,
                                                       String mcc, String mnc,
                                                       String imsi, String msisdn, String imsiProfileName,
                                                       Long totalDataUsageLimit) throws ParseException {

        List<CDRTestData> cdrTestDataList = new ArrayList<>();
        Long uplinkBytes, downlinkBytes;
        List<String> dayKeys = getDayKeysBetweenDates(fromDate, toDate);

        Long dailyUsage = totalDataUsageLimit/dayKeys.size();

        for(String day : dayKeys){
            uplinkBytes = dailyUsage/2;
            downlinkBytes = dailyUsage/2;
            cdrTestDataList.add(new CDRTestData().setIccId(iccId).setUplinkBytes(uplinkBytes).setDownlinkBytes(downlinkBytes).setDayKey(day).
                    setZoneId(zoneId).setUserId(userId).setGroupId(groupId).setCountryCode(countryCode).setCountryName(countryName).setMcc(mcc).setMnc(mnc).
                    setImsi(imsi).setMsisdn(msisdn).setImsiProfileName(imsiProfileName));
        }

        return cdrTestDataList;
    }

    public static List<String> getDayKeysBetweenDates(String fromDate, String toDate) throws ParseException {

        List<String> dayKeys = new ArrayList<>();

        String dayKey = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate, endDate;
        startDate = sdf.parse(fromDate);
        endDate = sdf.parse(toDate);

        if(startDate.after(endDate)){
            System.out.println("Invalid from and to date");
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);

        while(cal.getTime().before(endDate)|| cal.getTime().equals(endDate)){
            dayKey = sdf.format(cal.getTime());
            dayKeys.add(dayKey);
            cal.add(Calendar.DATE, 1);
        }

        return dayKeys;
    }

    public static List<String> getDayKeysWithTimeBetweenDates(String fromDate, String toDate) throws ParseException {

        List<String> dayKeys = new ArrayList<>();

        String dayKey = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-zzz");
        Date startDate, endDate;
        startDate = sdf.parse(fromDate+"-UTC");
        endDate = sdf.parse(toDate+"-UTC");
        if(startDate.after(endDate)){
            System.out.println("Invalid from and to date");
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        int cdrCountPerDay;
        while(cal.getTime().before(endDate)|| cal.getTime().equals(endDate)){
            long millis = cal.getTimeInMillis();
            cdrCountPerDay = 1 + (int)((MAX_CDR_COUNT-1) * Math.random());
            for(int i = 0; i < cdrCountPerDay; i++) {
                millis += 305000;
                Date dateTime = new Date(millis);
                dayKey = sdf.format(dateTime);
                dayKeys.add(dayKey);
            }
            cal.add(Calendar.DATE, 1);
        }
        return dayKeys;
    }

    public static void main(String args[]) throws Exception {

        TestAccountCDRConfig accountCDRConfig = new TestAccountCDRConfig(19653l);
//        TestAccountCDRConfig accountCDRConfig1 = new TestAccountCDRConfig(2l);

        List<CDRTestData> cdrTestDataList = getCDRListDailyUsageLimit("2018-02-24", "2018-03-02", "100204800110000", 1l, 2l, 3l, "IN", "INDIA", "400", "401","1234", "5678","TDC", 450000l);
//        List<CDRTestData> cdrTestDataList1 = getCDRListDailyUsageLimit("2018-01-01", "2018-01-05", "100304800110000", 1l, 2l, 3l, "AU", "AUSTRALIA", "400", "401","3456","7890", "TDC", 450000l);
        accountCDRConfig.setAccountDataUsage(cdrTestDataList);
//        accountCDRConfig1.setAccountDataUsage(cdrTestDataList1);
        AccountResponseBuilder arb = new AccountResponseBuilder();
//        arb.parse(Arrays.asList(accountCDRConfig,accountCDRConfig1));
        Long accountId = 19653l;
        arb.getAccountDataUsageResponse(accountId, GROUP_BY.DAY, SORT_BY.DATA_USAGE, SORT_DIRECTION.ASC, 0, 10, "2018-02-24", "2018-03-02",
                Arrays.asList(accountCDRConfig));
        arb.getAccountDataUsageList();

//        SimCDRActivityResBuilder srb = new SimCDRActivityResBuilder();
//        srb.parse(Arrays.asList(accountCDRConfig,accountCDRConfig1));
//        srb.getSIMCDRActivityResponse(accountId,GROUP_BY.COUNTRY,SORT_BY.COUNTRY,SORT_DIRECTION.ASC,0,10,"2018-02-24", "2018-03-02");
//        srb.getSimCDRActivity();

//        SimCdrEventResBuilder simCdrEventResBuilder = new SimCdrEventResBuilder();
//        simCdrEventResBuilder.parse(Arrays.asList(accountCDRConfig,accountCDRConfig1));
//        simCdrEventResBuilder.getSIMCDREventResponse(accountId,GROUP_BY.DAY,SORT_BY.COUNTRY,SORT_DIRECTION.ASC,0,2,"2017-02-24", "2019-03-02");
//        CDREventList eventList = simCdrEventResBuilder.getSimCDREvent();
//        System.out.println(eventList);
//        ObjectMapper Obj = new ObjectMapper();
//
//        try {
//
//            // get Oraganisation object as a json string
//            String jsonStr = Obj.writeValueAsString(eventList);
//
//            // Displaying JSON String
//            System.out.println(jsonStr);
//        }
//
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//        CDRBuilder cdrBuilder = new CDRBuilder();
//        CDRSender.getInstance().sendBulkCDR(cdrBuilder.convertToCDRData(Arrays.asList(accountCDRConfig,accountCDRConfig1)));
    }
}
