package com.gigsky.tests.common.calldatarecords.datagenerator;

import com.gigsky.tests.common.calldatarecords.beans.CDRData;

/**
 * Created by apandit on 7/29/18.
 */
public class CDRTestData {

    String iccId;
    Long subAccountId;
    Long zoneId;
    String zoneName;
    Long groupId;
    Long userId;
    String dayKey;
    Long uplinkBytes;
    Long downlinkBytes;
    String countryCode;
    String countryName;
    String imsi;
    String msisdn;
    String imsiProfileName;
    String mcc, mnc;
    Long initialDelayTimeMs;
    String carrierName;
    String cdrType;


    public String getIccId() {
        return iccId;
    }

    public CDRTestData setIccId(String iccId) {
        this.iccId = iccId;
        return this;
    }

    public Long getZoneId() {
        return zoneId;
    }

    public CDRTestData setZoneId(Long zoneId) {
        this.zoneId = zoneId;return this;
    }

    public String getZoneName() {
        return zoneName;
    }

    public CDRTestData setZoneName(String zoneName) {
        this.zoneName = zoneName; return this;
    }

    public Long getSubAccountId() {
        return subAccountId;
    }

    public CDRTestData setSubAccountId(Long subAccountId) {
        this.subAccountId = subAccountId;return this;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public CDRTestData setCountryCode(String code) {
        this.countryCode = code;return this;
    }

    public String getCountryName() {
        return countryName;
    }

    public CDRTestData setCountryName(String countryName) {
        this.countryName = countryName;return this;
    }

    public String getMcc() {
        return mcc;
    }

    public CDRTestData setMcc(String mcc) {
        this.mcc = mcc; return this;
    }

    public String getMnc() {
        return mnc;
    }

    public CDRTestData setMnc(String mnc) {
        this.mnc = mnc; return this;
    }

    public Long getGroupId() {
        return groupId;
    }

    public CDRTestData setGroupId(Long groupId) {
        this.groupId = groupId; return this;
    }

    public Long getUserId() {
        return userId;
    }

    public CDRTestData setUserId(Long userId) {
        this.userId = userId; return this;
    }

    public String getImsi() {
        return imsi;
    }

    public CDRTestData setImsi(String imsi) {
        this.imsi = imsi; return this;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public CDRTestData setMsisdn(String msisdn) {
        this.msisdn = msisdn; return this;
    }

    public String getImsiProfileName() {
        return imsiProfileName;
    }

    public CDRTestData setImsiProfileName(String imsiProfileName) {
        this.imsiProfileName = imsiProfileName; return this;
    }

    public String getDayKey() {
        return dayKey;
    }

    public CDRTestData setDayKey(String dayKey) {
        this.dayKey = dayKey; return this;
    }

    public Long getUplinkBytes() {
        return uplinkBytes;
    }

    public CDRTestData setUplinkBytes(Long uplinkBytes) {
        this.uplinkBytes = uplinkBytes; return this;
    }

    public Long getDownlinkBytes() {
        return downlinkBytes;
    }

    public CDRTestData setDownlinkBytes(Long downlinkBytes) {
        this.downlinkBytes = downlinkBytes;return this;
    }

    public String getCarrierName() {
        return this.carrierName;
    }

    public CDRTestData setCarrierName(String carrierName) {
        this.carrierName = carrierName;return this;
    }

    public Long getInitialDelayTimeMs() {
        return initialDelayTimeMs;
    }

    public CDRTestData setInitialDelayTimeMs(Long initialDelayTimeMs) {
        this.initialDelayTimeMs = initialDelayTimeMs;
        return this;
    }

    public String getCdrType() {
        return cdrType;
    }

    public CDRTestData setCdrType(String cdrType) {
        this.cdrType = cdrType; return this;
    }
}
