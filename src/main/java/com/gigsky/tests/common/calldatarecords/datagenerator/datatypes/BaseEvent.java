package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

/**
 * Created by anant on 28/02/18.
 */
public class BaseEvent {
    protected String eventType;
    protected Long eventTime;
    protected Long ingestionTime;
    protected Long processTime;
//    @JsonIgnore
    protected String operationType;

//    @JsonIgnore
//    private Long initialDelayTimeMs = 0l;

    public BaseEvent() {
    }

    public BaseEvent(String eventType) {
        this.eventType = eventType;
    }

    public Long getEventTime() {
        return eventTime;
    }

    public void setEventTime(Long eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Long getIngestionTime() {
        return ingestionTime;
    }

    public void setIngestionTime(Long ingestionTime) {
        this.ingestionTime = ingestionTime;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Long getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Long processTime) {
        this.processTime = processTime;
    }

    public Long getInitialDelayTimeMs() {
        return 0l;
    }

    public void setInitialDelayTimeMs(Long initialDelayTimeMs) {

    }

}
