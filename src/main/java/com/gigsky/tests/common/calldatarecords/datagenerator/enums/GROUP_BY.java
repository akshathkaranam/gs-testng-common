package com.gigsky.tests.common.calldatarecords.datagenerator.enums;

/**
 * Created by apandit on 7/30/19.
 */
public enum GROUP_BY {
    DAY,
    WEEK,
    MONTH,
    COUNTRY,
    IMSI_TYPE
}
