package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "userId",
        "firstName",
        "lastName",
        "emailId",
        "address",
        "userStatus",
        "createdOn",
        "homePhone",
        "mobile",
        "location",
        "dataUsedInBytes",
        "totalSimsAssociated",
        "accountDetails"
})
public class GetUserDetails{

    @JsonProperty("type")
    private String type;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("emailId")
    private String emailId;
    @JsonProperty("address")
    private Address address;
    @JsonProperty("userStatus")
    private String userStatus;
    @JsonProperty("createdOn")
    private String createdOn;
    @JsonProperty("homePhone")
    private String homePhone;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("location")
    private String location;
    @JsonProperty("dataUsedInBytes")
    private Integer dataUsedInBytes;
    @JsonProperty("totalSimsAssociated")
    private Integer totalSimsAssociated;
    @JsonProperty("accountDetails")
    private AccountDetails accountDetails;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("firstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("firstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("lastName")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("lastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("emailId")
    public String getEmailId() {
        return emailId;
    }

    @JsonProperty("emailId")
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    @JsonProperty("userStatus")
    public String getUserStatus() {
        return userStatus;
    }

    @JsonProperty("userStatus")
    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    @JsonProperty("createdOn")
    public String getCreatedOn() {
        return createdOn;
    }

    @JsonProperty("createdOn")
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @JsonProperty("homePhone")
    public String getHomePhone() {
        return homePhone;
    }

    @JsonProperty("homePhone")
    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    @JsonProperty("mobile")
    public String getMobile() {
        return mobile;
    }

    @JsonProperty("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

    @JsonProperty("dataUsedInBytes")
    public Integer getDataUsedInBytes() {
        return dataUsedInBytes;
    }

    @JsonProperty("dataUsedInBytes")
    public void setDataUsedInBytes(Integer dataUsedInBytes) {
        this.dataUsedInBytes = dataUsedInBytes;
    }

    @JsonProperty("totalSimsAssociated")
    public Integer getTotalSimsAssociated() {
        return totalSimsAssociated;
    }

    @JsonProperty("totalSimsAssociated")
    public void setTotalSimsAssociated(Integer totalSimsAssociated) {
        this.totalSimsAssociated = totalSimsAssociated;
    }

    @JsonProperty("accountDetails")
    public AccountDetails getAccountDetails() {
        return accountDetails;
    }

    @JsonProperty("accountDetails")
    public void setAccountDetails(AccountDetails accountDetails) {
        this.accountDetails = accountDetails;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

