package com.gigsky.tests.common.calldatarecords.impl;

import com.gigsky.tests.common.calldatarecords.datagenerator.CDRBuilder;
import com.gigsky.tests.common.calldatarecords.datagenerator.CommonUtil;
import com.gigsky.tests.common.calldatarecords.datagenerator.DBResultToDaoResultConverter;
import com.gigsky.tests.common.calldatarecords.datagenerator.TestAccountCDRConfig;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.GROUP_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_DIRECTION;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by apandit on 8/9/19.
 */
public class AccountZonesResponseBuilder extends CDRBuilder {

    private List<DBResultSet> resultSet = new ArrayList<>();

    @Override
    public void generateResultSet(Object id, GROUP_BY groupByType) {

        Function<EnrichedCDR, String> identity = CommonUtil.getClassifier(groupByType);

        Long accountId = Long.parseLong(String.valueOf(id));
        //One cdr per day
        Map<Long, Map<String, List<EnrichedCDR>>> zoneWiseGroupedCDRs = enrichedCDRList.stream().filter(e -> e.getEnterpriseAccountId().equals(accountId)).collect(
                Collectors.groupingBy(e -> e.getZoneId(),
                        Collectors.groupingBy(identity)));

        for (Map.Entry<Long, Map<String, List<EnrichedCDR>>> zoneMap : zoneWiseGroupedCDRs.entrySet()) {
            for (Map.Entry<String, List<EnrichedCDR>> groupByTypeMap : zoneMap.getValue().entrySet()) {
                DBResultSet dbResultSet = CommonUtil.aggregateResultSet(groupByTypeMap.getValue());
                dbResultSet.setZoneId(zoneMap.getKey());
                resultSet.add(dbResultSet);
            }
        }

    }

    public AccountDataUsage getAccountDataUsageDistributedByZones(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection,
                                            int startIndex, int count, String fromDate, String toDate,
                                            List<TestAccountCDRConfig> accountCDRConfigList, GetZoneListOfAnAcc accountZoneList, GetEnterpriseAccountDetails account) {
        parse(accountCDRConfigList);
        generateResultSet(accountId, groupByPeriod);
        List<DBResultSet> accountIdResultSet = resultSet.stream().skip(startIndex).limit(count).collect(Collectors.toList());

        List<DataUsageResultSetBean> dataUsageResultSetList = null;

//        int recordsCount = 0;
//        String lastAvailableDate = null;
        AtomicLong rowCount = new AtomicLong();
        dataUsageResultSetList = DBResultToDaoResultConverter.convert(accountIdResultSet,
                accountId, null, null, fromDate, toDate, rowCount);

        AccountDataUsage accountDataUsage = buildAccountDataUsageByZones(groupByPeriod, rowCount.get(), dataUsageResultSetList,
                sortByDirection, accountZoneList, fromDate, account, startIndex, count);


//        for (DataUsageResultSetBean dataUsageResultSet : dataUsageResultSetList) {
//            CommonUtil.buildDataUsageList(dataUsageResultSet, accountDataUsageList, groupByPeriod);
//        }
//
//        List<DataUsage> dataUsageList = accountDataUsageList.getList();
//
//        if(dataUsageList != null) {
//            recordsCount = dataUsageList.size();
//            Comparator<DataUsage> comparator = CommonUtil.getComparator(sortBy, sortByDirection);
//            Collections.sort(accountDataUsageList.getList(), comparator);
//        }
//        else{
//            recordsCount = 0;
//        }
//
//
//
//        int listIndex = 0;
//        if (sortByDirection == SORT_DIRECTION.ASC) {
//
//            listIndex = dataUsageResultSetList.size() - 1;
//        }
//
//
//        if (recordsCount > 0) {
//            lastAvailableDate = accountDataUsageList.getList().get(listIndex).getToDate();
//        }
//
//        accountDataUsageList.setGroupByPeriod(groupByPeriod.name());
//        accountDataUsageList.setTotalCount(rowCount.get());
//        accountDataUsageList.setCount(recordsCount);
//        accountDataUsageList.setLastAvailableDate(lastAvailableDate);

        return accountDataUsage;

    }


    private AccountDataUsage buildAccountDataUsageByZones(GROUP_BY grpByPeriod, long totalRecordCount,
                                                          List<DataUsageResultSetBean> dataUsageResultSetBeanList, SORT_DIRECTION sortDirection,
                                                          GetZoneListOfAnAcc accountZoneList, String fromDate, GetEnterpriseAccountDetails account, Integer startIndex,
                                                          Integer pagCount) {
        AccountDataUsage accountDataUsageByZones = new AccountDataUsage();

        Map<String, DataUsageList> dataUsageListForAllZones = new LinkedHashMap<String, DataUsageList>();
        AtomicLong totalDataUsedInBytesOfAllZones = new AtomicLong(0);
        BigDecimal totalDataUsageCost = new BigDecimal(0.0);

        List<DataUsageList> dataUsageListOfZones = accountDataUsageByZones.getList();
        if (dataUsageListOfZones == null) {
            dataUsageListOfZones = new ArrayList<DataUsageList>();
        }
        if (CollectionUtils.isNotEmpty(dataUsageResultSetBeanList)) {
            //For every record that the query returned
            for (DataUsageResultSetBean dataUsageResultSetBean : dataUsageResultSetBeanList) {
                String zoneId = dataUsageResultSetBean.getZone_id();
                if (StringUtils.isNotEmpty(zoneId) && (!zoneId.equals("null"))) {
                    //Check if the map already contains entry for this user
                    //If not create one
                    if (!dataUsageListForAllZones.containsKey(zoneId)) {
                        dataUsageListForAllZones.put(zoneId, new DataUsageList());
                    }
                    //Get the entry for this user
                    DataUsageList dataUsageListOfZone = dataUsageListForAllZones.get(zoneId);

                    CommonUtil.buildDataUsageList(dataUsageResultSetBean, dataUsageListOfZone, grpByPeriod);

                    //Set other user parameters
                    if (dataUsageResultSetBean.getZone_id() != null) {
                        dataUsageListOfZone.setZoneId(Long.valueOf(dataUsageResultSetBean.getZone_id()));
                    }
                    dataUsageListOfZone.setZoneName(dataUsageResultSetBean.getZone_name());
                }
            }

            //Add all these user's data usage list to the main Data Usage report
            for (Map.Entry<String, DataUsageList> dataUsageListForSingleZone : dataUsageListForAllZones.entrySet()) {
                dataUsageListOfZones.add(dataUsageListForSingleZone.getValue());
                totalDataUsedInBytesOfAllZones.set(totalDataUsedInBytesOfAllZones.addAndGet(
                        dataUsageListForSingleZone.getValue().getTotalDataUsedInBytes().get()));
                if (dataUsageListForSingleZone.getValue().getTotalDataUsageCost() != null) {
                    totalDataUsageCost = totalDataUsageCost.add(dataUsageListForSingleZone.getValue().getTotalDataUsageCost());
                }
            }


            if (accountZoneList.getList() != null && accountZoneList.getList() != null) {
                for (GetZoneDetailsOfAnAcc zoneInfo : accountZoneList.getList()) {
                    boolean isZonePresent = false;
                    for (DataUsageList dataUsageList : dataUsageListOfZones) {
                        if (dataUsageList.getZoneId().equals(zoneInfo.getZoneId())) {
                            dataUsageList.setZoneNickName(zoneInfo.getNickName());
//                            if (isV1_2Account && zoneInfo.getBucketPricingInfo() != null) {
//                                dataUsageList.setMinimumCommitment(new BigDecimal(zoneInfo.getBucketPricingInfo().getMinimumCommitment()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
//                            }
                            isZonePresent = true;
                            break;
                        }
                    }

                    if (!isZonePresent) {
                        totalRecordCount++;
                        DataUsageList dataUsageList = new DataUsageList();
                        dataUsageList.setZoneId(zoneInfo.getZoneId().longValue());
                        dataUsageList.setZoneName(zoneInfo.getName());
                        dataUsageList.setZoneNickName(zoneInfo.getNickName());
                        dataUsageList.setTotalDataUsedInBytes(new AtomicLong(0));
                        dataUsageList.setTotalDataUsageCost(new BigDecimal(0));
//                        if (isV1_2Account && zoneInfo.getBucketPricingInfo() != null) {
//                            dataUsageList.setMinimumCommitment(new BigDecimal(zoneInfo.getBucketPricingInfo().getMinimumCommitment()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
//                        }
                        List<DataUsage> dataUsages = new ArrayList<DataUsage>();
                        DataUsage dataUsage = CommonUtil.getDataUsageForUnlistGroupsZones(fromDate);
                        /*if (dataUsageDao instanceof com.gigsky.eods.dao.impl.mongodb.DataUsageDaoImpl) {
                            dataUsage.setToDate(dataUsageResultSetBeanList.get(0).getEnd_date() + " 00:00:00");
                        }*/
                        dataUsages.add(dataUsage);

                        dataUsageList.setList(dataUsages);
                        dataUsageListOfZones.add(dataUsageList);
                    }
                }
            }

            Comparator<DataUsageList> comparator = CommonUtil.getDataUsageListComparator(sortDirection);
            Collections.sort(dataUsageListOfZones, comparator);

        } else {
            List<GetZoneDetailsOfAnAcc> zoneInfoV2List = accountZoneList.getList();
            DataUsageList dataUsageList;
            if (zoneInfoV2List != null && !zoneInfoV2List.isEmpty()) {
                for (GetZoneDetailsOfAnAcc zoneInfoV2 : zoneInfoV2List) {
                    dataUsageList = new DataUsageList();
                    dataUsageList.setAccountId(Long.valueOf(account.getAccountId()));
                    dataUsageList.setAccountName(account.getAccountName());
                    dataUsageList.setAccountType(account.getAccountType());
//                    if (isV1_2Account && zoneInfoV2.getBucketPricingInfo() != null) {
//                        dataUsageList.setMinimumCommitment(new BigDecimal(zoneInfoV2.getBucketPricingInfo().getMinimumCommitment()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
//                    }
                    dataUsageList.setZoneId(zoneInfoV2.getZoneId().longValue());
                    dataUsageList.setZoneName(zoneInfoV2.getName());
                    dataUsageList.setZoneNickName(zoneInfoV2.getNickName());
                    dataUsageList.setTotalDataUsageCost(BigDecimal.valueOf(0.0));
                    dataUsageList.setTotalDataUsedInBytes(new AtomicLong(0));
                    dataUsageListOfZones.add(dataUsageList);
                }

                int size = zoneInfoV2List.size();
//                if (startIndex > size) {
//                    throw new GigskyAnalyticsServerException(ErrorCode.INVALID_ARGUMENTS);
//                }

                if (pagCount == null) {
                    pagCount = 10; //default count
                }

                if (pagCount > size) {
                    pagCount = size;
                }

                int limit = startIndex + pagCount;
                if (limit > size) {
                    limit = size;
                }

                dataUsageListOfZones = dataUsageListOfZones.subList(startIndex, limit);

                totalRecordCount = size;
            }

        }
        accountDataUsageByZones.setList(dataUsageListOfZones);
        int count = 0;
        if (CollectionUtils.isNotEmpty(accountDataUsageByZones.getList())) {
            count = accountDataUsageByZones.getList().size();
        }
        accountDataUsageByZones.setGroupByPeriod(grpByPeriod.name());
        // If the group by period is anything other than MONTH, then total row count returned by stored procedure will be wrong. So
        //re-write it using the actual number of records in the list.
        if ((grpByPeriod == GROUP_BY.WEEK) || (grpByPeriod == GROUP_BY.DAY)) {
            accountDataUsageByZones.setTotalCount(Long.valueOf(count));
        } else {
            accountDataUsageByZones.setTotalCount(totalRecordCount);
        }
        accountDataUsageByZones.setCount(count);
        accountDataUsageByZones.setTotalDataUsedInBytes(totalDataUsedInBytesOfAllZones);
//        if (calculationMode.equalsIgnoreCase("DEFAULT")) {
//            accountDataUsageByZones.setTotalDataUsageCost(
//                    totalDataUsageCost.setScale(NUMBER_OF_DECIMAL_PLACES, BigDecimal.ROUND_HALF_UP));
//        } else {
//            accountDataUsageByZones.setTotalDataUsageCost(
//                    totalDataUsageCost.setScale(6, BigDecimal.ROUND_HALF_UP));
//        }

        return accountDataUsageByZones;
    }
}
