package com.gigsky.tests.common.calldatarecords.impl;

import com.gigsky.tests.common.calldatarecords.datagenerator.CDRBuilder;
import com.gigsky.tests.common.calldatarecords.datagenerator.CommonUtil;
import com.gigsky.tests.common.calldatarecords.datagenerator.DBResultToDaoResultConverter;
import com.gigsky.tests.common.calldatarecords.datagenerator.TestAccountCDRConfig;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.GROUP_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_DIRECTION;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by apandit on 9/16/19.
 */
public class AccountGroupsResponseBuilder extends CDRBuilder {
    private List<DBResultSet> resultSet = new ArrayList<>();

    @Override
    public void generateResultSet(Object id, GROUP_BY groupByType) {

        Function<EnrichedCDR, String> identity = CommonUtil.getClassifier(groupByType);

        Long accountId = Long.parseLong(String.valueOf(id));
        //One cdr per day
        Map<Long, Map<String, List<EnrichedCDR>>> groupWiseGroupedCDRs = enrichedCDRList.stream().filter(e -> e.getEnterpriseAccountId().equals(accountId)).filter(e -> e.getGroupAccountId() != null).collect(
                Collectors.groupingBy(e -> e.getGroupAccountId(),
                        Collectors.groupingBy(identity)));

        for (Map.Entry<Long, Map<String, List<EnrichedCDR>>> groupMap : groupWiseGroupedCDRs.entrySet()) {
            for (Map.Entry<String, List<EnrichedCDR>> groupByTypeMap : groupMap.getValue().entrySet()) {
                DBResultSet dbResultSet = CommonUtil.aggregateResultSet(groupByTypeMap.getValue());
                dbResultSet.setUserId(groupMap.getKey());
                resultSet.add(dbResultSet);
            }
        }

    }

    @Override
    public AccountDataUsage getAccountDataUsageDistributedByGroups(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection,
                                                                   int startIndex, int count, String fromDate, String toDate,
                                                                   List<TestAccountCDRConfig> accountCDRConfigList,
                                                                   GetEnterpriseAccountDetails account, List<GetEnterpriseAccountDetails> accountList) {

        parse(accountCDRConfigList);
        generateResultSet(accountId, groupByPeriod);
        List<DBResultSet> groupIdResultSet = resultSet.stream().skip(startIndex).limit(count).collect(Collectors.toList());

        CommonUtil.addAccountAndUserDetails(groupIdResultSet, account, null);

        List<DataUsageResultSetBean> dataUsageResultSetList = null;
        AccountDataUsage accountDataUsageByGroups = null;
//        int recordsCount = 0;
//        String lastAvailableDate = null;
        AtomicLong rowCount = new AtomicLong();
        dataUsageResultSetList = DBResultToDaoResultConverter.convert(groupIdResultSet,
                accountId, null, null, fromDate, toDate, rowCount);

        accountDataUsageByGroups = buildAccountDataUsageByGroups(groupByPeriod, rowCount.get(), dataUsageResultSetList,
                sortByDirection, accountList, fromDate);
        return accountDataUsageByGroups;
    }


    private AccountDataUsage buildAccountDataUsageByGroups(GROUP_BY grpByPeriod, long totalRecordCount,
                                                           List<DataUsageResultSetBean> dataUsageResultSetBeanList,
                                                           SORT_DIRECTION sortDirection, List<GetEnterpriseAccountDetails> accountsList, String fromDate) {
        AccountDataUsage accountDataUsageByGroups = new AccountDataUsage();
        Map<String, DataUsageList> dataUsageListForAllGroups = new LinkedHashMap<String, DataUsageList>();
        AtomicLong totalDataUsedInBytesOfAllGroups = new AtomicLong(0);
        BigDecimal totalDataUsageCost = new BigDecimal(0.0);

        List<DataUsageList> dataUsageListOfGroups = accountDataUsageByGroups.getList();
        if (dataUsageListOfGroups == null) {
            dataUsageListOfGroups = new ArrayList<DataUsageList>();
        }

        if (CollectionUtils.isNotEmpty(dataUsageResultSetBeanList)) {
            //For every record that the query returned
            for (DataUsageResultSetBean dataUsageResultSetBean : dataUsageResultSetBeanList) {
                String groupAccountId = dataUsageResultSetBean.getGroup_account_id();
                //Check if the map already contains entry for this user
                //If not create one
                if (!dataUsageListForAllGroups.containsKey(groupAccountId)) {
                    dataUsageListForAllGroups.put(groupAccountId, new DataUsageList());
                }
                //Get the entry for this user
                DataUsageList dataUsageListOfGroup = dataUsageListForAllGroups.get(groupAccountId);

                CommonUtil.buildDataUsageList(dataUsageResultSetBean, dataUsageListOfGroup, grpByPeriod);

                //Set other user parameters
                if (groupAccountId != null && !groupAccountId.equalsIgnoreCase("null")) {
                    dataUsageListOfGroup.setAccountId(Long.valueOf(dataUsageResultSetBean.getGroup_account_id()));
                }
                dataUsageListOfGroup.setAccountType(dataUsageResultSetBean.getAccount_type());
                dataUsageListOfGroup.setAccountName(dataUsageResultSetBean.getGroup_account_name());

            }

            //Add all these user's data usage list to the main Data Usage report
            for (Map.Entry<String, DataUsageList> dataUsageListForSingleGroup : dataUsageListForAllGroups.entrySet()) {
                dataUsageListOfGroups.add(dataUsageListForSingleGroup.getValue());
                totalDataUsedInBytesOfAllGroups.set(totalDataUsedInBytesOfAllGroups.addAndGet(
                        dataUsageListForSingleGroup.getValue().getTotalDataUsedInBytes().get()));
                if (dataUsageListForSingleGroup.getValue().getTotalDataUsageCost() != null) {
                    totalDataUsageCost = totalDataUsageCost.add(dataUsageListForSingleGroup.getValue().getTotalDataUsageCost());
                }
            }
            if (accountsList != null) {
                for (GetEnterpriseAccountDetails account : accountsList) {
                    boolean isAccountPresent = false;
                    for (DataUsageList dataUsageList : dataUsageListOfGroups) {
                        if (account.getAccountId().equals(dataUsageList.getAccountId())) {
                            isAccountPresent = true;
                            break;
                        }
                    }

                    if (!isAccountPresent) {
                        totalRecordCount++;
                        DataUsageList dataUsageList = new DataUsageList();
                        dataUsageList.setAccountId(Long.valueOf(account.getAccountId()));
                        dataUsageList.setAccountName(account.getAccountName());
                        dataUsageList.setAccountType(account.getAccountType());
                        dataUsageList.setTotalDataUsedInBytes(new AtomicLong(0));
                        dataUsageList.setTotalDataUsageCost(new BigDecimal(0));
                        List<DataUsage> dataUsages = new ArrayList<DataUsage>();

                        dataUsages.add(CommonUtil.getDataUsageForUnlistGroupsZones(fromDate));

                        dataUsageList.setList(dataUsages);
                        dataUsageListOfGroups.add(dataUsageList);
                    }
                }
            }


            Comparator<DataUsageList> comparator = CommonUtil.getDataUsageListComparator(sortDirection);
            Collections.sort(dataUsageListOfGroups, comparator);

        }
        accountDataUsageByGroups.setList(dataUsageListOfGroups);

        int count = 0;
        if (CollectionUtils.isNotEmpty(accountDataUsageByGroups.getList())) {
            count = accountDataUsageByGroups.getList().size();
        }
        accountDataUsageByGroups.setGroupByPeriod(grpByPeriod.name());
        // If the group by period is anything other than MONTH, then total row count returned by stored proceudre will be wrong. So
        //re-write it using he actual number of records in the list.
        if ((grpByPeriod == GROUP_BY.WEEK) || (grpByPeriod == GROUP_BY.DAY)) {
            accountDataUsageByGroups.setTotalCount(Long.valueOf(count));
        } else {
            accountDataUsageByGroups.setTotalCount(totalRecordCount);
        }
        accountDataUsageByGroups.setCount(count);
        accountDataUsageByGroups.setTotalDataUsedInBytes(totalDataUsedInBytesOfAllGroups);
//        accountDataUsageByGroups.setTotalDataUsageCost(
//                totalDataUsageCost.setScale(NUMBER_OF_DECIMAL_PLACES, BigDecimal.ROUND_HALF_UP));
        return accountDataUsageByGroups;
    }

}
