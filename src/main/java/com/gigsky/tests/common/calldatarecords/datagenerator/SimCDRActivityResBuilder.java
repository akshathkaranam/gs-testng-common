package com.gigsky.tests.common.calldatarecords.datagenerator;

import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.CDRActivity;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.CDRActivityList;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.DBResultSet;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.EnrichedCDR;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by karthik on 7/8/19
 */

/**
 *    groupBy = COUNTRY                         (mandatory)
 *    fromDate=yyyy-mm-dd                       (mandatory)
 *    toDate=yyyy-mm-dd                         (mandatory)
 *    startIndex = 0                            (mandatory)
 *    count = 10                                (mandatory)
 *    filters = filters (country and imsiTypes) (optional)
 *    sortBy=country | sims                     (mandatory)
 *    sortDirection= ASC|DESC                   (mandatory)
 *
 */

public class SimCDRActivityResBuilder extends CDRBuilder {

    List<DBResultSet> resultSet = new ArrayList<>();
    private CDRActivityList simCDRActivityBean = new CDRActivityList();

    private void generateResultSet(Long accountID,String group_by,List<String> countryList, List<String> imsiTypes,String fromdate,String todate){

        Function<EnrichedCDR, String> identity = null;
        Function<EnrichedCDR, String> getCountryName = (e) -> {
            return e.getCountryName();
        };

        switch (group_by.toLowerCase()) {
            case "country":
                identity = getCountryName;
                break;
        }
        try {
            final Long fromDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-zzz").parse(fromdate+"-UTC").getTime();
            final Long toDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-zzz").parse(todate+"-UTC").getTime();

            Map<String, List<EnrichedCDR>> countryWisegroupedCDRs;
            if(!countryList.isEmpty()&&!imsiTypes.isEmpty()) {
                countryWisegroupedCDRs = enrichedCDRList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .filter(e -> countryList.contains(e.getCountryCode()))
                        .filter(e -> imsiTypes.contains(e.getImsiProfile()))
                        .collect(Collectors.groupingBy(identity));
            }else if(countryList.isEmpty() && imsiTypes.isEmpty()){
                countryWisegroupedCDRs = enrichedCDRList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .collect(Collectors.groupingBy(identity));
//                System.out.println(list);


            }else if(imsiTypes.isEmpty()){
                countryWisegroupedCDRs = enrichedCDRList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .filter(e -> countryList.contains(e.getCountryCode()))
                        .collect(Collectors.groupingBy(identity));
            }else{
                countryWisegroupedCDRs = enrichedCDRList.stream()
                        .filter(e -> e.getEnterpriseAccountId() == accountID.intValue())
                        .filter(e -> e.getRecordStartTime() >= fromDt && e.getRecordStartTime() <= toDate)
                        .filter(e -> imsiTypes.contains(e.getImsiProfile()))
                        .collect(Collectors.groupingBy(identity));
            }


            for (Map.Entry<String, List<EnrichedCDR>> countryMap : countryWisegroupedCDRs.entrySet()) {
                resultSet.add(aggregateResultSet(countryMap.getValue()));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public DBResultSet aggregateResultSet(List<EnrichedCDR> cdrList){
        DBResultSet resultSet = new DBResultSet();
        HashSet<String> hashSet = new HashSet<>();
        for(EnrichedCDR enrichedCDR:cdrList){
            hashSet.add(enrichedCDR.getIccId());
        }
        Integer totalSimCount= hashSet.size();
        EnrichedCDR enrichedCDR = cdrList.get(0);
        resultSet.setSimCount(totalSimCount);
        resultSet.setCountryName(enrichedCDR.getCountryName());
        resultSet.setCountryCode(enrichedCDR.getCountryCode());
        return resultSet;
    }

    public void getSIMCDRActivityResponse(Long accountId , Map<String,Object> queryParams /*String groupBy, String sortBy, String sortByDirection, List<String> countryList, List<String> imsiTypes, int startIndex, int count, String fromDate, String toDate*/){
        String imsis= null;
        String countries =null;
        String groupBy = queryParams.get("groupBy").toString();
        String fromDate = queryParams.get("fromDate").toString();
        String toDate = queryParams.get("toDate").toString();
//        int count = (int)queryParams.get("count");
//        int startIndex = (int)queryParams.get("startIndex");
//        String sortByDirection = queryParams.get("sortDirection").toString();
//        String sortBy = queryParams.get("sortBy").toString();
        if(queryParams.get("filters")!=null) {
            String filterList = queryParams.get("filters").toString();
            if (filterList != null && filterList.contains("country") && filterList.contains("imsi")) {
                countries = queryParams.get("filters").toString().split("&")[0].substring(9);
                imsis = queryParams.get("filters").toString().split("&")[1].substring(10);

            } else if (filterList != null && filterList.contains("country")) {
                countries = queryParams.get("filters").toString().substring(9);
            } else {
                imsis = queryParams.get("filters").toString().substring(10);
            }
        }

        List<String> countryList;
        if(countries!=null){
            countryList =  Arrays.asList(countries.split(","));
        }else {
            countryList = new ArrayList<>();
        }
        List<String> imsiTypes;
        if(imsis!=null) {
            imsiTypes = Arrays.asList(imsis.split(","));
        }
        else {
            imsiTypes= new ArrayList<>();
        }
        generateResultSet(accountId,groupBy,countryList,imsiTypes,fromDate,toDate);
        List<DBResultSet> accountIdResultSet = new ArrayList<>(resultSet);

        List<CDRActivity> simCcdrResultList = null;

        int recordsCount=0;
        String lastAvailableDate = null;
        simCcdrResultList = DBResultToDaoResultConverter.convert(accountIdResultSet,
                accountId,  fromDate, toDate);

//        recordsCount = simCcdrResultList.size();

//        Comparator<CDRActivity> comparator = getComparator(sortByDirection,sortBy);
//        Collections.sort(simCcdrResultList, comparator);

//        ArrayList<CDRActivity> reqCount = new ArrayList<>();
//        for(int cnt=startIndex;simCcdrResultList.size()>cnt&&cnt<count+startIndex;cnt++){
//            reqCount.add(simCcdrResultList.get(cnt));
//        }
//
//        if(count>recordsCount){
//            count = recordsCount;
//        }

        simCDRActivityBean.setList(simCcdrResultList);
        simCDRActivityBean.setGroupBy(groupBy.toString());
//        simCDRActivityBean.setTotalCount(recordsCount);
//        simCDRActivityBean.setCount(count);
//        simCDRActivityBean.setStartIndex(startIndex);
        simCDRActivityBean.setFromDate(fromDate);
        simCDRActivityBean.setToDate(toDate);

    }



    private Comparator<CDRActivity> getComparator( String sortByDirection,String sort_by){
        Comparator<CDRActivity> comparator = null;

        switch (sort_by.toLowerCase()) {
            case "country":
                if (sortByDirection.equals("ASC")) {
                    comparator = new Comparator<CDRActivity>() {
                        @Override
                        public int compare(CDRActivity o1, CDRActivity o2) {
                            return (o1.getCountry().getName().compareToIgnoreCase(o2.getCountry().getName()));
                        }
                    };

                } else {
                    comparator = new Comparator<CDRActivity>() {
                        @Override
                        public int compare(CDRActivity o1, CDRActivity o2) {
                            return (o2.getCountry().getName().compareToIgnoreCase(o1.getCountry().getName()));
                        }
                    };
                }break;
            case "sims":
                if (sortByDirection.equals("ASC")){
                    comparator = new Comparator<CDRActivity>() {
                        @Override
                        public int compare(CDRActivity o1, CDRActivity o2) {
                            return (o1.getSims()>o2.getSims()?1:0);
                        }
                    };

                } else {
                    comparator = new Comparator<CDRActivity>() {
                        @Override
                        public int compare(CDRActivity o1, CDRActivity o2) {
                            return (o1.getSims()>o2.getSims()?-1:0);
                        }
                    };
                }break;
                }

                return comparator;
        }

        public CDRActivityList getSimCDRActivity(){
        return simCDRActivityBean;
    }
}

