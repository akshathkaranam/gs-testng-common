package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;


/**
 * Created by smaragal on Jul, 2019
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "lastAvailableDate",
        "groupByPeriod",
        "totalDataUsedInBytes",
        "totalDataUsageCost ",
        "count",
        "totalCount",
        "list"
})
public class GetDataUsage {

        @JsonProperty("type")
        private String type;
        @JsonProperty("lastAvailableDate")
        private String lastAvailableDate;
        @JsonProperty("groupByPeriod")
        private String groupByPeriod;
        @JsonProperty("totalDataUsedInBytes")
        private Integer totalDataUsedInBytes;
        @JsonProperty("totalDataUsageCost ")
        private Double totalDataUsageCost;
        @JsonProperty("count")
        private Integer count;
        @JsonProperty("totalCount")
        private Integer totalCount;
        @JsonProperty("list")
        private List<DataUsage> list = null;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("type")
        public String getType() {
            return type;
        }

        @JsonProperty("type")
        public void setType(String type) {
            this.type = type;
        }

        @JsonProperty("lastAvailableDate")
        public String getLastAvailableDate() {
            return lastAvailableDate;
        }

        @JsonProperty("lastAvailableDate")
        public void setLastAvailableDate(String lastAvailableDate) {
            this.lastAvailableDate = lastAvailableDate;
        }

        @JsonProperty("groupByPeriod")
        public String getGroupByPeriod() {
            return groupByPeriod;
        }

        @JsonProperty("groupByPeriod")
        public void setGroupByPeriod(String groupByPeriod) {
            this.groupByPeriod = groupByPeriod;
        }

        @JsonProperty("totalDataUsedInBytes")
        public Integer getTotalDataUsedInBytes() {
            return totalDataUsedInBytes;
        }

        @JsonProperty("totalDataUsedInBytes")
        public void setTotalDataUsedInBytes(Integer totalDataUsedInBytes) {
            this.totalDataUsedInBytes = totalDataUsedInBytes;
        }

        @JsonProperty("totalDataUsageCost ")
        public Double getTotalDataUsageCost() {
            return totalDataUsageCost;
        }

        @JsonProperty("totalDataUsageCost ")
        public void setTotalDataUsageCost(Double totalDataUsageCost) {
            this.totalDataUsageCost = totalDataUsageCost;
        }

        @JsonProperty("count")
        public Integer getCount() {
            return count;
        }

        @JsonProperty("count")
        public void setCount(Integer count) {
            this.count = count;
        }

        @JsonProperty("totalCount")
        public Integer getTotalCount() {
            return totalCount;
        }

        @JsonProperty("totalCount")
        public void setTotalCount(Integer totalCount) {
            this.totalCount = totalCount;
        }

        @JsonProperty("list")
        public List<DataUsage> getList() {
            return list;
        }

        @JsonProperty("list")
        public void setList(List<DataUsage> list) {
            this.list = list;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }
