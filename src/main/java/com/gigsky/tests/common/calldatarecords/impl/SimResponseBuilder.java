package com.gigsky.tests.common.calldatarecords.impl;

import com.gigsky.tests.common.calldatarecords.datagenerator.CDRBuilder;
import com.gigsky.tests.common.calldatarecords.datagenerator.CommonUtil;
import com.gigsky.tests.common.calldatarecords.datagenerator.DBResultToDaoResultConverter;
import com.gigsky.tests.common.calldatarecords.datagenerator.TestAccountCDRConfig;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.GROUP_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_DIRECTION;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by apandit on 7/30/19.
 */
public class SimResponseBuilder extends CDRBuilder {
    private List<DBResultSet> resultSet = new ArrayList<>();
    private GetDataUsage simDataUsageList = new GetDataUsage();

    @Override
    public void generateResultSet(Object id, GROUP_BY groupByType) {

        Function<EnrichedCDR, String> identity = CommonUtil.getClassifier(groupByType);
        //One cdr per day
        String iccId = String.valueOf(id);
        Map<String, List<EnrichedCDR>> timeWiseGroupedCDRs = enrichedCDRList.stream().filter(e -> e.getIccId().equals(iccId)).collect(
                        Collectors.groupingBy(identity));

        for (Map.Entry<String, List<EnrichedCDR>> groupByTypeMap : timeWiseGroupedCDRs.entrySet()) {
            resultSet.add(CommonUtil.aggregateResultSet(groupByTypeMap.getValue()));
        }
    }

    public Object getSimDataUsageResponse(String iccId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection, int startIndex, int count, String fromDate, String toDate, List<TestAccountCDRConfig> accountCDRConfigList) {
        parse(accountCDRConfigList);
        generateResultSet(iccId, groupByPeriod);
        List<DBResultSet> simResultSet = resultSet.stream().skip(startIndex).limit(count).collect(Collectors.toList());

        List<DataUsageResultSetBean> dataUsageResultSetList = null;

        int recordsCount = 0;
        String lastAvailableDate = null;
        AtomicLong rowCount = new AtomicLong();
        dataUsageResultSetList = DBResultToDaoResultConverter.convert(simResultSet,
                null, iccId, null, fromDate, toDate, rowCount);

        for (DataUsageResultSetBean dataUsageResultSet : dataUsageResultSetList) {
            CommonUtil.buildDataUsageList(dataUsageResultSet, simDataUsageList, groupByPeriod);
        }

        List<DataUsage> dataUsageList = simDataUsageList.getList();

        if(dataUsageList != null) {
            recordsCount = dataUsageList.size();
            Comparator<DataUsage> comparator = CommonUtil.getComparator(sortBy, sortByDirection);
            Collections.sort(simDataUsageList.getList(), comparator);
        }
        else{
            recordsCount = 0;
        }



        int listIndex = 0;
        if (sortByDirection == SORT_DIRECTION.ASC) {

            listIndex = dataUsageResultSetList.size() - 1;
        }


        if (recordsCount > 0) {
            lastAvailableDate = simDataUsageList.getList().get(listIndex).getToDate();
        }

        simDataUsageList.setGroupByPeriod(groupByPeriod.name());
        simDataUsageList.setTotalCount((int)rowCount.get());
        simDataUsageList.setCount(recordsCount);
        simDataUsageList.setLastAvailableDate(lastAvailableDate);

        return simDataUsageList;

    }

    public GetDataUsage getSimDataUsageList() {
        return simDataUsageList;
    }
}
