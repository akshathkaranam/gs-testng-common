package com.gigsky.tests.common.calldatarecords.datagenerator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by apandit on 07/29/18.
 */
public class SimData {

    Long accountId;
    Long subAccountId;
    String iccId;
    Long zoneId;
    String zoneName;
    Long userId;
    Long groupId;
    Long timeStamp;
    String timeZone;
    Long uplinkBytes = 0l;
    Long downlinkBytes = 0l;
    String countryCode;
    String countryName;
    String imsi;
    String msisdn;
    String imsiProfileName;
    String mcc, mnc;
    String carrierName;
    String cdrType;


    Long initialDelayMs = 0l;

    public SimData(CDRTestData testData) throws ParseException {
        String dateTime = testData.getDayKey().length() == 10 ? testData.getDayKey()+ " 00:00:00" : testData.getDayKey();

        Calendar timeInMS = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        timeInMS.setTime(sdf.parse(dateTime));
        Long recordStartTime = timeInMS.getTimeInMillis();

        this.setSubAccountId(testData.getSubAccountId());
        this.setIccId(testData.getIccId());
        this.setZoneId(testData.getZoneId());
        this.setZoneName(testData.getZoneName());
        this.setGroupId(testData.getGroupId());
        this.setUserId(testData.getUserId());
        this.setCountryCode(testData.getCountryCode());
        this.setCountryName(testData.getCountryName());
        this.setMcc(testData.getMcc());
        this.setMnc(testData.getMnc());
        this.setTimeStamp(recordStartTime);
        this.setUplinkBytes(testData.getUplinkBytes());
        this.setDownlinkBytes(testData.getDownlinkBytes());
        this.setInitialDelayMs(testData.getInitialDelayTimeMs());
        this.setImsi(testData.getImsi());
        this.setMsisdn(testData.getMsisdn());
        this.setImsiProfileName(testData.getImsiProfileName());
        this.setCarrierName(testData.getCarrierName());
        this.setCdrType(testData.getCdrType());
    }

    public SimData(LocationTestData testData) throws ParseException {
        String dateTime = testData.getDayKey().length() == 10 ? testData.getDayKey()+ " 00:00:00" : testData.getDayKey();

        Calendar timeInMS = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        timeInMS.setTime(sdf.parse(dateTime));
        Long recordStartTime = timeInMS.getTimeInMillis();

        this.setSubAccountId(testData.getSubAccountId());
        this.setIccId(testData.getIccId());
        this.setZoneId(testData.getZoneId());
        this.setZoneName(testData.getZoneName());
        this.setGroupId(testData.getGroupId());
        this.setUserId(testData.getUserId());
        this.setCountryCode(testData.getCountryCode());
        this.setCountryName(testData.getCountryName());
        this.setMcc(testData.getMcc());
        this.setMnc(testData.getMnc());
        this.setTimeStamp(recordStartTime);
        this.setInitialDelayMs(testData.getInitialDelayTimeMs());
        this.setImsi(testData.getImsi());
        this.setMsisdn(testData.getMsisdn());
        this.setImsiProfileName(testData.getImsiProfileName());
        this.setCarrierName(testData.getCarrierName());
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String code) {
        this.countryCode = code;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getMnc() {
        return mnc;
    }

    public void setMnc(String mnc) {
        this.mnc = mnc;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getUplinkBytes() {
        return uplinkBytes;
    }

    public void setUplinkBytes(Long uplinkBytes) {
        this.uplinkBytes = uplinkBytes;
    }

    public Long getDownlinkBytes() {
        return downlinkBytes;
    }

    public void setDownlinkBytes(Long downlinkBytes) {
        this.downlinkBytes = downlinkBytes;
    }

    public Long getSubAccountId() {
        return subAccountId;
    }

    public void setSubAccountId(Long subAccountId) {
        this.subAccountId = subAccountId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getZoneId() {
        return zoneId;
    }

    public void setZoneId(Long zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getImsiProfileName() {
        return imsiProfileName;
    }

    public void setImsiProfileName(String imsiProfileName) {
        this.imsiProfileName = imsiProfileName;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Long getInitialDelayMs() {
        return initialDelayMs;
    }

    public void setInitialDelayMs(Long initialDelayMs) {
        this.initialDelayMs = initialDelayMs;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getCdrType() {
        return cdrType;
    }

    public void setCdrType(String cdrType) {
        this.cdrType = cdrType;
    }
}
