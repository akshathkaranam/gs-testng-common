package com.gigsky.tests.common.calldatarecords.impl;

import com.gigsky.tests.common.calldatarecords.datagenerator.CDRBuilder;
import com.gigsky.tests.common.calldatarecords.datagenerator.CommonUtil;
import com.gigsky.tests.common.calldatarecords.datagenerator.DBResultToDaoResultConverter;
import com.gigsky.tests.common.calldatarecords.datagenerator.TestAccountCDRConfig;
import com.gigsky.tests.common.calldatarecords.datagenerator.datatypes.*;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.GROUP_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_BY;
import com.gigsky.tests.common.calldatarecords.datagenerator.enums.SORT_DIRECTION;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by apandit on 9/12/19.
 */
public class AccountSimsResponseBuilder extends CDRBuilder {
    private List<DBResultSet> resultSet = new ArrayList<>();

    @Override
    public void generateResultSet(Object id, GROUP_BY groupByType) {

        Function<EnrichedCDR, String> identity = CommonUtil.getClassifier(groupByType);

        Long accountId = Long.parseLong(String.valueOf(id));
        //One cdr per day
        Map<String, Map<String, List<EnrichedCDR>>> simWiseGroupedCDRs = enrichedCDRList.stream().filter(e -> e.getEnterpriseAccountId().equals(accountId)).collect(
                Collectors.groupingBy(e -> e.getIccId(),
                        Collectors.groupingBy(identity)));

        for (Map.Entry<String, Map<String, List<EnrichedCDR>>> simMap : simWiseGroupedCDRs.entrySet()) {
            for (Map.Entry<String, List<EnrichedCDR>> groupByTypeMap : simMap.getValue().entrySet()) {
                DBResultSet dbResultSet = CommonUtil.aggregateResultSet(groupByTypeMap.getValue());
                dbResultSet.setIccId(simMap.getKey());
                resultSet.add(dbResultSet);
            }
        }

    }

    @Override
    public AccountDataUsage getAccountDataUsageDistributedBySims(Long accountId, GROUP_BY groupByPeriod, SORT_BY sortBy, SORT_DIRECTION sortByDirection,
                                                                 int startIndex, int count, String fromDate, String toDate,
                                                                 List<TestAccountCDRConfig> accountCDRConfigList,
                                                                 GetEnterpriseAccountDetails account, GetUserListOfEntAccount userDetails) {

        parse(accountCDRConfigList);
        generateResultSet(accountId, groupByPeriod);
        List<DBResultSet> iccIdResultSet = resultSet.stream().skip(startIndex).limit(count).collect(Collectors.toList());
        //Add user and account details to each result set
        CommonUtil.addAccountAndUserDetails(iccIdResultSet, account, userDetails);

        List<DataUsageResultSetBean> dataUsageResultSetList = null;
        AccountDataUsage accountDataUsageBySims = null;
//        int recordsCount = 0;
//        String lastAvailableDate = null;
        AtomicLong rowCount = new AtomicLong();
        dataUsageResultSetList = DBResultToDaoResultConverter.convert(iccIdResultSet,
                accountId, null, null, fromDate, toDate, rowCount);

        accountDataUsageBySims = buildAccountDataUsageBySims(groupByPeriod, rowCount.get(), dataUsageResultSetList, sortByDirection);

        return accountDataUsageBySims;
    }


    private AccountDataUsage buildAccountDataUsageBySims(GROUP_BY grpByPeriod, long totalRecordCount,
                                                         List<DataUsageResultSetBean> dataUsageResultSetBeanList, SORT_DIRECTION sortDirection) {
        AccountDataUsage accountDataUsageBySims = new AccountDataUsage();
        //<<simIccId,accountName>,datausage>
        Map<Map.Entry<String, String>, DataUsageList> dataUsageListForAllSims =
                new LinkedHashMap<Map.Entry<String, String>, DataUsageList>();
        AtomicLong totalDataUsedInBytesOfAllSims = new AtomicLong(0);
//        BigDecimal totalDataUsageCost = new BigDecimal(0.0);

        List<DataUsageList> dataUsageListOfSims = accountDataUsageBySims.getList();
        if (dataUsageListOfSims == null) {
            dataUsageListOfSims = new ArrayList<DataUsageList>();
        }
        if (CollectionUtils.isNotEmpty(dataUsageResultSetBeanList)) {
            //For every record that the query returned
            for (DataUsageResultSetBean dataUsageResultSetBean : dataUsageResultSetBeanList) {

                String simIccId = dataUsageResultSetBean.getIccId();
                String accountName = dataUsageResultSetBean.getEnt_account_name();
                Map.Entry<String, String> simDataUsageKey =
                        new AbstractMap.SimpleEntry<String, String>(simIccId, accountName);

                //Check if the map already contains entry for this user
                //If not create one
                if (!dataUsageListForAllSims.containsKey(simDataUsageKey)) {
                    dataUsageListForAllSims.put(simDataUsageKey, new DataUsageList());
                }

                //Get the entry for this user
                DataUsageList dataUsageListOfSim = dataUsageListForAllSims.get(simDataUsageKey);

                CommonUtil.buildDataUsageList(dataUsageResultSetBean, dataUsageListOfSim, grpByPeriod);

                //Set other user parameters
                dataUsageListOfSim.setSimIccid(dataUsageResultSetBean.getIccId());
                dataUsageListOfSim.setUserFirstName(dataUsageResultSetBean.getUser_account_first_name());
                dataUsageListOfSim.setUserLastName(dataUsageResultSetBean.getUser_account_last_name());
                dataUsageListOfSim.setAccountName(dataUsageResultSetBean.getEnt_account_name());

            }

            //Add all these user's data usage list to the main Data Usage report
            for (Map.Entry<Map.Entry<String, String>, DataUsageList> dataUsageListForSingleSim : dataUsageListForAllSims.entrySet()) {
                dataUsageListOfSims.add(dataUsageListForSingleSim.getValue());
                totalDataUsedInBytesOfAllSims.set(totalDataUsedInBytesOfAllSims.addAndGet(
                        dataUsageListForSingleSim.getValue().getTotalDataUsedInBytes().get()));
//                if (dataUsageListForSingleSim.getValue().getTotalDataUsageCost() != null) {
//                    totalDataUsageCost = totalDataUsageCost.add(dataUsageListForSingleSim.getValue().getTotalDataUsageCost());
//
//                }
            }

            Comparator<DataUsageList> comparator = CommonUtil.getDataUsageListComparator(sortDirection);
            Collections.sort(dataUsageListOfSims, comparator);
        }
        accountDataUsageBySims.setList(dataUsageListOfSims);
        int count = 0;
        if (CollectionUtils.isNotEmpty(accountDataUsageBySims.getList())) {
            count = accountDataUsageBySims.getList().size();
        }
        accountDataUsageBySims.setGroupByPeriod(grpByPeriod.name());
        // If the group by period is anything other than MONTH, then total row count returned by stored procedure will be wrong. So
        //re-write it using the actual number of records in the list.
        if ((grpByPeriod == GROUP_BY.WEEK) || (grpByPeriod == GROUP_BY.DAY)) {
            accountDataUsageBySims.setTotalCount((long) count);
        } else {
            accountDataUsageBySims.setTotalCount(totalRecordCount);
        }
        accountDataUsageBySims.setCount(count);
        accountDataUsageBySims.setTotalDataUsedInBytes(totalDataUsedInBytesOfAllSims);
//        accountDataUsageBySims.setTotalDataUsageCost(
//                totalDataUsageCost.setScale(NUMBER_OF_DECIMAL_PLACES, BigDecimal.ROUND_HALF_UP));
        return accountDataUsageBySims;
    }
}
