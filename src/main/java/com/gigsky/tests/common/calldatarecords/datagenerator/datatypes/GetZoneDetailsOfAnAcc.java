

package com.gigsky.tests.common.calldatarecords.datagenerator.datatypes;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "zoneId",
        "name",
        "nickName",
        "status",
        "lastUpdatedDate",
        "pricingModelVersion",
        "pricePerMB",
        "bucketPricingInfo",
        "zoneCountryList"
})
public class GetZoneDetailsOfAnAcc {

    @JsonProperty("type")
    private String type;
    @JsonProperty("zoneId")
    private Integer zoneId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("nickName")
    private String nickName;
    @JsonProperty("status")
    private String status;
    @JsonProperty("lastUpdatedDate")
    private String lastUpdatedDate;
    @JsonProperty("pricingModelVersion")
    private String pricingModelVersion;
    @JsonProperty("pricePerMB")
    private String pricePerMB;
    @JsonProperty("bucketPricingInfo")
    private BucketPricingInfo bucketPricingInfo;
    @JsonProperty("zoneCountryList")
    private List<ZoneCountryList> zoneCountryList = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("zoneId")
    public Integer getZoneId() {
        return zoneId;
    }

    @JsonProperty("zoneId")
    public void setZoneId(Integer zoneId) {
        this.zoneId = zoneId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("nickName")
    public String getNickName() {
        return nickName;
    }

    @JsonProperty("nickName")
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("lastUpdatedDate")
    public String getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    @JsonProperty("lastUpdatedDate")
    public void setLastUpdatedDate(String lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    @JsonProperty("pricingModelVersion")
    public String getPricingModelVersion() {
        return pricingModelVersion;
    }

    @JsonProperty("pricingModelVersion")
    public void setPricingModelVersion(String pricingModelVersion) {
        this.pricingModelVersion = pricingModelVersion;
    }

    @JsonProperty("pricePerMB")
    public String getPricePerMB() {
        return pricePerMB;
    }

    @JsonProperty("pricePerMB")
    public void setPricePerMB(String pricePerMB) {
        this.pricePerMB = pricePerMB;
    }

    @JsonProperty("bucketPricingInfo")
    public BucketPricingInfo getBucketPricingInfo() {
        return bucketPricingInfo;
    }

    @JsonProperty("bucketPricingInfo")
    public void setBucketPricingInfo(BucketPricingInfo bucketPricingInfo) {
        this.bucketPricingInfo = bucketPricingInfo;
    }

    @JsonProperty("zoneCountryList")
    public List<ZoneCountryList> getZoneCountryList() {
        return zoneCountryList;
    }

    @JsonProperty("zoneCountryList")
    public void setZoneCountryList(List<ZoneCountryList> zoneCountryList) {
        this.zoneCountryList = zoneCountryList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
