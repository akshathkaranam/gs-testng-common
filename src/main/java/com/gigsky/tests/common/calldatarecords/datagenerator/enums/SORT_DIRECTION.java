package com.gigsky.tests.common.calldatarecords.datagenerator.enums;

/**
 * Created by apandit on 7/31/19.
 */
public enum SORT_DIRECTION {
    ASC,
    DESC
}
