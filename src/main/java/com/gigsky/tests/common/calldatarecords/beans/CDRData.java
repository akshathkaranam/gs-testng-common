package com.gigsky.tests.common.calldatarecords.beans;

import com.gigsky.cdrmodule.interfaces.cdr.CGGPRSRecord;
import com.gigsky.cdrmodule.psm.decoder.PSMGPRSRecord;
import com.gigsky.tests.common.beans.Mccmnc;

import java.util.Calendar;

/**
 * Created by jeyarajs on 08/12/16.
 */
public class CDRData {

    private long imsi;
    private long msisdn;
    private Mccmnc mccmnc;
    private int uplinkVolume;
    private int downlinkVolume;
    private Calendar recordOpeningTime;
    private long recordDurationInSeconds;
    private CGGPRSRecord.CDRClosureReason cdrClosureReason = CGGPRSRecord.CDRClosureReason.timeLimit;
    private String accessPointName = "gigsky";
    private String nodeId = "psm";
    private long imei;
    private PSMGPRSRecord.ServiceVolume.TrafficType trafficType;

    public long getImsi() {
        return imsi;
    }

    public void setImsi(long imsi) {
        this.imsi = imsi;
    }

    public long getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(long msisdn) {
        this.msisdn = msisdn;
    }

    public Mccmnc getMccmnc() {
        return mccmnc;
    }

    public void setMccmnc(Mccmnc mccmnc) {
        this.mccmnc = mccmnc;
    }

    public int getUplinkVolume() {
        return uplinkVolume;
    }

    public void setUplinkVolume(int uplinkVolume) {
        this.uplinkVolume = uplinkVolume;
    }

    public int getDownlinkVolume() {
        return downlinkVolume;
    }

    public void setDownlinkVolume(int downlinkVolume) {
        this.downlinkVolume = downlinkVolume;
    }

    public Calendar getRecordOpeningTime() {
        return recordOpeningTime;
    }

    public void setRecordOpeningTime(Calendar recordOpeningTime) {
        this.recordOpeningTime = recordOpeningTime;
    }

    public long getRecordDurationInSeconds() {
        return recordDurationInSeconds;
    }

    public void setRecordDurationInSeconds(long recordDurationInSeconds) {
        this.recordDurationInSeconds = recordDurationInSeconds;
    }

    public CGGPRSRecord.CDRClosureReason getCdrClosureReason() {
        return cdrClosureReason;
    }

    public void setCdrClosureReason(CGGPRSRecord.CDRClosureReason cdrClosureReason) {
        this.cdrClosureReason = cdrClosureReason;
    }

    public String getAccessPointName() {
        return accessPointName;
    }

    public void setAccessPointName(String accessPointName) {
        this.accessPointName = accessPointName;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public long getImei() {
        return imei;
    }

    public void setImei(long imei) {
        this.imei = imei;
    }

    public PSMGPRSRecord.ServiceVolume.TrafficType getTrafficType() {
        return trafficType;
    }

    public void setTrafficType(PSMGPRSRecord.ServiceVolume.TrafficType trafficType) {
        this.trafficType = trafficType;
    }



    public void setCDRData(long imsi,long msisdn,Mccmnc mccmnc,PSMGPRSRecord.ServiceVolume.TrafficType trafficType,int uplinkVolume,int downlinkVolume,Calendar recordOpeningTime){

        this.setImsi(imsi);
        this.setMsisdn(msisdn);
        this.setImei(8000l);
        this.setMccmnc(mccmnc);
        this.setTrafficType(trafficType);
        this.setUplinkVolume(uplinkVolume);
        this.setDownlinkVolume(downlinkVolume);
        this.setRecordOpeningTime(recordOpeningTime);


    }
}
