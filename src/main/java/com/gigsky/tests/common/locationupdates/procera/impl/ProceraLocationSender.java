package com.gigsky.tests.common.locationupdates.procera.impl;

import com.gigsky.tests.common.beans.Mccmnc;
import com.gigsky.tests.common.locationupdates.common.beans.LocationEvent;
import com.gigsky.tests.common.locationupdates.common.beans.LocationInfo;
import com.gigsky.tests.common.locationupdates.common.interfaces.LocationSender;
import com.gigsky.tests.common.locationupdates.procera.beans.*;
import com.gigsky.tests.common.utils.JsonUtils;
import com.gigsky.tests.common.utils.SocketUtils;
import org.apache.commons.lang.StringEscapeUtils;

import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by jeyarajs on 10/12/16.
 */
public class ProceraLocationSender implements LocationSender {

    public static final ProceraLocationSender instance = new ProceraLocationSender();

    private ProceraLocationSender() {

    }

    public static ProceraLocationSender getInstance() {
        return instance;
    }

    @Override
    public void sendLocations(LocationEvent locationEvent) throws Exception {
        List<ProceraLocation> proceraLocationList = getProceraLocationList(locationEvent.getLocationInfoList());
        Object serverConfig = locationEvent.getServerConfig();
        if(serverConfig instanceof RpcServerConfig) {
            RpcServerConfig rpcServerConfig = (RpcServerConfig) serverConfig;
            sendLocationUpdates(JsonUtils.writeValueAsString(proceraLocationList), rpcServerConfig);
        }
    }

    private List<ProceraLocation> getProceraLocationList(List<LocationInfo> locationInfoList) {
        List<ProceraLocation> proceraLocationList = new LinkedList<ProceraLocation>();
        for(LocationInfo locationInfo : locationInfoList) {
            if(locationInfo instanceof ProceraLocationInfo) {
                ProceraLocationInfo proceraLocationInfo = (ProceraLocationInfo) locationInfo;
                proceraLocationList.add(getProceraLocationInfo(proceraLocationInfo));
            } else {
                System.out.println("Ignoring location info " + locationInfo);
            }
        }
        return proceraLocationList;
    }

    private void sendLocationUpdates(String message,
                                     RpcServerConfig rpcServerConfig) throws Exception {
        message = StringEscapeUtils.escapeXml(message);
        String sentence = String.format("{\"id\":1, \"method\":\"com.gigsky.test.notify\", \"params\":[\"%s\"]}", message);
        String host = rpcServerConfig.getHost();
        int port = rpcServerConfig.getPort();
        Socket clientSocket = null;
        try {
            clientSocket = SocketUtils.connect(host, port);
            SocketUtils.writeData(clientSocket, sentence);
            BaseResponse response = JsonUtils.readValue(SocketUtils.readData(clientSocket), BaseResponse.class);
            BaseResponse.Error error = response.getError();
            if (error != null) {
                System.out.println("{\"result\": \"failure\" , \"errorMsg\":" + " \"" + error.getMessage() + "\"}");
            } else {
                System.out.println("{\"result\": \"success\"}");
            }
        } finally {
            SocketUtils.closeConnection(clientSocket);
        }
    }

    private ProceraLocation getProceraLocationInfo(ProceraLocationInfo proceraLocationInfo) {
        ProceraLocation proceraLocation = new ProceraLocation();
        proceraLocation.setMethod("object.created");
        proceraLocation.setParams(getParams(proceraLocationInfo));
        return proceraLocation;
    }

    private Params getParams(ProceraLocationInfo proceraLocationInfo) {
        Params params = new Params();
        params.setType("session");
        params.setLocationData(getLocationInfo(proceraLocationInfo));
        return params;
    }

    private LocationData getLocationInfo(ProceraLocationInfo proceraLocationInfo) {

        proceraLocationInfo.setCreationTime(Calendar.getInstance().getTime().toString());
        proceraLocationInfo.setUpdateTime(Calendar.getInstance().getTime().toString());
        LocationData locationData = new LocationData();
        locationData.setAuxiliarySubscriberId(proceraLocationInfo.getMsisdn());
        locationData.setImsi(proceraLocationInfo.getImsi());
        locationData.setMccmnc(getProceraMccmnc(proceraLocationInfo.getMccmnc()));
        if(proceraLocationInfo.getSessionId()!=null){
        locationData.setSessionId(proceraLocationInfo.getSessionId());
        }
        else{
            locationData.setSessionId(null);
        }
        locationData.setCreationTime(getTimeInUTC(proceraLocationInfo.getCreationTime()));
        locationData.setUpdateTime(getTimeInUTC(proceraLocationInfo.getUpdateTime()));
        return locationData;
    }

    private ProceraMccmnc getProceraMccmnc(Mccmnc mccmnc) {
        ProceraMccmnc proceraMccmnc = new ProceraMccmnc();
        proceraMccmnc.setMcc(mccmnc.getMcc());
        proceraMccmnc.setMnc(mccmnc.getMnc());
        return proceraMccmnc;
    }

    private String getTimeInUTC(String time) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            cal.setTime(sdf.parse(time));
        }catch (Exception e) {
            System.out.println("Time parsing Exception:"+e);
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(cal.getTime());
    }
}
