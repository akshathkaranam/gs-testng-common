package com.gigsky.tests.common.locationupdates.huaweiCSLocation.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by jeyarajs on 12/12/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "trigger", propOrder = {
        "object"
})
public class TriggerCSLocation {
    @XmlElement(required = true, nillable = true)
    private ObjectTypeCSLocation object;

    public ObjectTypeCSLocation getObject() {
        return object;
    }

    public void setObject(ObjectTypeCSLocation object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "ClassPojo [object = "+object+"]";
    }
}
