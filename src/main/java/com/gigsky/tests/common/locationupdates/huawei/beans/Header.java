package com.gigsky.tests.common.locationupdates.huawei.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by jeyarajs on 12/12/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Header", propOrder = {
        "Trans"
})
public class Header {
    @XmlElement(required = true, nillable = true)
    private Trans Trans;

    public Trans getTrans () {
        return Trans;
    }

    public void setTrans (Trans Trans) {
        this.Trans = Trans;
    }

    @Override
    public String toString() {
        return "ClassPojo [Trans = "+Trans+"]";
    }
}
