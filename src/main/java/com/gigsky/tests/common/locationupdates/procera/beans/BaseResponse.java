package com.gigsky.tests.common.locationupdates.procera.beans;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by jeyarajs on 12/12/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseResponse {
    private long id = -1;
    private Error error;
    private Object result;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Error {
        private long code;
        private String message;

        public long getCode() {
            return code;
        }

        public void setCode(long code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
