package com.gigsky.tests.common.locationupdates.tdc.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

/**
 * Created by jeyarajs on 09/12/16.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class RoamingChangeNotification {

    private List<Roaming> roaming;

    @JsonProperty("roaming")
    public List<Roaming> getRoaming() {
        return roaming;
    }

    @JsonProperty("roaming")
    public void setRoaming(List<Roaming> roaming) {
        this.roaming = roaming;
    }
}
