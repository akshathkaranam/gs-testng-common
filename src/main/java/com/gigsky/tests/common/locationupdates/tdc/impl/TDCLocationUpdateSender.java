package com.gigsky.tests.common.locationupdates.tdc.impl;

import com.gigsky.tests.common.beans.Mccmnc;
import com.gigsky.tests.common.locationupdates.common.beans.LocationEvent;
import com.gigsky.tests.common.locationupdates.common.beans.LocationInfo;
import com.gigsky.tests.common.locationupdates.common.interfaces.LocationSender;
import com.gigsky.tests.common.locationupdates.tdc.beans.*;
import com.gigsky.tests.common.utils.JsonUtils;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static io.restassured.RestAssured.config;
import static io.restassured.config.RedirectConfig.redirectConfig;

/**
 * Created by jeyarajs on 09/12/16.
 */
public class TDCLocationUpdateSender implements LocationSender {

    public static final TDCLocationUpdateSender instance = new TDCLocationUpdateSender();

    private TDCLocationUpdateSender() {

    }

    public static TDCLocationUpdateSender getInstance() {
        return instance;
    }

    @Override
    public void sendLocations(LocationEvent locationEvent) throws Exception {
        Object serverConfig = locationEvent.getServerConfig();
        if(serverConfig instanceof TDCLocationServerConfig) {
            TDCLocationServerConfig tdcLocationServerConfig = (TDCLocationServerConfig) serverConfig;
            sendLocationUpdate(getLocationEventSchema(locationEvent.getLocationInfoList()), tdcLocationServerConfig);
        }
    }

    private LocationEventSchema getLocationEventSchema(List<LocationInfo> locationInfoList) {
        LocationEventSchema locationEventSchema = new LocationEventSchema();
        locationEventSchema.setRoamingChangeNotification(getRoamingChangeNotification(locationInfoList));
        return locationEventSchema;
    }

    private RoamingChangeNotification getRoamingChangeNotification(List<LocationInfo> locationInfoList) {
        RoamingChangeNotification roamingChangeNotification = new RoamingChangeNotification();
        roamingChangeNotification.setRoaming(getRoamingList(locationInfoList));
        return roamingChangeNotification;
    }

    private List<Roaming> getRoamingList(List<LocationInfo> locationInfoList) {
        List<Roaming> roamingList = new LinkedList<Roaming>();
        for(LocationInfo locationInfo : locationInfoList) {
            if(locationInfo instanceof TDCLocationInfo) {
                TDCLocationInfo tdcLocationInfo = (TDCLocationInfo) locationInfo;
                roamingList.add(getRoaming(tdcLocationInfo));
            } else {
                System.out.println("Ignoring location info " + locationInfo);
            }
        }
        return roamingList;
    }

    private Roaming getRoaming(TDCLocationInfo tdcLocationInfo) {
        Roaming roaming = new Roaming();
        roaming.setAddress("tel:+" + tdcLocationInfo.getMsisdn());
        roaming.setCurrentRoaming(tdcLocationInfo.getCurrentRoaming());
        roaming.setRetrievalStatus(tdcLocationInfo.getRetrievalStatus());
        roaming.setRetrievalTime(tdcLocationInfo.getRetrievalTime());
        roaming.setServingMccMnc(getServingMccMnc(tdcLocationInfo.getMccmnc()));
        roaming.setServingNode(getServingNode(tdcLocationInfo));
        roaming.setSubscriberId(String.valueOf(tdcLocationInfo.getImsi()));
        return roaming;
    }

    private ServingMccMnc getServingMccMnc(Mccmnc mccmnc) {
        ServingMccMnc servingMccMnc = new ServingMccMnc();
        servingMccMnc.setMcc(String.valueOf(mccmnc.getMcc()));
        servingMccMnc.setMnc(String.valueOf(mccmnc.getMnc()));
        return servingMccMnc;
    }

    private ServingNode getServingNode(TDCLocationInfo tdcLocationInfo) {
        ServingNode servingNode = new ServingNode();
        servingNode.setNode(tdcLocationInfo.getServingNode());
        servingNode.setType(tdcLocationInfo.getServingNodeType());
        return servingNode;
    }

    private void sendLocationUpdate(LocationEventSchema locationEventSchema,
                                    TDCLocationServerConfig tdcLocationServerConfig) throws IOException {
        String tdcLocationUpdateUrl = tdcLocationServerConfig.getImsiServerBaseUrl() + "/tdc/terminalstatus/version1/notifications/RoamingChangeNotification";
        RequestSpecification requestSpecification = RestAssured.given().log().all();
        requestSpecification.config(config().redirect(redirectConfig().followRedirects(false)));
        requestSpecification.header("Content-Type", "application/json");
        requestSpecification.body(JsonUtils.writeValueAsString(locationEventSchema));
        Response response = requestSpecification.post(tdcLocationUpdateUrl);
        Assert.assertEquals(200, response.statusCode());
    }

}
