package com.gigsky.tests.common.locationupdates.tdc.beans;

/**
 * Created by jeyarajs on 12/12/16.
 */
public class TDCLocationServerConfig {

    public TDCLocationServerConfig(String imsiServerBaseUrl) {
        this.imsiServerBaseUrl = imsiServerBaseUrl;
    }

    private String imsiServerBaseUrl;

    public String getImsiServerBaseUrl() {
        return imsiServerBaseUrl;
    }

    public void setImsiServerBaseUrl(String imsiServerBaseUrl) {
        this.imsiServerBaseUrl = imsiServerBaseUrl;
    }
}
