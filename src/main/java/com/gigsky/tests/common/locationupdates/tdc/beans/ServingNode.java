package com.gigsky.tests.common.locationupdates.tdc.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by jeyarajs on 09/12/16.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ServingNode {

    private String type;
    private String node;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("node")
    public String getNode() {
        return node;
    }

    @JsonProperty("node")
    public void setNode(String node) {
        this.node = node;
    }
}
