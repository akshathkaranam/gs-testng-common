package com.gigsky.tests.common.locationupdates.huaweiCSLocation.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by jeyarajs on 12/12/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Header", propOrder = {
        "Trans"
})
public class HeaderCSLocation {
    @XmlElement(required = true, nillable = true)
    private TransCSLocation Trans;

    public TransCSLocation getTrans () {
        return Trans;
    }

    public void setTrans (TransCSLocation Trans) {
        this.Trans = Trans;
    }

    @Override
    public String toString() {
        return "ClassPojo [Trans = "+Trans+"]";
    }
}
