package com.gigsky.tests.common.locationupdates.tdc.beans;

import com.gigsky.tests.common.locationupdates.common.beans.LocationInfo;

/**
 * Created by jeyarajs on 12/12/16.
 */
public class TDCLocationInfo extends LocationInfo {

    private String currentRoaming = "NotRoaming";
    private String retrievalStatus = "Retrieved";
    private String retrievalTime;
    private String servingNode = "+4540150021";
    private String servingNodeType = "VLR";

    public String getCurrentRoaming() {
        return currentRoaming;
    }

    public void setCurrentRoaming(String currentRoaming) {
        this.currentRoaming = currentRoaming;
    }

    public String getRetrievalStatus() {
        return retrievalStatus;
    }

    public void setRetrievalStatus(String retrievalStatus) {
        this.retrievalStatus = retrievalStatus;
    }

    public String getRetrievalTime() {
        return retrievalTime;
    }

    public void setRetrievalTime(String retrievalTime) {
        this.retrievalTime = retrievalTime;
    }

    public String getServingNode() {
        return servingNode;
    }

    public void setServingNode(String servingNode) {
        this.servingNode = servingNode;
    }

    public String getServingNodeType() {
        return servingNodeType;
    }

    public void setServingNodeType(String servingNodeType) {
        this.servingNodeType = servingNodeType;
    }
}
