package com.gigsky.tests.common.locationupdates.huawei.beans;

/**
 * Created by jeyarajs on 12/12/16.
 */
public class HuaweiLocationServerConfig {
    public HuaweiLocationServerConfig(String imsiServerBaseUrl) {
        this.imsiServerBaseUrl = imsiServerBaseUrl;
    }

    private String imsiServerBaseUrl;

    public String getImsiServerBaseUrl() {
        return imsiServerBaseUrl;
    }

    public void setImsiServerBaseUrl(String imsiServerBaseUrl) {
        this.imsiServerBaseUrl = imsiServerBaseUrl;
    }
}
