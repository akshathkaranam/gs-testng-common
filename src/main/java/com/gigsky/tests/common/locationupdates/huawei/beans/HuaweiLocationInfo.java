package com.gigsky.tests.common.locationupdates.huawei.beans;

import com.gigsky.tests.common.locationupdates.common.beans.LocationInfo;

/**
 * Created by jeyarajs on 12/12/16.
 */
public class HuaweiLocationInfo extends LocationInfo {

    private String serviceName = "broadcast";
    private String messageId = "14";
    private String connectionId = "0001";
    private String commandName = "LTEInfoReport";
    private String imei;
    private String imeiMatch;
    private String lteICBR = "1";
    private String lteStatus = "1";
    private String mmeNewPLMN = "461456";
    private String mmeOldHostName = "l00297380saehss1.huawei.com";
    private String mmeOldPLMN = "460456";

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getImeiMatch() {
        return imeiMatch;
    }

    public void setImeiMatch(String imeiMatch) {
        this.imeiMatch = imeiMatch;
    }

    public String getLteICBR() {
        return lteICBR;
    }

    public void setLteICBR(String lteICBR) {
        this.lteICBR = lteICBR;
    }

    public String getLteStatus() {
        return lteStatus;
    }

    public void setLteStatus(String lteStatus) {
        this.lteStatus = lteStatus;
    }

    public String getMmeNewPLMN() {
        return mmeNewPLMN;
    }

    public void setMmeNewPLMN(String mmeNewPLMN) {
        this.mmeNewPLMN = mmeNewPLMN;
    }

    public String getMmeOldHostName() {
        return mmeOldHostName;
    }

    public void setMmeOldHostName(String mmeOldHostName) {
        this.mmeOldHostName = mmeOldHostName;
    }

    public String getMmeOldPLMN() {
        return mmeOldPLMN;
    }

    public void setMmeOldPLMN(String mmeOldPLMN) {
        this.mmeOldPLMN = mmeOldPLMN;
    }
}
