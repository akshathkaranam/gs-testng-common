package com.gigsky.tests.common.locationupdates.procera.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by jeyarajs on 09/12/16.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Params {
    private String type;
    private List<Object> tags = new LinkedList<Object>();
    private LocationData locationData;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("tags")
    public List<Object> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    @JsonProperty("object")
    public LocationData getLocationData() {
        return locationData;
    }

    @JsonProperty("object")
    public void setLocationData(LocationData locationData) {
        this.locationData = locationData;
    }
}
