package com.gigsky.tests.common.locationupdates.huawei.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by jeyarajs on 12/12/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "trigger", propOrder = {
        "object",
        "objectType"
})
public class Trigger {
    @XmlElement(required = true, nillable = true)
    private Object object;
    private ObjectType objectType;

    public Object getObject () {
        return object;
    }

    public void setObject (Object object) {
        this.object = object;
    }

    public ObjectType getObjectType() {
        return objectType;
    }

    public void setObjectType(ObjectType objectType) {
        this.objectType = objectType;
    }

    @Override
    public String toString() {
        return "ClassPojo [object = "+object+"]";
    }
}
