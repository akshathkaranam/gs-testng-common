package com.gigsky.tests.common.locationupdates.common.interfaces;

import com.gigsky.tests.common.locationupdates.common.beans.LocationEvent;

/**
 * Created by jeyarajs on 12/12/16.
 */
public interface LocationSender {

    public void sendLocations(LocationEvent locationEvent) throws Exception;
}
