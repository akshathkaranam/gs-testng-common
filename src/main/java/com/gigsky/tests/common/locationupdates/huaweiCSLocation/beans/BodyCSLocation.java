package com.gigsky.tests.common.locationupdates.huaweiCSLocation.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by jeyarajs on 12/12/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Body", propOrder = {
        "trigger"
})
public class BodyCSLocation {
    @XmlElement(required = true, nillable = true)
    private TriggerCSLocation trigger;

    public TriggerCSLocation getTrigger () {
        return trigger;
    }

    public void setTrigger (TriggerCSLocation trigger) {
        this.trigger = trigger;
    }

    @Override
    public String toString() {
        return "ClassPojo [trigger = "+trigger+"]";
    }
}
