package com.gigsky.tests.common.locationupdates.huawei.impl;

import com.gigsky.tests.common.beans.Mccmnc;
import com.gigsky.tests.common.locationupdates.common.beans.LocationEvent;
import com.gigsky.tests.common.locationupdates.common.beans.LocationInfo;
import com.gigsky.tests.common.locationupdates.common.interfaces.LocationSender;
import com.gigsky.tests.common.locationupdates.huawei.beans.*;
import com.gigsky.tests.common.locationupdates.huawei.beans.Object;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import static io.restassured.RestAssured.config;
import static io.restassured.config.RedirectConfig.redirectConfig;

/**
 * Created by jeyarajs on 12/12/16.
 */
public class HuaweiHLRLocationSender implements LocationSender {

    private static final HuaweiHLRLocationSender instance = new HuaweiHLRLocationSender();

    private HuaweiHLRLocationSender() {

    }

    public static HuaweiHLRLocationSender getInstance() {
        return instance;
    }

    @Override
    public void sendLocations(LocationEvent locationEvent) throws Exception {
        java.lang.Object serverConfig = locationEvent.getServerConfig();
        if(serverConfig instanceof HuaweiLocationServerConfig) {
            HuaweiLocationServerConfig huaweiLocationServerConfig = (HuaweiLocationServerConfig) serverConfig;
            for (LocationInfo locationInfo : locationEvent.getLocationInfoList()) {
                if (locationInfo instanceof HuaweiLocationInfo) {
                    HuaweiLocationInfo huaweiLocationInfo = (HuaweiLocationInfo) locationInfo;
                    sendLocationUpdate(getEnvelope(huaweiLocationInfo), huaweiLocationServerConfig);
                } else {
                    System.out.println("Ignoring location info " + locationInfo);
                }
            }
        }
    }

    private Envelope getEnvelope(HuaweiLocationInfo huaweiLocationInfo) {
        Envelope envelope = new Envelope();
        envelope.setHeader(getHeader(huaweiLocationInfo));
        envelope.setBody(getBody(huaweiLocationInfo));
        return envelope;
    }

    private Header getHeader(HuaweiLocationInfo huaweiLocationInfo) {
        Header header = new Header();
        header.setTrans(getTrans(huaweiLocationInfo));
        return header;
    }

    private Trans getTrans(HuaweiLocationInfo huaweiLocationInfo) {
        Trans trans = new Trans();
        trans.setServiceName(huaweiLocationInfo.getServiceName());
        trans.setMsgId(huaweiLocationInfo.getMessageId());
        trans.setConnId(huaweiLocationInfo.getConnectionId());
        return trans;
    }

    private Body getBody(HuaweiLocationInfo huaweiLocationInfo) {
        Body body = new Body();
        body.setTrigger(getTrigger(huaweiLocationInfo));
        return body;
    }

    private Trigger getTrigger(HuaweiLocationInfo huaweiLocationInfo) {
        Trigger trigger = new Trigger();
        trigger.setObject(getObject(huaweiLocationInfo));
        return trigger;
    }

    private Object getObject(HuaweiLocationInfo huaweiLocationInfo) {
        Object object = new Object();
        object.setCmdName(huaweiLocationInfo.getCommandName());
        Long imsi = huaweiLocationInfo.getImsi();
        if(imsi != null) {
            object.setIMSI(String.valueOf(imsi));
        }
        object.setLTEICBR(huaweiLocationInfo.getLteICBR());
        object.setLTEStatus(huaweiLocationInfo.getLteStatus());
        Mccmnc mccmnc = huaweiLocationInfo.getMccmnc();
        if(mccmnc != null) {
            object.setMMENewHostName("mme01.epc.mnc" + mccmnc.getMnc() + ".mcc" + mccmnc.getMcc() + ".3gppnetwork.org");
        }
        object.setMMENewPLMN(huaweiLocationInfo.getMmeNewPLMN());
        object.setMMEOldHostName(huaweiLocationInfo.getMmeOldHostName());
        object.setMMEOldPLMN(huaweiLocationInfo.getMmeOldPLMN());
        return object;
    }

    private void sendLocationUpdate(Envelope envelope,
                                    HuaweiLocationServerConfig huaweiLocationServerConfig) {
        String huaweiLocationUpdateUrl = huaweiLocationServerConfig.getImsiServerBaseUrl() + "/hlrlocation";
        RequestSpecification requestSpecification = RestAssured.given().log().all();
        requestSpecification.config(config().redirect(redirectConfig().followRedirects(false)));
        requestSpecification.header("Content-Type", "text/xml");
        requestSpecification.body(envelope);
        Response response = requestSpecification.post(huaweiLocationUpdateUrl);
        Assert.assertEquals(200, response.statusCode());
    }
}
