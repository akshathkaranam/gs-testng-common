package com.gigsky.tests.common.locationupdates.huaweiCSLocation.impl;

import com.gigsky.tests.common.locationupdates.common.beans.LocationEvent;
import com.gigsky.tests.common.locationupdates.common.beans.LocationInfo;
import com.gigsky.tests.common.locationupdates.common.interfaces.LocationSender;
import com.gigsky.tests.common.locationupdates.huawei.beans.HuaweiLocationServerConfig;
import com.gigsky.tests.common.locationupdates.huaweiCSLocation.beans.*;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.config;
import static io.restassured.config.RedirectConfig.redirectConfig;

/**
 * Created by Yuvasrikushal on 09/11/17.
 */
public class HuaweiCsLocationSender implements LocationSender {

    private static final HuaweiCsLocationSender instance = new HuaweiCsLocationSender();

    private HuaweiCsLocationSender() {

    }

    public static HuaweiCsLocationSender getInstance() {
        return instance;
    }

    @Override
    public void sendLocations(LocationEvent locationEvent) throws Exception {
        java.lang.Object serverConfig = locationEvent.getServerConfig();
        if(serverConfig instanceof HuaweiLocationServerConfig) {
            HuaweiLocationServerConfig huaweiLocationServerConfig = (HuaweiLocationServerConfig) serverConfig;
            for (LocationInfo locationInfo : locationEvent.getLocationInfoList()) {
                if (locationInfo instanceof HuaweiCsLocationInfo) {
                    HuaweiCsLocationInfo huaweiCsLocationInfo = (HuaweiCsLocationInfo) locationInfo;
                    try {
                        sendLocationUpdate(getEnvelope(huaweiCsLocationInfo), huaweiLocationServerConfig);
                    } catch (AssertionError e) {
                        throw e;
                    }
                } else {
                    System.out.println("Ignoring location info " + locationInfo);
                }
            }
        }
    }

    private EnvelopeCSLocation getEnvelope(HuaweiCsLocationInfo huaweiCsLocationInfo) {
        EnvelopeCSLocation envelope = new EnvelopeCSLocation();
        envelope.setHeader(getHeader(huaweiCsLocationInfo));
        envelope.setBody(getBody(huaweiCsLocationInfo));
        return envelope;
    }

    private HeaderCSLocation getHeader(HuaweiCsLocationInfo huaweiCsLocationInfo) {
        HeaderCSLocation header = new HeaderCSLocation();
        header.setTrans(getTrans(huaweiCsLocationInfo));
        return header;
    }

    private TransCSLocation getTrans(HuaweiCsLocationInfo huaweiCsLocationInfo) {
        TransCSLocation trans = new TransCSLocation();
        trans.setServiceName(huaweiCsLocationInfo.getServiceName());
        trans.setMsgId(huaweiCsLocationInfo.getMessageId());
        trans.setConnId(huaweiCsLocationInfo.getConnectionId());
        return trans;
    }

    private BodyCSLocation getBody(HuaweiCsLocationInfo huaweiCsLocationInfo) {
        BodyCSLocation body = new BodyCSLocation();
        body.setTrigger(getTrigger(huaweiCsLocationInfo));
        return body;
    }

    private TriggerCSLocation getTrigger(HuaweiCsLocationInfo huaweiCsLocationInfo) {
        TriggerCSLocation trigger = new TriggerCSLocation();
        trigger.setObject(getObjectType(huaweiCsLocationInfo));
        return trigger;
    }

    private ObjectTypeCSLocation getObjectType(HuaweiCsLocationInfo huaweiCsLocationInfo) {
        ObjectTypeCSLocation objectType = new ObjectTypeCSLocation();
        List<AttributeCSLocation> attributeList = new ArrayList<>();
        AttributeCSLocation attribute = new AttributeCSLocation();

        if(huaweiCsLocationInfo.getImsi() != null) {
            attribute.setNme1("IMSI");
            attribute.setCurVal(String.valueOf(huaweiCsLocationInfo.getImsi()));
            attributeList.add(attribute);
        }

        if(huaweiCsLocationInfo.getMsisdn() != null) {
            attribute = new AttributeCSLocation();
            attribute.setNme1("MSISDN");
            attribute.setCurVal(String.valueOf(huaweiCsLocationInfo.getMsisdn()));
            attributeList.add(attribute);
        }

        if(huaweiCsLocationInfo.getCurrNum() != null) {
            attribute = new AttributeCSLocation();
            attribute.setNme1("CURRVLRNUM");
            attribute.setCurVal(huaweiCsLocationInfo.getCurrNum());
            attributeList.add(attribute);
        }

        if(huaweiCsLocationInfo.getImei() != null) {
            attribute = new AttributeCSLocation();
            attribute.setNme1("IMEI");
            attribute.setCurVal(huaweiCsLocationInfo.getImei());
            attributeList.add(attribute);
        }

        if(huaweiCsLocationInfo.getLu_type() != null) {
            attribute = new AttributeCSLocation();
            attribute.setNme1("LU_TYPE");
            attribute.setCurVal(huaweiCsLocationInfo.getLu_type());
            attributeList.add(attribute);
        }

        if(huaweiCsLocationInfo.getrTime() != null) {
            attribute = new AttributeCSLocation();
            attribute.setNme1("RTIME");
            attribute.setCurVal(String.valueOf(huaweiCsLocationInfo.getrTime()));
            attributeList.add(attribute);
        }

        objectType.setAttributes(attributeList);

        return objectType;
    }

    private void sendLocationUpdate(EnvelopeCSLocation envelope,
                                    HuaweiLocationServerConfig huaweiLocationServerConfig) {
        String huaweiLocationUpdateUrl = huaweiLocationServerConfig.getImsiServerBaseUrl() + "/csLocation";
        RequestSpecification requestSpecification = RestAssured.given().log().all();
        requestSpecification.config(config().redirect(redirectConfig().followRedirects(false)));
        requestSpecification.header("Content-Type", "text/xml");
        requestSpecification.body(envelope);
        Response response = requestSpecification.post(huaweiLocationUpdateUrl);
        Assert.assertEquals(200, response.statusCode());
    }
}
