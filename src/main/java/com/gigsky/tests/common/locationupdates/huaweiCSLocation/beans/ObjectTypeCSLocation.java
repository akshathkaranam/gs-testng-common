package com.gigsky.tests.common.locationupdates.huaweiCSLocation.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by vishaljogi on 31/08/17.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ObjectTypeCSLocation {
    @XmlAttribute(name = "DN")
    String DN;

    @XmlAttribute
    String operation;
    @XmlElement(name="attribute")
    List<AttributeCSLocation> attributes;
    public String getDN1() {
        return DN;
    }

    public void setDN1(String DN) {
        this.DN = DN;
    }

    public List<AttributeCSLocation> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<AttributeCSLocation> attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return "ObjectType{" +
                "attributes=" + attributes +
                ", DN='" + DN + '\'' +
                ", operation='" + operation + '\'' +
                '}';
    }
}
