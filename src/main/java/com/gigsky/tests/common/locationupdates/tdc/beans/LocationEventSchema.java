package com.gigsky.tests.common.locationupdates.tdc.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by jeyarajs on 09/12/16.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LocationEventSchema {
    private RoamingChangeNotification roamingChangeNotification;

    @JsonProperty("roamingChangeNotification ")
    public RoamingChangeNotification getRoamingChangeNotification() {
        return roamingChangeNotification;
    }

    @JsonProperty("roamingChangeNotification ")
    public void setRoamingChangeNotification(RoamingChangeNotification roamingChangeNotification) {
        this.roamingChangeNotification = roamingChangeNotification;
    }
}
