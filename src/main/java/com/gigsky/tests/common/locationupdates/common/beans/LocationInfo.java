package com.gigsky.tests.common.locationupdates.common.beans;

import com.gigsky.tests.common.beans.Mccmnc;

/**
 * Created by jeyarajs on 12/12/16.
 */
public class LocationInfo {

    private Long imsi;
    private Long msisdn;
    private Mccmnc mccmnc;

    public Long getImsi() {
        return imsi;
    }

    public void setImsi(Long imsi) {
        this.imsi = imsi;
    }

    public Long getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(Long msisdn) {
        this.msisdn = msisdn;
    }

    public Mccmnc getMccmnc() {
        return mccmnc;
    }

    public void setMccmnc(Mccmnc mccmnc) {
        this.mccmnc = mccmnc;
    }

    @Override
    public String toString() {
        return "LocationInfo{" +
                "imsi=" + imsi +
                ", msisdn=" + msisdn +
                ", mccmnc=" + mccmnc +
                '}';
    }
}
