package com.gigsky.tests.common.locationupdates.huaweiCSLocation.beans;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by vishaljogi on 31/08/17.
 */
public class AttributeCSLocation {


    String nme1;

    String mod;

    String curVal;

    public String getCurVal() {
        return curVal;
    }
    @XmlElement(name ="currentValue")
    public void setCurVal(String curVal) {
        this.curVal = curVal;
    }
    public String getMod() {
        return mod;
    }
    @XmlAttribute (name ="modification")
    public void setMod(String mod) {
        this.mod = mod;
    }
    public String getNme1() {
        return nme1;
    }
    @XmlAttribute(name = "name")
    public void setNme1(String nme1) {
        this.nme1 = nme1;
    }

    @Override
    public String toString() {
        return "Attribute{" +
                "currentValue='" + curVal + '\'' +
                ", name='" + nme1 + '\'' +
                ", modification='" + mod + '\'' +
                '}';
    }
}
