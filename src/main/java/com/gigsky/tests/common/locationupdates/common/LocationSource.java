package com.gigsky.tests.common.locationupdates.common;

/**
 * Created by jeyarajs on 12/12/16.
 */
public enum LocationSource {
    TDC,
    HUAWEI,
    PROCERA,
    HUAWEI_CS;
}
