package com.gigsky.tests.common.locationupdates.huaweiCSLocation.beans;

import javax.xml.bind.annotation.*;

/**
 * Created by jeyarajs on 12/12/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Envelope", namespace = "http://schemas.xmlsoap.org/soap/envelope/")
@XmlType(name = "Envelope", propOrder = {
        "Header",
        "Body"
})
public class EnvelopeCSLocation {
    @XmlElement(required = true, nillable = true, namespace = "http://schemas.xmlsoap.org/soap/envelope/")
    private BodyCSLocation Body;
    @XmlElement(required = true, nillable = true, namespace = "http://schemas.xmlsoap.org/soap/envelope/")
    private HeaderCSLocation Header;

    public BodyCSLocation getBody () {
        return Body;
    }

    public void setBody (BodyCSLocation Body) {
        this.Body = Body;
    }

    public HeaderCSLocation getHeader () {
        return Header;
    }

    public void setHeader (HeaderCSLocation Header) {
        this.Header = Header;
    }

    @Override
    public String toString() {
        return "ClassPojo [Body = "+Body+", Header = "+Header+"]";
    }
}
