package com.gigsky.tests.common.locationupdates.procera.beans;

import com.gigsky.tests.common.locationupdates.common.beans.LocationInfo;

/**
 * Created by jeyarajs on 09/12/16.
 */
public class ProceraLocationInfo extends LocationInfo {
    private String sessionId = "10.171.61.77";
    private String creationTime;
    private String updateTime;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
