package com.gigsky.tests.common.locationupdates.tdc.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by jeyarajs on 09/12/16.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Roaming {
    private String address;
    private String subscriberId;
    private String currentRoaming;
    private String retrievalStatus;
    private String retrievalTime;
    private ServingNode servingNode;
    private ServingMccMnc servingMccMnc;

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("subscriberId")
    public String getSubscriberId() {
        return subscriberId;
    }

    @JsonProperty("subscriberId")
    public void setSubscriberId(String subscriberId) {
        this.subscriberId = subscriberId;
    }

    @JsonProperty("currentRoaming")
    public String getCurrentRoaming() {
        return currentRoaming;
    }

    @JsonProperty("currentRoaming")
    public void setCurrentRoaming(String currentRoaming) {
        this.currentRoaming = currentRoaming;
    }

    @JsonProperty("retrievalStatus")
    public String getRetrievalStatus() {
        return retrievalStatus;
    }

    @JsonProperty("retrievalStatus")
    public void setRetrievalStatus(String retrievalStatus) {
        this.retrievalStatus = retrievalStatus;
    }

    @JsonProperty("retrievalTime")
    public String getRetrievalTime() {
        return retrievalTime;
    }

    @JsonProperty("retrievalTime")
    public void setRetrievalTime(String retrievalTime) {
        this.retrievalTime = retrievalTime;
    }

    @JsonProperty("servingNode")
    public ServingNode getServingNode() {
        return servingNode;
    }

    @JsonProperty("servingNode")
    public void setServingNode(ServingNode servingNode) {
        this.servingNode = servingNode;
    }

    @JsonProperty("servingMccMnc")
    public ServingMccMnc getServingMccMnc() {
        return servingMccMnc;
    }

    @JsonProperty("servingMccMnc")
    public void setServingMccMnc(ServingMccMnc servingMccMnc) {
        this.servingMccMnc = servingMccMnc;
    }
}
