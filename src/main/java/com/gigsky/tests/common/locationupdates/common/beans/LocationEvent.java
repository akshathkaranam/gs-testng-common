package com.gigsky.tests.common.locationupdates.common.beans;

import java.util.List;

/**
 * Created by jeyarajs on 12/12/16.
 */
public class LocationEvent {

    private List<LocationInfo> locationInfoList;
    private Object serverConfig;

    public List<LocationInfo> getLocationInfoList() {
        return locationInfoList;
    }

    public void setLocationInfoList(List<LocationInfo> locationInfoList) {
        this.locationInfoList = locationInfoList;
    }

    public Object getServerConfig() {
        return serverConfig;
    }

    public void setServerConfig(Object serverConfig) {
        this.serverConfig = serverConfig;
    }
}
