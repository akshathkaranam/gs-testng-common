package com.gigsky.tests.common.locationupdates.procera.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by jeyarajs on 09/12/16.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LocationData {
    private Long imsi;
    private String sessionId;
    private Long auxiliarySubscriberId;
    private ProceraMccmnc mccmnc;
    private String creationTime;
    private String updateTime;

    @JsonProperty("imsi")
    public Long getImsi() {
        return imsi;
    }

    @JsonProperty("imsi")
    public void setImsi(Long imsi) {
        this.imsi = imsi;
    }

    @JsonProperty("sessionId")
    public String getSessionId() {
        return sessionId;
    }

    @JsonProperty("sessionId")
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @JsonProperty("parentOid")
    public Long getAuxiliarySubscriberId() {
        return auxiliarySubscriberId;
    }

    @JsonProperty("parentOid")
    public void setAuxiliarySubscriberId(Long auxiliarySubscriberId) {
        this.auxiliarySubscriberId = auxiliarySubscriberId;
    }

    @JsonProperty("mccMnc")
    public ProceraMccmnc getMccmnc() {
        return mccmnc;
    }

    @JsonProperty("mccMnc")
    public void setMccmnc(ProceraMccmnc mccmnc) {
        this.mccmnc = mccmnc;
    }

    @JsonProperty("creationTime")
    public String getCreationTime() {
        return creationTime;
    }

    @JsonProperty("creationTime")
    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    @JsonProperty("updateTime")
    public String getUpdateTime() {
        return updateTime;
    }

    @JsonProperty("updateTime")
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
