package com.gigsky.tests.common.locationupdates.huaweiCSLocation.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by jeyarajs on 12/12/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Trans", propOrder = {
        "serviceName",
        "msgId",
        "connId"
})
public class TransCSLocation {
    @XmlElement(required = true, nillable = true)
    private String connId;
    @XmlElement(required = true, nillable = true)
    private String msgId;
    @XmlElement(required = true, nillable = true)
    private String serviceName;

    public String getConnId () {
        return connId;
    }

    public void setConnId (String connId) {
        this.connId = connId;
    }

    public String getMsgId () {
        return msgId;
    }

    public void setMsgId (String msgId) {
        this.msgId = msgId;
    }

    public String getServiceName () {
        return serviceName;
    }

    public void setServiceName (String serviceName) {
        this.serviceName = serviceName;
    }

    @Override
    public String toString() {
        return "ClassPojo [connId = "+connId+", msgId = "+msgId+", serviceName = "+serviceName+"]";
    }
}
