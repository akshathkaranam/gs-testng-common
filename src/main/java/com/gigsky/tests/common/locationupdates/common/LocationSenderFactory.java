package com.gigsky.tests.common.locationupdates.common;

import com.gigsky.tests.common.locationupdates.common.interfaces.LocationSender;
import com.gigsky.tests.common.locationupdates.huaweiCSLocation.impl.HuaweiCsLocationSender;
import com.gigsky.tests.common.locationupdates.huawei.impl.HuaweiHLRLocationSender;
import com.gigsky.tests.common.locationupdates.procera.impl.ProceraLocationSender;
import com.gigsky.tests.common.locationupdates.tdc.impl.TDCLocationUpdateSender;

/**
 * Created by jeyarajs on 12/12/16.
 */
public class LocationSenderFactory {

    public static final LocationSenderFactory instance = new LocationSenderFactory();

    private LocationSenderFactory() {

    }

    public static LocationSenderFactory getInstance() {
        return instance;
    }

    public LocationSender getLocationSender(LocationSource locationSource) {
        LocationSender locationSender = null;
        switch (locationSource) {
            case TDC:
                locationSender = TDCLocationUpdateSender.getInstance();
                break;
            case HUAWEI:
                locationSender = HuaweiHLRLocationSender.getInstance();
                break;
            case PROCERA:
                locationSender = ProceraLocationSender.getInstance();
                break;
            case HUAWEI_CS:
                locationSender = HuaweiCsLocationSender.getInstance();
        }
        return locationSender;
    }
}
