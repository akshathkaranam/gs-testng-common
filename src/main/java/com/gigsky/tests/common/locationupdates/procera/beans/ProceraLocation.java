package com.gigsky.tests.common.locationupdates.procera.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by jeyarajs on 09/12/16.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ProceraLocation {

    private String method;
    private Params params;

    @JsonProperty("method")
    public String getMethod() {
        return method;
    }

    @JsonProperty("method")
    public void setMethod(String method) {
        this.method = method;
    }

    @JsonProperty("params")
    public Params getParams() {
        return params;
    }

    @JsonProperty("params")
    public void setParams(Params params) {
        this.params = params;
    }
}
