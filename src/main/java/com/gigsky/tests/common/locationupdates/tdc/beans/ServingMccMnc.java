package com.gigsky.tests.common.locationupdates.tdc.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by jeyarajs on 09/12/16.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ServingMccMnc {
    private String mcc;
    private String mnc;

    @JsonProperty("mcc")
    public String getMcc() {
        return mcc;
    }

    @JsonProperty("mcc")
    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    @JsonProperty("mnc")
    public String getMnc() {
        return mnc;
    }

    @JsonProperty("mnc")
    public void setMnc(String mnc) {
        this.mnc = mnc;
    }
}
