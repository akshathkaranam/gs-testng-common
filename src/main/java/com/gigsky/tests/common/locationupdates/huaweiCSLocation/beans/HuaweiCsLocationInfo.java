package com.gigsky.tests.common.locationupdates.huaweiCSLocation.beans;

import com.gigsky.tests.common.locationupdates.common.beans.LocationInfo;
import java.util.Date;

/**
 * Created by Yuvasrikushal on 10/11/17.
 */
public class HuaweiCsLocationInfo extends LocationInfo {

    private String serviceName = "broadcast";
    private String messageId = "14";
    private String connectionId = "0001";
    private String imei;
    private String currNum;
    private String lu_type;
    private Date rTime = new Date();

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getCurrNum() {
        return currNum;
    }

    public void setCurrNum(String currNum) {
        this.currNum = currNum;
    }

    public String getLu_type() {
        return lu_type;
    }

    public void setLu_type(String lu_type) {
        this.lu_type = lu_type;
    }

    public Date getrTime() {
        return rTime;
    }
}
