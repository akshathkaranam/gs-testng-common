package com.gigsky.tests.common.locationupdates.procera.beans;

/**
 * Created by jeyarajs on 12/12/16.
 */
public class RpcServerConfig {

    public RpcServerConfig(String host, int port) {
        this.host = host;
        this.port = port;
    }

    private String host;
    private int port;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
