package com.gigsky.tests.common.locationupdates.huawei.beans;

import javax.xml.bind.annotation.*;

/**
 * Created by jeyarajs on 12/12/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "object", propOrder = {
        "IMEI",
        "IMEIMatch",
        "IMSI",
        "LTEICBR",
        "LTEStatus",
        "MMENewHostName",
        "MMENewPLMN",
        "MMEOldHostName",
        "MMEOldPLMN"
})
public class Object {

    @XmlAttribute(name = "CmdName")
    private String cmdName;
    @XmlElement(required = true, nillable = true)
    private String MMENewHostName;
    @XmlElement(required = true)
    private String IMEIMatch;
    @XmlElement(required = true)
    private String IMEI;
    @XmlElement(required = true, nillable = true)
    private String LTEICBR;
    @XmlElement(required = true, nillable = true)
    private String MMENewPLMN;
    @XmlElement(required = true, nillable = true)
    private String MMEOldHostName;
    @XmlElement(required = true, nillable = true)
    private String IMSI;
    @XmlElement(required = true, nillable = true)
    private String LTEStatus;
    @XmlElement(required = true, nillable = true)
    private String MMEOldPLMN;

    public String getCmdName () {
        return cmdName;
    }

    public void setCmdName (String cmdName) {
        this.cmdName = cmdName;
    }

    public String getMMENewHostName () {
        return MMENewHostName;
    }

    public void setMMENewHostName (String MMENewHostName) {
        this.MMENewHostName = MMENewHostName;
    }

    public String getIMEIMatch () {
        return IMEIMatch;
    }

    public void setIMEIMatch (String IMEIMatch) {
        this.IMEIMatch = IMEIMatch;
    }

    public String getIMEI () {
        return IMEI;
    }

    public void setIMEI (String IMEI) {
        this.IMEI = IMEI;
    }

    public String getLTEICBR () {
        return LTEICBR;
    }

    public void setLTEICBR (String LTEICBR) {
        this.LTEICBR = LTEICBR;
    }

    public String getMMENewPLMN () {
        return MMENewPLMN;
    }

    public void setMMENewPLMN (String MMENewPLMN) {
        this.MMENewPLMN = MMENewPLMN;
    }

    public String getMMEOldHostName () {
        return MMEOldHostName;
    }

    public void setMMEOldHostName (String MMEOldHostName) {
        this.MMEOldHostName = MMEOldHostName;
    }

    public String getIMSI () {
        return IMSI;
    }

    public void setIMSI (String IMSI) {
        this.IMSI = IMSI;
    }

    public String getLTEStatus () {
        return LTEStatus;
    }

    public void setLTEStatus (String LTEStatus) {
        this.LTEStatus = LTEStatus;
    }

    public String getMMEOldPLMN () {
        return MMEOldPLMN;
    }

    public void setMMEOldPLMN (String MMEOldPLMN) {
        this.MMEOldPLMN = MMEOldPLMN;
    }
}
