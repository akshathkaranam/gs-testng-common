package com.gigsky.tests.common.locationupdates.procera.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by jeyarajs on 09/12/16.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ProceraMccmnc {
    private Integer mcc;
    private Integer mnc;

    @JsonProperty("mcc")
    public Integer getMcc() {
        return mcc;
    }

    @JsonProperty("mcc")
    public void setMcc(Integer mcc) {
        this.mcc = mcc;
    }

    @JsonProperty("mnc")
    public Integer getMnc() {
        return mnc;
    }

    @JsonProperty("mnc")
    public void setMnc(Integer mnc) {
        this.mnc = mnc;
    }
}
