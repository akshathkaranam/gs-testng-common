package com.gigsky.tests.common.locationupdates.huawei.beans;

import javax.xml.bind.annotation.*;

/**
 * Created by jeyarajs on 12/12/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Envelope")
@XmlType(name = "Envelope", propOrder = {
        "Header",
        "Body"
})
public class Envelope {
    @XmlElement(required = true, nillable = true)
    private Body Body;
    @XmlElement(required = true, nillable = true)
    private Header Header;

    public Body getBody () {
        return Body;
    }

    public void setBody (Body Body) {
        this.Body = Body;
    }

    public Header getHeader () {
        return Header;
    }

    public void setHeader (Header Header) {
        this.Header = Header;
    }

    @Override
    public String toString() {
        return "ClassPojo [Body = "+Body+", Header = "+Header+"]";
    }
}
