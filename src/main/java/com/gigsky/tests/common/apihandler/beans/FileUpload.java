package com.gigsky.tests.common.apihandler.beans;

/**
 * Created by Vijayalaxmi on 08/01/18.
 */
public class FileUpload {

    private FileData fileData;
    private JsonData jsonData;

    public FileData getFileData() {
        return fileData;
    }

    public void setFileData(FileData fileData) {
        this.fileData = fileData;
    }

    public JsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(JsonData jsonData) {
        this.jsonData = jsonData;
    }
}
