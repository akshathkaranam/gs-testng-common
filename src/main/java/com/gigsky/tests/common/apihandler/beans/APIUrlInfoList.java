package com.gigsky.tests.common.apihandler.beans;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/**
 * Created by haribongale on 13/12/16.
 */

/*
  {
      "server": "UMBS",
      "baseurl": "https://staging-ent.gigsky.com/umbs/api/",
      "apiurls": [
        {
          "name": "USER_LOGIN",
          "url": "v1/users/user/login"
        },
        {
          "name": "ACCOUNT_LOGIN",
          "url": "v1/users/user/#USERID/account/#ACCOUNTID/login"
        }
      ]
 */
@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class APIUrlInfoList {
    private List<APIUrlInfo> urls;

    public List<APIUrlInfo> getUrls() {
        return urls;
    }

    public void setUrls(List<APIUrlInfo> urls) {
        this.urls = urls;
    }

    public APIUrlInfo getAPIUrlInfo(String apiServerName) {
        if(apiServerName == null) {
            return null;
        }

        for(APIUrlInfo apiUrlInfo : getUrls()) {
            if(apiServerName.equalsIgnoreCase(apiUrlInfo.getServer())) {
                return apiUrlInfo;
            }
        }

        return null;
    }
}

