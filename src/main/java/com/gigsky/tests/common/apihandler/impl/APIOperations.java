package com.gigsky.tests.common.apihandler.impl;

import javax.ws.rs.core.MediaType;

/**
 * Created by jeyarajs on 13/12/16.
 */
public class APIOperations {

    protected static final String CONTENT_TYPE_HEADER = "Content-Type";
    protected static final String JSON_MEDIA_TYPE = MediaType.APPLICATION_JSON + ";charset=UTF-8";
    protected static final String MULTIPART_FORM_DATA_MEDIA_TYPE = MediaType.MULTIPART_FORM_DATA + ";charset=UTF-8";
    protected static final String TOKEN_HEADER = "token";
    protected static final String AUTHORIZATION_HEADER = "Authorization";
    protected static final String ACCEPT_LANGUAGE = "Accept-Language";
    protected static final String EN_ACCEPT_LANGUAGE = "en";
    protected static final String X_MS_DM_TRANSACTIONID = "X-MS-DM-TransactionId";
    protected static final String LIMIT = "limit";
    protected static final String FIELDS_TEMPLATE = "fieldstemplate";
    protected static final String LOCATION = "location";
    protected static final String TENANTSID = "tenantsId";
}
