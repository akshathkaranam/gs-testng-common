package com.gigsky.tests.common.apihandler.beans;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/**
 * Created by haribongale on 13/12/16.
 */

/*
  {
      "server": "UMBS",
      "baseurl": "https://staging-ent.gigsky.com/umbs/api/",
      "apiurls": [
        {
          "name": "USER_LOGIN",
          "url": "v1/users/user/login"
        },
        {
          "name": "ACCOUNT_LOGIN",
          "url": "v1/users/user/#USERID/account/#ACCOUNTID/login"
        }
      ]
 */
@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class APIUrlInfo {
    private String server;
    private String baseurl;
    private List<APIUrl> apiurls;


    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getBaseurl() {
        return baseurl;
    }

    public void setBaseurl(String baseurl) {
        this.baseurl = baseurl;
    }

    public List<APIUrl> getApiurls() {
        return apiurls;
    }

    public void setApiurls(List<APIUrl> apiurls) {
        this.apiurls = apiurls;
    }

    public APIUrl getAPIUrl(String apiName) {
        if(apiName == null) {
            return null;
        }

        for(APIUrl apiUrl : getApiurls()) {
            if(apiName.equalsIgnoreCase(apiUrl.getApi())) {
                return apiUrl;
            }
        }

        return null;
    }
}
