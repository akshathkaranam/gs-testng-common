package com.gigsky.tests.common.apihandler.beans;

/**
 * Created by Vijayalaxmi on 09/01/18.
 */
public class JsonData {
    private String key;
    private Object requestData;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getRequestData() {
        return requestData;
    }

    public void setRequestData(String requestData) {
        this.requestData = requestData;
    }
}
