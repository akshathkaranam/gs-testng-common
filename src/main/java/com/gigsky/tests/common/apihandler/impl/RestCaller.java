package com.gigsky.tests.common.apihandler.impl;

import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;
import com.gigsky.tests.common.apihandler.factory.APIHandlerFactory;
import com.gigsky.tests.common.apihandler.interfaces.APIHandler;

import javax.ws.rs.HttpMethod;



/**
 * Created by smaragal on Feb, 2020
 */
public class RestCaller  {

    public APIResponse restCaller(APIHandlerConfig apiHandlerConfig) throws Exception {
        APIHandlerFactory apiHandlerFactory = APIHandlerFactory.getInstance();
        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        APIResponse response;

        switch (apiHandlerConfig.getHttpMethod()) {
            case HttpMethod.GET: {
                response = apiHandler.get(apiHandlerConfig);
                break;
            }
            case HttpMethod.PUT: {
                response = apiHandler.put(apiHandlerConfig);
                break;
            }
            case HttpMethod.DELETE: {
                response = apiHandler.delete(apiHandlerConfig);
                break;
            }
            case HttpMethod.POST: {
                response = apiHandler.post(apiHandlerConfig);
                break;
            }
            default:
                throw new IllegalStateException("Unexpected value: " + apiHandlerConfig.getHttpMethod());
        }
        return response;
    }

}
