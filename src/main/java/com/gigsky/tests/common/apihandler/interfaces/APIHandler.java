package com.gigsky.tests.common.apihandler.interfaces;

import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;

/**
 * Created by jeyarajs on 13/12/16.
 */
public interface APIHandler {

    public APIResponse get(APIHandlerConfig apiHandlerConfig) throws Exception;
    public APIResponse put(APIHandlerConfig apiHandlerConfig) throws Exception;
    public APIResponse post(APIHandlerConfig apiHandlerConfig) throws Exception;
    public APIResponse delete(APIHandlerConfig apiHandlerConfig) throws Exception;
}
