package com.gigsky.tests.common.apihandler.beans;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gigsky.tests.common.utils.JsonToObjectConverter;
import com.gigsky.tests.common.utils.JsonUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;


@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class GenericData {


    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap();

    public GenericData() {
    }


    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Object get2(String key) {
        return this.getAdditionalProperties().get(key);
    }

    public <T> T get(String compositeKey, Class<T> clazz) throws Exception {
        String[] keySet = compositeKey.split("\\.");
        Object temp = this.getAdditionalProperties();

        for (int i = 0; i < keySet.length; i++) {
            temp = ((HashMap<String, Object>) temp).get(keySet[i]);
        }

        if (clazz != null) {
            // do class conversion

            // convert map to JSON String
            try {
                String jsonResp = JsonUtils.writeValueAsString(temp);
                String pkgName = getPackageName(clazz);
                if(StringUtils.isNotEmpty(pkgName) && pkgName.startsWith("com.fasterxml.jackson")){
                    return JsonToObjectConverter.convert(jsonResp,clazz);
                }else {
                    return JsonUtils.readValue(jsonResp, clazz);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return null;
    }

    public static String getPackageName(Class clazz){
        Method[] methods = clazz.getMethods();
        for(Method method: methods){
            Annotation[] annotations = method.getAnnotations();
            for(Annotation annotation:annotations){
                String pkgName = annotation.annotationType().getPackage().getName();
                return pkgName;

            }
        }
        Field[] fields = clazz.getDeclaredFields();
        for(Field field:fields){

            Annotation[] annotations = field.getAnnotations();
            for(Annotation annotation:annotations){
                String pkgName = annotation.annotationType().getPackage().getName();
                return pkgName;

            }
            if(!isJavaLang(field.getType()) && !isEnumType(field.getType()))
                return getPackageName(field.getType());


        }
        return null;
    }
    public static boolean isJavaLang(Class check) {
        return check.getName().startsWith("java.lang");
    }
    public static boolean isEnumType(Class type){
        if(type instanceof Class && ((Class<?>)type).isEnum()){
            return true;
        }else{
            return false;
        }
    }
//    public <T> T get(String compositeKey, CollectionType clazz) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
//        String[] keySet = compositeKey.split("\\.");
//        Object temp = this.getAdditionalProperties();
//
//        for (int i = 0; i < keySet.length; i++) {
//            temp = ((HashMap<String, Object>) temp).get(keySet[i]);
//        }
//
//        if (clazz != null) {
//            // do class conversion
//
//            // convert map to JSON String
//            try {
//                String jsonResp = JsonUtils.writeValueAsString(temp);
//                return JsonUtils.readValue(jsonResp, clazz);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        }
//
//        return null;
//    }


}
