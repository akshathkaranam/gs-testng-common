package com.gigsky.tests.common.apihandler.beans;

/**
 * Created by jeyarajs on 13/12/16.
 */
public class APIResponse {
    private int responseCode;
    private String responseString;
    private Object responseBody;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseString() {
        return responseString;
    }

    public void setResponseString(String responseString) {
        this.responseString = responseString;
    }

    public Object getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(Object responseBody) {
        this.responseBody = responseBody;
    }
}
