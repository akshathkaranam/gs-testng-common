package com.gigsky.tests.common.apihandler.beans;


import com.gigsky.tests.common.utils.JsonToObjectConverter;

/**
 * Created by vishaljogi on 09/01/17.
 */
public abstract class AbstractTestNGData {

    private GenericData data = null;

    protected abstract String getJsonData();
    public AbstractTestNGData() {

    }


    private void initialize(){
        try {
            if(data == null && getJsonData() != null) {
                data = (GenericData) JsonToObjectConverter.convert(getJsonData(), GenericData.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public GenericData getData(){
        initialize();
        return data;
    }
}
