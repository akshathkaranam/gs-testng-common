package com.gigsky.tests.common.apihandler.impl;

import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;
import com.gigsky.tests.common.apihandler.beans.FileUpload;
import com.gigsky.tests.common.apihandler.beans.GenericData;
import com.gigsky.tests.common.apihandler.interfaces.APIHandler;
import com.gigsky.tests.common.apihandler.interfaces.ResponseBodyValidator;
import com.gigsky.tests.common.utils.APIUrlUtils;
import com.gigsky.tests.common.utils.CsvFileUtils;
import com.gigsky.tests.common.utils.JsonToObjectConverter;
import com.gigsky.tests.common.utils.JsonUtils;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import javax.ws.rs.HttpMethod;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;
import static io.restassured.config.RedirectConfig.redirectConfig;
import static org.junit.Assert.assertTrue;

/**
 * Created by jeyarajs on 13/12/16.
 */
public class APIHandlerImpl implements APIHandler {

    public static final APIHandlerImpl instance = new APIHandlerImpl();
    private static final Logger logger = LoggerFactory.getLogger(APIHandlerImpl.class);


    private APIHandlerImpl() {

    }

    public static APIHandlerImpl getInstance() {
        return instance;
    }

    @Override
    public APIResponse get(APIHandlerConfig apiHandlerConfig) throws Exception {
        return executeMethod(apiHandlerConfig, "GET");
    }

    @Override
    public APIResponse put(APIHandlerConfig apiHandlerConfig) throws Exception {
        return executeMethod(apiHandlerConfig, "PUT");
    }

    @Override
    public APIResponse post(APIHandlerConfig apiHandlerConfig) throws Exception {
        return executeMethod(apiHandlerConfig, "POST");
    }

    @Override
    public APIResponse delete(APIHandlerConfig apiHandlerConfig) throws Exception {
        return executeMethod(apiHandlerConfig, "DELETE");
    }

    private APIResponse executeMethod(APIHandlerConfig apiHandlerConfig, String httpMethod) throws Exception {
        int retryCount = apiHandlerConfig.getRetryCount() == null || apiHandlerConfig.getRetryCount() == 0 ? 1 : apiHandlerConfig.getRetryCount();
        int delayMs = apiHandlerConfig.getRetryTimeoutMS() == null || apiHandlerConfig.getRetryTimeoutMS() == 0 ? 100 : apiHandlerConfig.getRetryTimeoutMS();
        Response response = null;
        for (int iteration = 0; iteration < retryCount; iteration++) {
            try {
                switch (httpMethod) {
                    case HttpMethod.GET:
                        response = getRequestSpecification(apiHandlerConfig).get(getUrl(apiHandlerConfig));
                        break;
                    case HttpMethod.POST:
                        response = getRequestSpecification(apiHandlerConfig).post(getUrl(apiHandlerConfig));
                        break;
                    case HttpMethod.DELETE:
                        response = getRequestSpecification(apiHandlerConfig).delete(getUrl(apiHandlerConfig));
                        break;
                    case HttpMethod.PUT:
                        response = getRequestSpecification(apiHandlerConfig).put(getUrl(apiHandlerConfig));
                        break;
                }

                response.then().log().all();
                validateResponse(apiHandlerConfig, response);
                break;
            } catch (AssertionError | Exception e) {
                if (iteration == retryCount - 1) {
                    throw e;
                }else {
                    Thread.sleep(delayMs);
                }
            }
        }
        return getAPIResponse(apiHandlerConfig, response);
    }

    private APIResponse getAPIResponse(APIHandlerConfig apiHandlerConfig,
                                       Response response) throws Exception {
        APIResponse apiResponse = new APIResponse();
        apiResponse.setResponseCode(response.getStatusCode());
        String responseString = response.getBody().print();
        apiResponse.setResponseString(responseString);
        Class responseValueType = apiHandlerConfig.getResponseValueType();
        if (responseValueType != null && StringUtils.isNotEmpty(responseString)) {
            if(responseValueType.equals(File.class)) {
                apiResponse.setResponseBody(CsvFileUtils.getFileFromResponseCsvLines(response.getBody().print().split("\r\n")));
            }
            else {
                apiResponse.setResponseBody(JsonUtils.readValue(responseString, responseValueType));
            }
        }
        return apiResponse;
    }





    public static APIResponse uploadFile(APIHandlerConfig apiHandlerConfig, FileUpload fileUpload) throws Exception {
        try {
            HttpClient client = HttpClientBuilder.create().build();
            String apiUrl = getAPIUrl(apiHandlerConfig);
            HttpEntityEnclosingRequestBase httpRequest = null;

            if(apiHandlerConfig.getHttpMethod().equals("POST")) {
                httpRequest = new HttpPost(apiUrl);
            }
            else if(apiHandlerConfig.getHttpMethod().equals("PUT"))
            {
                httpRequest = new HttpPut(apiUrl);
            }
            else
            {
                throw new Exception("Unsupported method for upload file");
            }
            addQueryParams(apiHandlerConfig, httpRequest);
            addHeaders(apiHandlerConfig, httpRequest);

            logger.info(httpRequest.getURI().toString());



            String textFileName = fileUpload.getFileData().getFilePath();
            File inputFile = new File(textFileName);


            assertTrue(inputFile.getAbsolutePath() + " file doesn't exists", (inputFile.exists() && inputFile.isFile()));

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            logger.info("key = " + fileUpload.getFileData().getKey() + " FileName : " + inputFile.getName());
            builder.addBinaryBody(fileUpload.getFileData().getKey(), inputFile, ContentType.DEFAULT_BINARY, inputFile.getName());

            if(fileUpload.getJsonData()!=null) {
                String message = JsonUtils.writeValueAsString(fileUpload.getJsonData().getRequestData());
                builder.addTextBody(fileUpload.getJsonData().getKey(), message, ContentType.APPLICATION_JSON);

            }

            HttpEntity entity = builder.build();
            httpRequest.setEntity(entity);

            HttpResponse response = client.execute(httpRequest);
            return convertToAPIResponse(response, apiHandlerConfig);
        }
        catch (Exception e) {
            logger.error("Exception occurred", e);
            throw e;
        }
    }



    public static APIResponse convertToAPIResponse(HttpResponse response, APIHandlerConfig apiHandlerConfig) throws IOException {
        APIResponse apiResponse = new APIResponse();
        apiResponse.setResponseCode(response.getStatusLine().getStatusCode());

        String responseString = getStringContent(response);

        if (StringUtils.isNotEmpty(responseString)) {
            apiResponse.setResponseString(responseString);
            apiResponse.setResponseBody(JsonUtils.readValue(responseString, apiHandlerConfig.getResponseValueType()));
        }
        logger.info(apiResponse.getResponseString());
        return apiResponse;
    }


    private static String getStringContent(HttpResponse response) throws IOException {
        InputStream content = response.getEntity().getContent();
        BufferedReader reader = new BufferedReader(new InputStreamReader(content));
        StringBuilder respContent = new StringBuilder();
        String str;
        while ((str = reader.readLine()) != null) {
            respContent.append(str);
        }
        return respContent.toString();
    }



    public static String getAPIUrl(APIHandlerConfig apiHandlerConfig) {
        String apiUrl = APIUrlUtils.getUrl(apiHandlerConfig.getApiServer(), apiHandlerConfig.getApiName());
        String pathParamReplacedUrl = replacePlaceHoldersInUrl(apiHandlerConfig, apiUrl);
        return pathParamReplacedUrl;
    }

    private static void addHeaders(APIHandlerConfig apiHandlerConfig, HttpRequestBase requestBase) {
        requestBase.addHeader("token", (String) apiHandlerConfig.getHeaders().get("token"));
    }

    private static void addQueryParams(APIHandlerConfig apiHandlerConfig, HttpRequestBase httpRequestBase) throws URISyntaxException {
        if (MapUtils.isNotEmpty(apiHandlerConfig.getQueryParams())) {
            URIBuilder uriBuilder = new URIBuilder(httpRequestBase.getURI());
            for (String key : apiHandlerConfig.getQueryParams().keySet()) {
                Object queryParamValue = apiHandlerConfig.getQueryParams().get(key);
                uriBuilder = uriBuilder.addParameter(key, queryParamValue.toString());
            }
            URI uri = uriBuilder.build();
            httpRequestBase.setURI(uri);
        }
    }

    public static String replacePlaceHoldersInUrl(APIHandlerConfig apiHandlerConfig, String apiUrl) {
        Map<String, String> pathParams = apiHandlerConfig.getPathParams();
        if (MapUtils.isNotEmpty(pathParams)) {
            for (String pathParamKey : pathParams.keySet()) {
                apiUrl = apiUrl.replaceAll("\\{" + pathParamKey + "\\}", pathParams.get(pathParamKey));
            }
        }
        return apiUrl;
    }

    private void validateResponse(APIHandlerConfig apiHandlerConfig,
                                  Response response) throws Exception {

        if ( apiHandlerConfig.getExpectedResponseCode() != null) {
            validateResponseCode(apiHandlerConfig.getExpectedResponseCode(), response);
        }
        validateResponseHeader(apiHandlerConfig, response);
        validateResponseBody(apiHandlerConfig, response);
    }

    private void validateResponseHeader(APIHandlerConfig apiHandlerConfig, Response response) {
        if (apiHandlerConfig.getExpectedHeaders() != null) {
            Headers headers = response.getHeaders();
            Assert.assertNotNull(headers);
            for (Map.Entry<String, String> entry : apiHandlerConfig.getExpectedHeaders().entrySet()) {
                Header header = headers.get(entry.getKey());
                Assert.assertNotNull(header);
                Assert.assertEquals(header.getValue(), entry.getValue(), "Header miss match for Header Key[" + entry.getKey() + "]");
            }

        }
    }

    private void validateResponseCode(int expectedResponseCode,
                                      Response response) {

        Assert.assertEquals(response.getStatusCode(), expectedResponseCode, response.getBody().print());
    }

    private void validateResponseBody(APIHandlerConfig apiHandlerConfig,
                                      Response response) throws Exception {
        Object expectedResponseBody = apiHandlerConfig.getExpectedResponseBody();
        ResponseBodyValidator responseBodyValidator = apiHandlerConfig.getResponseBodyValidator();
        if (expectedResponseBody != null
                && responseBodyValidator != null) {
            if(response.getContentType().equals("text/csv")) {
                Assert.assertTrue(responseBodyValidator.validateResponseBody(expectedResponseBody, CsvFileUtils.getFileFromResponseCsvLines(response.getBody().print().split("\r\n"))));
            }
            else {

                String pkgName = null;
                if(StringUtils.isEmpty(apiHandlerConfig.getJsonPackage())) {
                    pkgName = GenericData.getPackageName(apiHandlerConfig.getResponseValueType());
                }else{
                    pkgName = apiHandlerConfig.getJsonPackage();
                }

                if(StringUtils.isNotEmpty(pkgName) && !pkgName.startsWith("com.fasterxml.jackson")) {
                    Assert.assertTrue(responseBodyValidator.validateResponseBody(expectedResponseBody,
                            JsonUtils.readValue(response.getBody().print(), apiHandlerConfig.getResponseValueType())));

                }else{
                    Assert.assertTrue(responseBodyValidator.validateResponseBody(expectedResponseBody,
                            JsonToObjectConverter.convert(response.getBody().print(), apiHandlerConfig.getResponseValueType())));

                }
            }
        }
    }

    private RequestSpecification getRequestSpecification(APIHandlerConfig apiHandlerConfig) {
        RequestSpecification requestSpecification = given().log().all();
        requestSpecification.config(config().redirect(redirectConfig().followRedirects(false)));
        addHeaders(apiHandlerConfig.getHeaders(), requestSpecification);
        addQueryParams(apiHandlerConfig.getQueryParams(), requestSpecification);
        addRequestBody(apiHandlerConfig.getRequestBody(), requestSpecification);
        return requestSpecification;
    }

    private void addHeaders(Map<String, Object> headers,
                            RequestSpecification requestSpecification) {
        if (headers != null) {
            for (Map.Entry<String, Object> headerEntry : headers.entrySet()) {
                requestSpecification.header(headerEntry.getKey(), headerEntry.getValue());
            }
        }
    }

    private void addQueryParams(Map<String, Object> queryParams,
                                RequestSpecification requestSpecification) {
        if (queryParams != null) {
            for (Map.Entry<String, Object> queryParamEntry : queryParams.entrySet()) {
                requestSpecification.queryParam(queryParamEntry.getKey(), queryParamEntry.getValue());
            }
        }
    }

    private String getUrl(APIHandlerConfig apiHandlerConfig) {
        String url = APIUrlUtils.getUrl(apiHandlerConfig.getApiServer(), apiHandlerConfig.getApiName());
        return replacePathParam(apiHandlerConfig.getPathParams(), url);
    }

    private String replacePathParam(Map<String, String> pathParams,
                                    String urlToReplace) {
        String url = urlToReplace;
        if (pathParams != null) {
            for (Map.Entry<String, String> pathParamEntry : pathParams.entrySet()) {
                url = url.replaceAll("\\{" + pathParamEntry.getKey() + "\\}", pathParamEntry.getValue());
            }
        }
        return url;
    }

    private void addRequestBody(Object requestBody,
                                RequestSpecification requestSpecification) {
        if (requestBody != null) {
            if(requestBody instanceof File) {
                requestSpecification.multiPart((File)requestBody);
            }
            else {
                requestSpecification.body(requestBody);
            }
        }
    }
}
