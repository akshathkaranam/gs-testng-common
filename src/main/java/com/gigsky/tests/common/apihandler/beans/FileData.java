package com.gigsky.tests.common.apihandler.beans;

/**
 * Created by Vijayalaxmi on 09/01/18.
 */
public class FileData {

    private String key;
    private String filePath;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
