package com.gigsky.tests.common.apihandler.beans;

import com.gigsky.tests.common.apihandler.interfaces.ResponseBodyValidator;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by haribongale on 12/12/16.
 */
public class APIHandlerConfig {

    private String apiServer;
    private String apiName;
    private String httpMethod;
    private Map<String, Object> headers;
    private Map<String, Object> queryParams;
    private Map<String, String> pathParams;
    private Integer retryCount;
    private Integer retryTimeoutMS;
    private Object requestBody;
    private Integer expectedResponseCode;
    private Object expectedResponseBody;
    private Map<String, String> expectedHeaders;
    private ResponseBodyValidator responseBodyValidator;
    private Class responseValueType;
    private String jsonPackage;

    public String getApiServer() {
        return apiServer;
    }

    public void setApiServer(String apiServer) {
        this.apiServer = apiServer;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    public Map<String, Object> getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(Map<String, Object> queryParams) {
        this.queryParams = queryParams;
    }

    public Map<String, String> getPathParams() {
        return pathParams;
    }

    public void setPathParams(Map<String, String> pathParams) {
        this.pathParams = pathParams;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public Integer getRetryTimeoutMS() {
        return retryTimeoutMS;
    }

    public void setRetryTimeoutMS(Integer retryTimeoutMS) {
        this.retryTimeoutMS = retryTimeoutMS;
    }

    public Object getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(Object requestBody) {
        this.requestBody = requestBody;
    }

    public Integer getExpectedResponseCode() {
        return expectedResponseCode;
    }

    public void setExpectedResponseCode(Integer expectedResponseCode) {
        this.expectedResponseCode = expectedResponseCode;
    }

    public Object getExpectedResponseBody() {
        return expectedResponseBody;
    }

    public void setExpectedResponseBody(Object expectedResponseBody) {
        this.expectedResponseBody = expectedResponseBody;
    }

    public ResponseBodyValidator getResponseBodyValidator() {
        return responseBodyValidator;
    }

    public void setResponseBodyValidator(ResponseBodyValidator responseBodyValidator) {
        this.responseBodyValidator = responseBodyValidator;
    }

    public Class getResponseValueType() {
        return responseValueType;
    }

    public void setResponseValueType(Class responseValueType) {
        this.responseValueType = responseValueType;
    }

    public void addHeader(String key,
                          Object value) {
        if(headers == null) {
            headers = new HashMap<String, Object>();
        }
        headers.put(key, value);
    }

    public void addQueryParam(String key,
                              String value) {
        if(queryParams == null) {
            queryParams = new HashMap<String, Object>();
        }
        queryParams.put(key, value);
    }

    public void addPathParam(String key,
                              String value) {
        if(pathParams == null) {
            pathParams = new HashMap<String, String>();
        }
        pathParams.put(key, value);
    }

    public void addExpectedHeader(String key, String value){
        if(expectedHeaders == null){
            expectedHeaders = new HashMap<String, String>();
        }
        expectedHeaders.put(key,value);
    }
    public Map<String, String> getExpectedHeaders() {
        return expectedHeaders;
    }

    public void setExpectedHeaders(Map<String, String> expectedHeaders) {
        this.expectedHeaders = expectedHeaders;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public String getJsonPackage() {
        return jsonPackage;
    }

    public void setJsonPackage(String jsonPackage) {
        this.jsonPackage = jsonPackage;
    }
}
