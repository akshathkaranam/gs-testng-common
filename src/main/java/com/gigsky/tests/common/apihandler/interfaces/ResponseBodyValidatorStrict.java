package com.gigsky.tests.common.apihandler.interfaces;

import com.gigsky.tests.common.utils.JsonUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.skyscreamer.jsonassert.FieldComparisonFailure;
import org.skyscreamer.jsonassert.JSONCompare;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.JSONCompareResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Harshith on 26/6/18.
 */
public abstract class ResponseBodyValidatorStrict<T> extends ResponseBodyValidator<T> {

    private static final Logger logger = LoggerFactory.getLogger(ResponseBodyValidatorStrict.class);

    public boolean validateResponseBodyValues(T expectedResponseBody,
                                              T actualResponseBody) throws Exception {
        if(expectedResponseBody == null
                || actualResponseBody == null) {
            return false;
        }
        return validateResponseBodyValues(JsonUtils.writeValueAsString(expectedResponseBody),
                JsonUtils.writeValueAsString(actualResponseBody));

    }

    public boolean validateResponseBodyValues(String expectedResponseBody,
                                              String actualResponseBody) throws Exception {

        System.out.println("Expected Response : " + expectedResponseBody);
        System.out.println("Actual Response : " + actualResponseBody);

        /*Base class verifies the values and throws error*/
        JSONCompareResult jsonCompareResult = JSONCompare.compareJSON(
                expectedResponseBody,
                actualResponseBody,
                JSONCompareMode.STRICT);
        valueComparionPassed = jsonCompareResult.passed();
        logger.info("Validation Comparison: passed: "+valueComparionPassed);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(jsonCompareResult.getMessage()+"\n");
        List<FieldComparisonFailure> fieldFailures = jsonCompareResult.getFieldFailures();
        if(CollectionUtils.isNotEmpty(fieldFailures)) {
            stringBuilder.append("FieldFailures : " + fieldFailures.size() +"\n");
            for (FieldComparisonFailure fieldComparisonFailure : fieldFailures) {
                stringBuilder.append("Field: " + fieldComparisonFailure.getField()
                        + " Expected:" + fieldComparisonFailure.getExpected().toString()
                        + " Actual:" + fieldComparisonFailure.getActual().toString());
            }
        }
        comparsionMessage = stringBuilder.toString();
        if(StringUtils.isNotEmpty(comparsionMessage)) {
            logger.error(comparsionMessage);
        }
        return valueComparionPassed;
    }
}
