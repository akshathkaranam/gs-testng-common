package com.gigsky.tests.common.paypal;

import com.gigsky.tests.common.paypal.beans.PaypalLoginBean;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.AssertJUnit;

import java.util.concurrent.TimeUnit;

/**
 * Created by vishaljogi on 07/06/18.
 */
public class PaypalUtil {

    private static final PaypalUtil instance=new PaypalUtil();

    public static PaypalUtil getInstance() {
        return instance;
    }

    WebDriver driver = null;
    public void runStepPaypalLogin(PaypalLoginBean paypalLoginBean, boolean exceptionOnFailure) throws Exception {

        AssertJUnit.assertTrue("paypallogin xml element was not found in step\n", (paypalLoginBean != null));
        AssertJUnit.assertTrue("paypallogin password is empty\n", (paypalLoginBean.getPassword().length() > 0));

        String errorMessage = "Failed to instantiate webdriver";
        StringBuffer stringBuffer = new StringBuffer();
        try {
            driver = new ChromeDriver();
            driver.manage().window().setSize(new Dimension(1280, 1024));
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
            driver.manage().deleteAllCookies();

            errorMessage = "Failed to load paypal access page: " + paypalLoginBean.getLoginApproveUrl();
            driver.get(paypalLoginBean.getLoginApproveUrl());
            System.out.println("Paypal Url: " + paypalLoginBean.getLoginApproveUrl());
            System.out.flush();
            // User name is already set on the page.
            errorMessage = "Failed to find password field by id: " + PaypalLoginConstants.PAYPAL_LOGIN_PASSWORD_FIELDNAME_TYPEA;
            WebElement element = null;
            if(driver.findElements(By.id(PaypalLoginConstants.PAYPAL_LOGIN_PASSWORD_FIELDNAME_TYPEA)).size()!=0){
                stringBuffer.append("Searching by ID: " + PaypalLoginConstants.PAYPAL_LOGIN_PASSWORD_FIELDNAME_TYPEA);
                System.out.flush();
                element = driver.findElement(By.id(PaypalLoginConstants.PAYPAL_LOGIN_PASSWORD_FIELDNAME_TYPEA));
            }else{
                stringBuffer.append("Searching by ID: " + PaypalLoginConstants.PAYPAL_LOGIN_PASSWORD_FIELDNAME_TYPEB);
                System.out.flush();
                element = driver.findElement(By.id(PaypalLoginConstants.PAYPAL_LOGIN_PASSWORD_FIELDNAME_TYPEB));
            }
            // wait for animation to finish. Otherwise password box is not visible yet and we get ElementNotVisibleException.
            Thread.sleep(3000);

            errorMessage = "Failed to send password text to browser.";
            element.sendKeys(paypalLoginBean.getPassword());

            errorMessage = "Failed to find submit id: " + PaypalLoginConstants.PAYPAL_LOGIN_LOGIN_SUBMIT_ID_TYPEA;
            if(driver.findElements(By.id(PaypalLoginConstants.PAYPAL_LOGIN_LOGIN_SUBMIT_ID_TYPEA)).size()!=0){
                stringBuffer.append("Searching by ID: " + PaypalLoginConstants.PAYPAL_LOGIN_LOGIN_SUBMIT_ID_TYPEA);
                System.out.flush();
                element = driver.findElement(By.id(PaypalLoginConstants.PAYPAL_LOGIN_LOGIN_SUBMIT_ID_TYPEA));
            }else{
                stringBuffer.append("Searching by ID: " + PaypalLoginConstants.PAYPAL_LOGIN_LOGIN_SUBMIT_ID_TYPEB);
                System.out.flush();
                element = driver.findElement(By.id(PaypalLoginConstants.PAYPAL_LOGIN_LOGIN_SUBMIT_ID_TYPEB));
            }


            errorMessage = "Failed to login with clicking on login-submit";
            try {
                element.click();
            } catch (TimeoutException te) {
                driver.findElement(By.tagName("body")).sendKeys("Keys.ESCAPE");
                ((JavascriptExecutor) driver).executeScript("return window.stop");
            }


            errorMessage = "Failed to find Approve button with id: " + PaypalLoginConstants.PAYPAL_APPROVE_SUBMIT_ID;
            Thread.sleep(10000);

            if(driver.findElements(By.id(PaypalLoginConstants.PAYPAL_APPROVE_SUBMIT_ID)).size()!=0){

                stringBuffer.append("Searching by ID: " + PaypalLoginConstants.PAYPAL_APPROVE_SUBMIT_ID);
               // driver.findElementById(PaypalLoginConstants.PAYPAL_APPROVE_SUBMIT_ID).click();
                driver.findElement(By.id(PaypalLoginConstants.PAYPAL_APPROVE_SUBMIT_ID)).click();

            }else{
                stringBuffer.append("Could not find " + PaypalLoginConstants.PAYPAL_APPROVE_SUBMIT_ID);
                throw new Exception("Paypal Approval Failed. Could not find approve button");
            }

            errorMessage = "Paypal Approval Failed. Could not find success message on site.";

            if(driver.findElements(By.id(PaypalLoginConstants.PAYPAL_APPROVE_DONE_MESSAGE_ID)).size()!=0){
                stringBuffer.append("Searching by ID: " + PaypalLoginConstants.PAYPAL_APPROVE_DONE_MESSAGE_ID);
                element = driver.findElement(By.id(PaypalLoginConstants.PAYPAL_APPROVE_DONE_MESSAGE_ID));
            }else{
                stringBuffer.append("Could not find " + PaypalLoginConstants.PAYPAL_APPROVE_DONE_MESSAGE_ID);
                throw new Exception("Paypal Approval Failed. Could not find done info button");
            }

      /*      String msg = element.getAttribute("innerHTML");
            if (msg == null || msg.toLowerCase().contains("success") == false) {

                if(driver.findElements(By.id(PaypalLoginConstants.PAYPAL_LOGIN_PASSWORD_FIELDNAME_TYPEB)).size()!=0){

                    stringBuffer.append("Considering new login page as the success page");
                }
                else
                {
                    throw new Exception("Paypal Approval Failed. Could not find success message on site.");
                }
            }*/

            errorMessage = "Failed to unload web driver";
            driver.close();
            driver = null;

        }
        catch (Exception e) {
            if (exceptionOnFailure == false) {
                AssertJUnit.assertTrue(errorMessage + "\n\nException message:\n" + e.getMessage() + "\n", false);
            }
            else {
                System.out.println(e.getMessage());
                throw new Exception(errorMessage + "\n");

            }
        }
        finally {
            if (driver != null) {
                driver.close();
                driver = null;
            }
        }
    }
}
