package com.gigsky.tests.common.paypal.beans;

/**
 * Created by vishaljogi on 07/06/18.
 */
public class PaypalLoginBean {

    String username;

    String password;

    String loginApproveUrl;

    public String getLoginApproveUrl() {
        return loginApproveUrl;
    }

    public void setLoginApproveUrl(String loginApproveUrl) {
        this.loginApproveUrl = loginApproveUrl;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
