package com.gigsky.tests.common.paypal;

/**
 * Created by vishaljogi on 07/06/18.
 */
public class PaypalLoginConstants {

    public static final String PAYPAL_LOGIN_URL = "https://sandbox.google.com";
    public static final String PAYPAL_LOGIN_USERNAME_FIELDNAME = "login_email";
    public static final String PAYPAL_LOGIN_PASSWORD_FIELDNAME_TYPEA = "login_password";
    public static final String PAYPAL_LOGIN_PASSWORD_FIELDNAME_TYPEB = "password";
    public static final String PAYPAL_LOGIN_LOGIN_SUBMIT_ID_TYPEA = "submitLogin";
    public static final String PAYPAL_LOGIN_LOGIN_SUBMIT_ID_TYPEB = "btnLogin";
    public static final String PAYPAL_APPROVE_SUBMIT_ID = "submit.x";
    public static final String PAYPAL_APPROVE_DONE_MESSAGE_ID = "doneInfo";
   // public static final String PAYPAL_APPROVE_DONE_MESSAGE_ID = "returnToMerchant";

}
